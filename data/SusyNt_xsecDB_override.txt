dataset_number/I:physics_short/C:crossSection/D:genFiltEff/D:kFactor/D:relUncertUP/D:relUncertDOWN/D:generator_name/C
################################################################################
# Single Higgs
################################################################################
# VH
342284 Pythia8EvtGen_A14NNPDF23LO_WH125_inc                      1.369       1.0      1.0  0.0 0.0 Pythia8(v8.186)+EvtGen(v1.2.0)
342285 Pythia8EvtGen_A14NNPDF23LO_ZH125_inc                      0.8824      1.0      1.0  0.0 0.0 Pythia8(v8.186)+EvtGen(v1.2.0)

345211 PowhegPy8EG_NNPDF30_AZNLO_WmH125J_Winc_MINLO_tautau       0.00054016  1        1.0  0.0 0.0 Powheg+Pythia8(v8.212)+EvtGen(v1.6.0)
345212 PowhegPy8EG_NNPDF30_AZNLO_WpH125J_Winc_MINLO_tautau       0.00086204  1        1.0  0.0 0.0 Powheg+Pythia8(v8.212)+EvtGen(v1.6.0)

# ggH
345120 PowhegPy8EG_NNLOPS_nnlo_30_ggH125_tautaul13l7             0.028302    0.43353  1.0  0.0 0.0 Powheg+Pythia8(v8.212)+EvtGen(v1.6.0)
345121 PowhegPy8EG_NNLOPS_nnlo_30_ggH125_tautaulm15hp20          0.028302    0.16617  1.0  0.0 0.0 Powheg+Pythia8(v8.212)+EvtGen(v1.6.0)
345122 PowhegPy8EG_NNLOPS_nnlo_30_ggH125_tautaulp15hm20          0.028301    0.16603  1.0  0.0 0.0 Powheg+Pythia8(v8.212)+EvtGen(v1.6.0)
345123 PowhegPy8EG_NNLOPS_nnlo_30_ggH125_tautauh30h20            0.028302    0.45032  1.0  0.0 0.0 Powheg+Pythia8(v8.212)+EvtGen(v1.6.0)

345324 PowhegPythia8EvtGen_NNLOPS_NN30_ggH125_WWlvlv_EF_15_5     0.028301    0.49374  1.0  0.0 0.0 Powheg+Pythia8(v8.212)+EvtGen(v1.2.0)

# VBF
346190 PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_tautaul13l7      0.0037476   0.46579  1.0  0.0 0.0 Powheg+Pythia8(v.230)+EvtGen
346191 PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_tautaulm15hp20   0.0037474   0.17647  1.0  0.0 0.0 Powheg+Pythia8(v.230)+EvtGen
346192 PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_tautaulp15hm20   0.0037473   0.17659  1.0  0.0 0.0 Powheg+Pythia8(v.230)+EvtGen
346193 PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_tautauh30h20     0.0037476   0.47297  1.0  0.0 0.0 Powheg+Pythia8(v.230)+EvtGen

345948 PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_WWlvlv           0.0037467   0.51037  1.0  0.0 0.0 Powheg+Pythia8(v.230)+EvtGen(v.1.6.0)

# ttH
345940 aMcAtNloPythia8EvtGen_ttH_dilep                           0.0542260   1.0000   1.10 0.0 0.0 MadGraph5_aMC@NLO(v2.6.0)+EvtGen(v1.6.0)

346343 PhPy8EG_A14NNPDF23_NNPDF30ME_ttH125_allhad                0.00023844  1        1.0  0.0 0.0 Powheg+Pythia8(v.230)+EvtGen(v.1.6.0)
346344 PhPy8EG_A14NNPDF23_NNPDF30ME_ttH125_semilep               0.00052458  0.43844  1.0  0.0 0.0 Powheg+Pythia8(v.230)+EvtGen(v.1.6.0)
346345 PhPy8EG_A14NNPDF23_NNPDF30ME_ttH125_dilep                 0.000054667 1        1.0  0.0 0.0 Powheg+Pythia8(v.230)+EvtGen(v.1.6.0)

################################################################################
## DiHiggs
################################################################################
342620 aMcAtNloHerwigppEvtGen_UEEE5_CTEQ6L1_CT10ME_hh_yybb       0.00003102  0.002660 1.0  0.0 0.0 MadGraph5_aMC@NLO(v2.2.3.p1)+Herwigpp(v2.7.1)+EvtGen(v1.2.0)
345835 aMcAtNloHerwig7EvtGen_UEEE5_CTEQ6L1_CT10ME_hh_ttbb_hh     0.000027411 0.15606  1.0  0.0 0.0 aMcAtNlo+Herwig7+EvtGen
345836 aMcAtNloHerwig7EvtGen_UEEE5_CTEQ6L1_CT10ME_hh_ttbb_lh     0.0000274   0.1446   1.0  0.0 0.0 aMcAtNlo+Herwig7+EvtGen
346218 aMcAtNloHerwig7EvtGen_UEEE5_CTEQ6L1_CT10ME_hh_WWbb_ll     0.00003102  0.01647  1.0  0.0 0.0 aMcAtNlo+Herwig7+EvtGen(v.1.6.0)
450030 aMcAtNloHerwig7EvtGen_H7UE_MMHT2014lo68cl_CT10_hh_ttbb_ll 0.00003102  0.005355 1.0  0.0 0.0 aMcAtNlo+Herwig7+EvtGen
450572 aMcAtNloHerwig7EvtGen_H7UEMMHT_CT10ME_hh_bbZZ_llvv        0.00003102  0.000424 1.0  0.0 0.0 aMcAtNlo+Herwig7+EvtGen
450579 aMcAtNloHerwig7EvtGen_H7UEMMHT_CT10ME_hh_bbWW             0.000027347 0.21963  1.0  0.0 0.0 aMcAtNlo+Herwig7+EvtGen

450624 MadGraphHerwig7EvtGen_NNPDF23LO_X251tohh_WWbb_2lep        0.079734    0.01647  1.0  0.0 0.0 MadGraph.2.6.1+Herwig7.7.1.3+EvtGen.1.6.0
450625 MadGraphHerwig7EvtGen_NNPDF23LO_X260tohh_WWbb_2lep        0.22451     0.01674  1.0  0.0 0.0 MadGraph.2.6.1+Herwig7.7.1.3+EvtGen.1.6.0
450626 MadGraphHerwig7EvtGen_NNPDF23LO_X300tohh_WWbb_2lep        0.32681     0.01674  1.0  0.0 0.0 MadGraph.2.6.1+Herwig7.7.1.3+EvtGen.1.6.0
450627 MadGraphHerwig7EvtGen_NNPDF23LO_X400tohh_WWbb_2lep        0.33641     0.01674  1.0  0.0 0.0 MadGraph.2.6.1+Herwig7.7.1.3+EvtGen.1.6.0
450628 MadGraphHerwig7EvtGen_NNPDF23LO_X500tohh_WWbb_2lep        0.1454      0.01674  1.0  0.0 0.0 MadGraph.2.6.1+Herwig7.7.1.3+EvtGen.1.6.0
450629 MadGraphHerwig7EvtGen_NNPDF23LO_X600tohh_WWbb_2lep        0.057085    0.01674  1.0  0.0 0.0 MadGraph.2.6.1+Herwig7.7.1.3+EvtGen.1.6.0
450630 MadGraphHerwig7EvtGen_NNPDF23LO_X800tohh_WWbb_2lep        0.010293    0.01674  1.0  0.0 0.0 MadGraph.2.6.1+Herwig7.7.1.3+EvtGen.1.6.0
450631 MadGraphHerwig7EvtGen_NNPDF23LO_X1000tohh_WWbb_2lep       0.0023589   0.01674  1.0  0.0 0.0 MadGraph.2.6.1+Herwig7.7.1.3+EvtGen.1.6.0

450652 MadGraphHerwig7EvtGen_NNPDF23LO_X251tohh_bbZZllvv         0.079519    0.000424 1.0  0.0 0.0 MadGraph.2.6.1+Herwig7.7.1.3+EvtGen.1.6.0
450653 MadGraphHerwig7EvtGen_NNPDF23LO_X260tohh_bbZZllvv         0.22451     0.000424 1.0  0.0 0.0 MadGraph.2.6.1+Herwig7.7.1.3+EvtGen.1.6.0
450654 MadGraphHerwig7EvtGen_NNPDF23LO_X300tohh_bbZZllvv         0.32681     0.000424 1.0  0.0 0.0 MadGraph.2.6.1+Herwig7.7.1.3+EvtGen.1.6.0
450655 MadGraphHerwig7EvtGen_NNPDF23LO_X400tohh_bbZZllvv         0.33641     0.000424 1.0  0.0 0.0 MadGraph.2.6.1+Herwig7.7.1.3+EvtGen.1.6.0
450656 MadGraphHerwig7EvtGen_NNPDF23LO_X500tohh_bbZZllvv         0.1454      0.000424 1.0  0.0 0.0 MadGraph.2.6.1+Herwig7.7.1.3+EvtGen.1.6.0
450657 MadGraphHerwig7EvtGen_NNPDF23LO_X600tohh_bbZZllvv         0.057085    0.000424 1.0  0.0 0.0 MadGraph.2.6.1+Herwig7.7.1.3+EvtGen.1.6.0
450658 MadGraphHerwig7EvtGen_NNPDF23LO_X800tohh_bbZZllvv         0.010293    0.000424 1.0  0.0 0.0 MadGraph.2.6.1+Herwig7.7.1.3+EvtGen.1.6.0
450659 MadGraphHerwig7EvtGen_NNPDF23LO_X1000tohh_bbZZllvv        0.0023589   0.000424 1.0  0.0 0.0 MadGraph.2.6.1+Herwig7.7.1.3+EvtGen.1.6.0
