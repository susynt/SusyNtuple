#!/bin/env python

import os
import sys
import glob
import subprocess
import argparse

tar_file = '/data/uclhc/uci/user/dantrim/n0303val/area.tgz'
tarred_dir = 'susynt-read'

out_dir = '/data/uclhc/uci/user/dantrim/ntuples/n0301/sumw_files/'
log_dir = '/data/uclhc/uci/user/dantrim/ntuples/n0301/sumw_files/'

filelist_dir = '/data/uclhc/uci/user/dantrim/n0303val/susynt-read/filelists/'
samples = [ 'n0303_mc16a', 'n0303_mc16d' ]

do_brick = True
do_gp = True
do_uc = True

def get_samples(args) :

    global samples

    if args.sample != '' :
        user_list = args.sample.split(',')
        return user_list 
    return samples

def bool_string(boolean) :

    return { True : 'true', False : 'false' } [ boolean ]

def check_samples(sample_names, filelist_dir) :

    all_ok = True
    for name in sample_names :
        name_loc = filelist_dir + '/' + name
        if not os.path.isdir(name_loc) :
            print 'ERROR Could not find sample \"%s\" in filelist directory (filelist dir = %s)' (name, filelist_dir)
            all_ok = False
    if not all_ok :
        print 'Exitting with ERROR'
        sys.exit()

def make_condor_file(sample, txt_files, condor_filename, exec_name) :

    global tar_file

    with open(condor_filename, 'w') as f :
        f.write('universe = vanilla\n')
        f.write('+local=%s\n' % bool_string(do_brick))
        f.write('+site_local=%s\n' % bool_string(do_gp))
        f.write('+uc=%s\n' % bool_string(do_uc))
        f.write('+sdsc=false\n')
        f.write('executable = %s\n' % exec_name)
        f.write('should_transfer_files = YES\n')
        f.write('transfer_input_files = %s\n' % tar_file)
        f.write('use_x509userproxy = True\n')
        f.write('notification = Never\n')

        for txt_file in txt_files :

            injob_filelist = './' + filelist_dir[filelist_dir.find(tarred_dir):] + '/'
            injob_filelist += sample
            injob_filelist += '/%s' % txt_file.split('/')[-1]

            log_base = txt_file.split('/')[-1].replace('.txt', '')

            arg_string = ' %s %s %s ' % (tarred_dir, injob_filelist, txt_file)

            f.write('\n')
            f.write('arguments = %s\n' % arg_string)
            f.write('output = log_%s.out\n' % log_base)
            f.write('log = log_%s.log\n' % log_base)
            f.write('error = log_%s.err\n' % log_base)
            f.write('queue\n')

def make_executable(exec_name) :

    with open(exec_name, 'w') as f :
        f.write('#!/bin/bash\n\n\n')
        f.write('echo "----------- %s ----------"\n' % exec_name)
        f.write('hostname\n')
        f.write('echo "start: `date`"\n')
        f.write('echo "input arguments:"\n')
        f.write('tarred_dir=${1}\n')
        f.write('injob_filelist=${2}\n')
        f.write('sample_list=${3}\n')
        f.write('echo "   tarred directory      : ${tarred_dir}"\n')
        f.write('echo "   injob filelist loc    : ${injob_filelist}"\n')
        f.write('echo "   sample list           : ${sample_list}"\n')
        f.write('while (( "$#" )); do\n')
        f.write('     shift\n')
        f.write('done\n\n')
        f.write('echo "untarring area.tgz"\n')
        f.write('tar -xf area.tgz\n\n')
        f.write('echo "current directory structure:"\n')
        f.write('ls -ltrh\n')
        f.write('export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase\n')
        f.write('source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh\n')
        f.write('echo "moving"\n')
        f.write('pushd ${tarred_dir}\n')
        f.write('echo "current directory structure:"\n')
        f.write('ls -ltrh\n')
        f.write('lsetup fax\n')
        f.write('asetup AnalysisBase,21.2.55\n')
        f.write('source build/x86*/setup.sh\n')
        f.write('echo "moving"\n')
        f.write('popd\n')
        f.write('echo "current directory structure:"\n')
        f.write('ls -ltrh\n')
        f.write('echo "calling: grabSumw -i ${injob_filelist}"\n')
        f.write('grabSumw -i ${injob_filelist}\n')
        f.write('echo "final directory structure:"\n')
        f.write('ls -ltrh\n')
        f.write('echo "finish: `date`"\n')

def submit_job(sample, verbose) :

    txt_files_for_sample = glob.glob(filelist_dir + '/' + sample + '/*.txt')
    n_txt = len(txt_files_for_sample)

    if verbose :
        print 'Found %d text files for sample %s' % (n_txt, sample)

    if n_txt == 0 :
        print 'WARNING Found not text files for sample %s, skipping this sample' % sample
        return

    txt_files = [os.path.abspath(s) for s in txt_files_for_sample]

    condor_filename = 'submit_sumw_%s.condor' % sample
    exec_name = 'run_condor_sumw_%s.sh' % sample
    make_condor_file(sample, txt_files_for_sample, condor_filename, exec_name)
    make_executable(exec_name)

    cmd = 'condor_submit %s' % condor_filename
    subprocess.call(cmd, shell = True)

def main() :

    parser = argparse.ArgumentParser( description = 'Launch jobs to get sumw ROOT files' )
    parser.add_argument('-s', '--sample', default = '',
        help = 'Override samples list by providing a sample name (can be comma-separated-list)')
    parser.add_argument('-v', '--verbose', default = False, action = 'store_true',
        help = 'Turn on verbose mode')
    args = parser.parse_args()

    samples = get_samples(args)
    print samples
    check_samples(samples, filelist_dir)

    n_samples = len(samples)
    for isample, sample in enumerate(samples) :
        print '\n[%0d/%02d] Submitting %s' % (isample+1, n_samples, sample)
        submit_job(sample, args.verbose)

#__________________________
if __name__ == '__main__' :
    main()


