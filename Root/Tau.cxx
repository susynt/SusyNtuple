#include "SusyNtuple/Tau.h"

#include <iostream>
#include <iomanip>

using namespace std;
using Susy::Tau;
using Susy::Particle;

/*--------------------------------------------------------------------------------*/
// Copy constructor
/*--------------------------------------------------------------------------------*/
Tau::Tau(const Tau &rhs):
    Particle(rhs),
    q(rhs.q),
    nTrack(rhs.nTrack),
    loose(rhs.loose),
    medium(rhs.medium),
    tight(rhs.tight),
    isTruthMatched(rhs.isTruthMatched),
    truthPdgId(rhs.truthPdgId),
    truthNProngs(rhs.truthNProngs),
    truthCharge(rhs.truthCharge),
    isHadronicTau(rhs.isHadronicTau),
    truthType(rhs.truthType),
    truthOrigin(rhs.truthOrigin),
    looseEffSF(rhs.looseEffSF),
    errLooseEffSF(rhs.errLooseEffSF),
    mediumEffSF(rhs.mediumEffSF),
    errMediumEffSF(rhs.errMediumEffSF),
    tightEffSF(rhs.tightEffSF),
    errTightEffSF(rhs.errTightEffSF),
    tau_sme_tes_det_up(rhs.tau_sme_tes_det_up),
    tau_sme_tes_det_dn(rhs.tau_sme_tes_det_dn),
    tau_sme_tes_insitu_up(rhs.tau_sme_tes_insitu_up),
    tau_sme_tes_insitu_dn(rhs.tau_sme_tes_insitu_dn),
    tau_sme_tes_model_up(rhs.tau_sme_tes_model_up),
    tau_sme_tes_model_dn(rhs.tau_sme_tes_model_dn),
    trigFlags(rhs.trigFlags)
{
}
/*--------------------------------------------------------------------------------*/
// Assignment operator
/*--------------------------------------------------------------------------------*/
Tau& Tau::operator=(const Tau &rhs)
{
    if (this != &rhs) {
        Particle::operator=(rhs);
        q = rhs.q;
        nTrack = rhs.nTrack;
        loose = rhs.loose;
        medium = rhs.medium;
        tight = rhs.tight;
        isTruthMatched = rhs.isTruthMatched;
        truthPdgId = rhs.truthPdgId;
        truthNProngs = rhs.truthNProngs;
        truthCharge = rhs.truthCharge;
        isHadronicTau = rhs.isHadronicTau;
        truthType = rhs.truthType;
        truthOrigin = rhs.truthOrigin;
        looseEffSF = rhs.looseEffSF;
        errLooseEffSF = rhs.errLooseEffSF;
        mediumEffSF = rhs.mediumEffSF;
        errMediumEffSF = rhs.errMediumEffSF;
        tightEffSF = rhs.tightEffSF;
        errTightEffSF = rhs.errTightEffSF;
        tau_sme_tes_det_up = rhs.tau_sme_tes_det_up;
        tau_sme_tes_det_dn = rhs.tau_sme_tes_det_dn;
        tau_sme_tes_insitu_up = rhs.tau_sme_tes_insitu_up;
        tau_sme_tes_insitu_dn = rhs.tau_sme_tes_insitu_dn;
        tau_sme_tes_model_up = rhs.tau_sme_tes_model_up;
        tau_sme_tes_model_dn = rhs.tau_sme_tes_model_dn;
        trigFlags = rhs.trigFlags;
    }
    return *this;
}
/*--------------------------------------------------------------------------------*/
// Tau Set State
/*--------------------------------------------------------------------------------*/
void Tau::setState(int sys)
{
    resetTLV();
    if(sys == NtSys::NOM) return;
    
    float sf = 0;
    if     ( sys == NtSys::TAU_SME_TES_DET_DN ) sf = tau_sme_tes_det_dn;
    else if( sys == NtSys::TAU_SME_TES_DET_UP ) sf = tau_sme_tes_det_up;
    else if( sys == NtSys::TAU_SME_TES_INSITU_DN ) sf = tau_sme_tes_insitu_dn;
    else if( sys == NtSys::TAU_SME_TES_INSITU_UP ) sf = tau_sme_tes_insitu_up;
    else if( sys == NtSys::TAU_SME_TES_MODEL_DN ) sf = tau_sme_tes_model_dn;
    else if( sys == NtSys::TAU_SME_TES_MODEL_UP ) sf = tau_sme_tes_model_up;
    else return;
    
    this->SetPtEtaPhiE(sf * this->Pt(), this->Eta(), this->Phi(), sf * this->E());
}
/*--------------------------------------------------------------------------------*/
// Tau print
/*--------------------------------------------------------------------------------*/
void Tau::print() const
{
    cout.precision(2);
    cout << fixed << "Tau : q " << setw(2) << q << " Et " << setw(6) << Et() << " eta " << setw(5) << Eta()
         << " nTrk " << nTrack
         << " phi " << setw(5) << Phi()
         << " truthMatched " << isTruthMatched
         << endl;
    cout.precision(6);
    cout.unsetf(ios_base::fixed);
}
