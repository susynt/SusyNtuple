#include "SusyNtuple/AnalysisType.h"

namespace Susy
{

std::string AnalysisType2str(const AnalysisType &a)
{
    std::string out;
    switch(a) {
    case AnalysisType::Ana_2Lep     : out = "Ana_2Lep"     ; break;
    case AnalysisType::Ana_Stop2L   : out = "Ana_Stop2L"   ; break;
    case AnalysisType::Ana_HLFV     : out = "Ana_HLFV"     ; break;
    case AnalysisType::kUnknown     : out = "Unknown"      ; break;
    }
    return out;    
}

} // namespace Susy
