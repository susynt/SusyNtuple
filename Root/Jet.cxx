#include "SusyNtuple/Jet.h"

#include <iostream>
#include <iomanip>

using namespace std;

using Susy::Jet;
using Susy::Particle;

/*--------------------------------------------------------------------------------*/
// Copy constructor
/*--------------------------------------------------------------------------------*/
Jet::Jet(const Jet &rhs):
    Particle(rhs),
    idx(rhs.idx),
    jvt(rhs.jvt),
    detEta(rhs.detEta),
    emfrac(rhs.emfrac),
    truthLabel(rhs.truthLabel),
    matchTruth(rhs.matchTruth),
    nTracks(rhs.nTracks),
    sumTrkPt(rhs.sumTrkPt),
    bjet(rhs.bjet),
    effscalefact(rhs.effscalefact),
    btagSF_mv2c10_77(rhs.btagSF_mv2c10_77),
    btagSF_mv2c10_85(rhs.btagSF_mv2c10_85),
    btagSF_dl1_77(rhs.btagSF_dl1_77),
    btagSF_dl1_85(rhs.btagSF_dl1_85),
    btagNotSF_mv2c10_77(rhs.btagNotSF_mv2c10_77),
    btagNotSF_mv2c10_85(rhs.btagNotSF_mv2c10_85),
    btagNotSF_dl1_77(rhs.btagNotSF_dl1_77),
    btagNotSF_dl1_85(rhs.btagNotSF_dl1_85),
    mv2c10(rhs.mv2c10),
    dl1(rhs.dl1),
    jvtEff(rhs.jvtEff),
    jvtEff_up(rhs.jvtEff_up),
    jvtEff_dn(rhs.jvtEff_dn),
    isBad(rhs.isBad),
    jer(rhs.jer),
    jer_datamc_up(rhs.jer_datamc_up),
    jer_datamc_dn(rhs.jer_datamc_dn),
    jer_effective_np1_up(rhs.jer_effective_np1_up),
    jer_effective_np1_dn(rhs.jer_effective_np1_dn),
    jer_effective_np2_up(rhs.jer_effective_np2_up),
    jer_effective_np2_dn(rhs.jer_effective_np2_dn),
    jer_effective_np3_up(rhs.jer_effective_np3_up),
    jer_effective_np3_dn(rhs.jer_effective_np3_dn),
    jer_effective_np4_up(rhs.jer_effective_np4_up),
    jer_effective_np4_dn(rhs.jer_effective_np4_dn),
    jer_effective_np5_up(rhs.jer_effective_np5_up),
    jer_effective_np5_dn(rhs.jer_effective_np5_dn),
    jer_effective_np6_up(rhs.jer_effective_np6_up),
    jer_effective_np6_dn(rhs.jer_effective_np6_dn),
    jer_effective_np7rest_up(rhs.jer_effective_np7rest_up),
    jer_effective_np7rest_dn(rhs.jer_effective_np7rest_dn),
    eta_intercal_up(rhs.eta_intercal_up),
    eta_intercal_dn(rhs.eta_intercal_dn),
    groupedNP(rhs.groupedNP),
    jerNP(rhs.jerNP),
    jesNP(rhs.jesNP),
    FTSys(rhs.FTSys)
{ 
}
/*--------------------------------------------------------------------------------*/
// Assignment operator
/*--------------------------------------------------------------------------------*/
Jet& Jet::operator=(const Jet &rhs)
{
  if (this != &rhs) {
    Particle::operator=(rhs);
    idx  = rhs.idx;
    jvt  = rhs.jvt; 
    detEta = rhs.detEta;
    emfrac = rhs.emfrac;
    truthLabel = rhs.truthLabel;
    matchTruth = rhs.matchTruth;
    bjet = rhs.bjet;
    effscalefact = rhs.effscalefact;
    btagSF_mv2c10_77 = rhs.btagSF_mv2c10_77;
    btagSF_mv2c10_85 = rhs.btagSF_mv2c10_85;
    btagSF_dl1_77 = rhs.btagSF_dl1_77;
    btagSF_dl1_85 = rhs.btagSF_dl1_85;
    btagNotSF_mv2c10_77 = rhs.btagNotSF_mv2c10_77;
    btagNotSF_mv2c10_85 = rhs.btagNotSF_mv2c10_85;
    btagNotSF_dl1_77 = rhs.btagNotSF_dl1_77;
    btagNotSF_dl1_85 = rhs.btagNotSF_dl1_85;
    nTracks = rhs.nTracks;
    sumTrkPt = rhs.sumTrkPt;
    mv2c10 = rhs.mv2c10;
    dl1 = rhs.dl1;
    jvtEff = rhs.jvtEff;
    jvtEff_up = rhs.jvtEff_up;
    jvtEff_dn = rhs.jvtEff_dn;
    isBad = rhs.isBad;
    jer = rhs.jer;
    jer_datamc_up = rhs.jer_datamc_up;
    jer_datamc_dn = rhs.jer_datamc_dn;
    jer_effective_np1_up = rhs.jer_effective_np1_up;
    jer_effective_np1_dn = rhs.jer_effective_np1_dn;
    jer_effective_np2_up = rhs.jer_effective_np2_up;
    jer_effective_np2_dn = rhs.jer_effective_np2_dn;
    jer_effective_np3_up = rhs.jer_effective_np3_up;
    jer_effective_np3_dn = rhs.jer_effective_np3_dn;
    jer_effective_np4_up = rhs.jer_effective_np4_up;
    jer_effective_np4_dn = rhs.jer_effective_np4_dn;
    jer_effective_np5_up = rhs.jer_effective_np5_up;
    jer_effective_np5_dn = rhs.jer_effective_np5_dn;
    jer_effective_np6_up = rhs.jer_effective_np6_up;
    jer_effective_np6_dn = rhs.jer_effective_np6_dn;
    jer_effective_np7rest_up = rhs.jer_effective_np7rest_up;
    jer_effective_np7rest_dn = rhs.jer_effective_np7rest_dn;
    eta_intercal_up = rhs.eta_intercal_up;
    eta_intercal_dn = rhs.eta_intercal_dn;
    groupedNP = rhs.groupedNP;
    jerNP = rhs.jerNP;
    jesNP = rhs.jesNP;
    FTSys = rhs.FTSys;
  }
  return *this;
}
/*--------------------------------------------------------------------------------*/
// Jet Set State
/*--------------------------------------------------------------------------------*/
void Jet::setState(int sys)
{
    resetTLV();
    if(sys == NtSys::NOM) return;
    
    float sf = 0;
    // JER
    if(      sys == NtSys::JET_JER_DataVsMC_UP) sf = jerNP.at(0);
    else if( sys == NtSys::JET_JER_DataVsMC_DN) sf = jerNP.at(1);
    else if( sys == NtSys::JET_JER_EffectiveNP_1_UP) sf= jerNP.at(2);
    else if( sys == NtSys::JET_JER_EffectiveNP_1_DN) sf= jerNP.at(3);
    else if( sys == NtSys::JET_JER_EffectiveNP_2_UP) sf= jerNP.at(4);
    else if( sys == NtSys::JET_JER_EffectiveNP_2_DN) sf= jerNP.at(5);
    else if( sys == NtSys::JET_JER_EffectiveNP_3_UP) sf= jerNP.at(6);
    else if( sys == NtSys::JET_JER_EffectiveNP_3_DN) sf= jerNP.at(7);
    else if( sys == NtSys::JET_JER_EffectiveNP_4_UP) sf= jerNP.at(8);
    else if( sys == NtSys::JET_JER_EffectiveNP_4_DN) sf= jerNP.at(9);
    else if( sys == NtSys::JET_JER_EffectiveNP_5_UP) sf= jerNP.at(10);
    else if( sys == NtSys::JET_JER_EffectiveNP_5_DN) sf= jerNP.at(11);
    else if( sys == NtSys::JET_JER_EffectiveNP_6_UP) sf= jerNP.at(12);
    else if( sys == NtSys::JET_JER_EffectiveNP_6_DN) sf= jerNP.at(13);
    else if( sys == NtSys::JET_JER_EffectiveNP_7_UP) sf= jerNP.at(14);
    else if( sys == NtSys::JET_JER_EffectiveNP_7_DN) sf= jerNP.at(15);
    else if( sys == NtSys::JET_JER_EffectiveNP_7rest_UP) sf= jerNP.at(16);
    else if( sys == NtSys::JET_JER_EffectiveNP_7rest_DN) sf= jerNP.at(17);
    else if( sys == NtSys::JET_JER_EffectiveNP_8_UP) sf= jerNP.at(18);
    else if( sys == NtSys::JET_JER_EffectiveNP_8_DN) sf= jerNP.at(19);
    else if( sys == NtSys::JET_JER_EffectiveNP_9_UP) sf= jerNP.at(20);
    else if( sys == NtSys::JET_JER_EffectiveNP_9_DN) sf= jerNP.at(21);
    else if( sys == NtSys::JET_JER_EffectiveNP_10_UP) sf= jerNP.at(22);
    else if( sys == NtSys::JET_JER_EffectiveNP_10_DN) sf= jerNP.at(23);
    else if( sys == NtSys::JET_JER_EffectiveNP_11_UP) sf= jerNP.at(24);
    else if( sys == NtSys::JET_JER_EffectiveNP_11_DN) sf= jerNP.at(25);
    else if( sys == NtSys::JET_JER_EffectiveNP_12rest_UP) sf= jerNP.at(26);
    else if( sys == NtSys::JET_JER_EffectiveNP_12rest_DN) sf= jerNP.at(27);

    // JES
    else if( sys == NtSys::JET_EffectiveNP_1_UP) sf = jesNP.at(0);
    else if( sys == NtSys::JET_EffectiveNP_1_DN) sf = jesNP.at(1);
    else if( sys == NtSys::JET_EffectiveNP_2_UP) sf = jesNP.at(2);
    else if( sys == NtSys::JET_EffectiveNP_2_DN) sf = jesNP.at(3);
    else if( sys == NtSys::JET_EffectiveNP_3_UP) sf = jesNP.at(4);
    else if( sys == NtSys::JET_EffectiveNP_3_DN) sf = jesNP.at(5);
    else if( sys == NtSys::JET_EffectiveNP_4_UP) sf = jesNP.at(6);
    else if( sys == NtSys::JET_EffectiveNP_4_DN) sf = jesNP.at(7);
    else if( sys == NtSys::JET_EffectiveNP_5_UP) sf = jesNP.at(8);
    else if( sys == NtSys::JET_EffectiveNP_5_DN) sf = jesNP.at(9);
    else if( sys == NtSys::JET_EffectiveNP_6_UP) sf = jesNP.at(10);
    else if( sys == NtSys::JET_EffectiveNP_6_DN) sf = jesNP.at(11);
    else if( sys == NtSys::JET_EffectiveNP_7_UP) sf = jesNP.at(12);
    else if( sys == NtSys::JET_EffectiveNP_7_DN) sf = jesNP.at(13);
    else if( sys == NtSys::JET_EffectiveNP_8rest_UP) sf = jesNP.at(14);
    else if( sys == NtSys::JET_EffectiveNP_8rest_DN) sf = jesNP.at(15);
    else if( sys == NtSys::JET_EtaIntercalibration_Modelling_UP) sf = jesNP.at(16);
    else if( sys == NtSys::JET_EtaIntercalibration_Modelling_DN) sf = jesNP.at(17);
    else if( sys == NtSys::JET_EtaIntercalibration_NonClosure_highE_UP) sf = jesNP.at(18);
    else if( sys == NtSys::JET_EtaIntercalibration_NonClosure_highE_DN) sf = jesNP.at(19);
    else if( sys == NtSys::JET_EtaIntercalibration_NonClosure_negEta_UP) sf = jesNP.at(20);
    else if( sys == NtSys::JET_EtaIntercalibration_NonClosure_negEta_DN) sf = jesNP.at(21);
    else if( sys == NtSys::JET_EtaIntercalibration_NonClosure_posEta_UP) sf = jesNP.at(22);
    else if( sys == NtSys::JET_EtaIntercalibration_NonClosure_posEta_DN) sf = jesNP.at(23);
    else if( sys == NtSys::JET_EtaIntercalibration_TotalStat_UP) sf = jesNP.at(24);
    else if( sys == NtSys::JET_EtaIntercalibration_TotalStat_DN) sf = jesNP.at(25);
    else if( sys == NtSys::JET_Flavor_Composition_UP) sf = jesNP.at(26);
    else if( sys == NtSys::JET_Flavor_Composition_DN) sf = jesNP.at(27);
    else if( sys == NtSys::JET_Flavor_Response_UP) sf = jesNP.at(28);
    else if( sys == NtSys::JET_Flavor_Response_DN) sf = jesNP.at(29);
    else if( sys == NtSys::JET_BJES_Response_UP) sf = jesNP.at(30);
    else if( sys == NtSys::JET_BJES_Response_DN) sf = jesNP.at(31);
    else if( sys == NtSys::JET_Pileup_OffsetMu_UP) sf = jesNP.at(32);
    else if( sys == NtSys::JET_Pileup_OffsetMu_DN) sf = jesNP.at(33);
    else if( sys == NtSys::JET_Pileup_OffsetNPV_UP) sf = jesNP.at(34);
    else if( sys == NtSys::JET_Pileup_OffsetNPV_DN) sf = jesNP.at(35);
    else if( sys == NtSys::JET_Pileup_PtTerm_UP) sf = jesNP.at(36);
    else if( sys == NtSys::JET_Pileup_PtTerm_DN) sf = jesNP.at(37);
    else if( sys == NtSys::JET_Pileup_RhoTopology_UP) sf = jesNP.at(38);
    else if( sys == NtSys::JET_Pileup_RhoTopology_DN) sf = jesNP.at(39);
    else if( sys == NtSys::JET_PunchThrough_MC16_UP) sf = jesNP.at(40);
    else if( sys == NtSys::JET_PunchThrough_MC16_DN) sf = jesNP.at(41);
    else if( sys == NtSys::JET_SingleParticle_HighPt_UP) sf = jesNP.at(42);
    else if( sys == NtSys::JET_SingleParticle_HighPt_DN) sf = jesNP.at(43);
    
    else if( sys == NtSys::JET_GroupedNP_1_UP) sf = groupedNP[0];
    else if( sys == NtSys::JET_GroupedNP_1_DN) sf = groupedNP[1];
    else if( sys == NtSys::JET_GroupedNP_2_UP) sf = groupedNP[2];
    else if( sys == NtSys::JET_GroupedNP_2_DN) sf = groupedNP[3];
    else if( sys == NtSys::JET_GroupedNP_3_UP) sf = groupedNP[4];
    else if( sys == NtSys::JET_GroupedNP_3_DN) sf = groupedNP[5];
    else if( sys == NtSys::JET_EtaIntercalibration_UP) sf = eta_intercal_up;
    else if( sys == NtSys::JET_EtaIntercalibration_DN) sf = eta_intercal_dn;
    else return;
    
    this->SetPtEtaPhiE(sf * this->Pt(), this->Eta(), this->Phi(), sf * this->E());
}
/*--------------------------------------------------------------------------------*/
// Flavor systematics
/*--------------------------------------------------------------------------------*/
float Jet::getFTSys(Susy::NtSys::SusyNtSys sys)
{
    float s= 1;

    if      ( sys == NtSys::FT_EFF_B_systematics_DN ) s= FTSys[0];
    else if ( sys == NtSys::FT_EFF_B_systematics_UP ) s= FTSys[1];
    else if ( sys == NtSys::FT_EFF_C_systematics_DN ) s= FTSys[2];
    else if ( sys == NtSys::FT_EFF_C_systematics_UP ) s= FTSys[3];
    else if ( sys == NtSys::FT_EFF_Light_systematics_DN ) s= FTSys[4];
    else if ( sys == NtSys::FT_EFF_Light_systematics_UP ) s= FTSys[5];
    else if ( sys == NtSys::FT_EFF_extrapolation_DN ) s= FTSys[6];
    else if ( sys == NtSys::FT_EFF_extrapolation_UP ) s= FTSys[7];
    else if ( sys == NtSys::FT_EFF_extrapolation_charm_DN ) s= FTSys[8];
    else if ( sys == NtSys::FT_EFF_extrapolation_charm_UP ) s= FTSys[9];
    return s;

}

void Jet::setFTSys(Susy::NtSys::SusyNtSys sys, double scale=0.)
{
    if      ( sys == NtSys::FT_EFF_B_systematics_DN ) FTSys[0] =scale;
    else if ( sys == NtSys::FT_EFF_B_systematics_UP ) FTSys[1] =scale;
    else if ( sys == NtSys::FT_EFF_C_systematics_DN ) FTSys[2] =scale;
    else if ( sys == NtSys::FT_EFF_C_systematics_UP ) FTSys[3] =scale;
    else if ( sys == NtSys::FT_EFF_Light_systematics_DN ) FTSys[4] =scale;
    else if ( sys == NtSys::FT_EFF_Light_systematics_UP ) FTSys[5] =scale;
    else if ( sys == NtSys::FT_EFF_extrapolation_DN ) FTSys[6] =scale;
    else if ( sys == NtSys::FT_EFF_extrapolation_UP ) FTSys[7] =scale;
    else if ( sys == NtSys::FT_EFF_extrapolation_charm_DN ) FTSys[8] =scale;
    else if ( sys == NtSys::FT_EFF_extrapolation_charm_UP ) FTSys[9] =scale;
    return;
}


/*--------------------------------------------------------------------------------*/
// Jet print
/*--------------------------------------------------------------------------------*/
void Jet::print() const
{
  cout.precision(2);
  cout << fixed << "Jet : pt " << setw(6) << Pt() << " eta " << setw(5) << Eta()
       << " phi " << setw(5) << Phi()
       << " jvt " << setw(4) << jvt
       << " mv2c10 " << setw(5) << mv2c10 
       << " dl1 " << setw(5) << dl1
       // No way do we want to see this every time we print
       //<< " jer " << jer << " jes_up " << jes_up << " jes_dn " << jes_dn
       << endl;
  cout.precision(6);
  cout.unsetf(ios_base::fixed);
}

