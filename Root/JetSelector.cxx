#include "SusyNtuple/JetSelector.h"
#include "SusyNtuple/Jet.h"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <type_traits> // underlying_type

using Susy::NtSys::SusyNtSys;
using std::cout;
using std::endl;
using std::string;

namespace Susy {
//----------------------------------------------------------
JetSelector* JetSelector::build(const AnalysisType &a, bool verbose) {
    JetSelector *selector;
    // setFTagEff sets WP for regular and OR flavor tagging
    // Use setFTagOREff for setting OR WP specifically
    switch(a) {
    case AnalysisType::Ana_2Lep:
        selector = new JetSelector_2Lep();
        selector->setFTagAlgo(FTagAlgo::MV2c10);
        selector->setFTagEff(FTagEff::WP77);
        break;
    case AnalysisType::Ana_Stop2L:
        selector = new JetSelector_Stop2L();
        selector->setFTagAlgo(FTagAlgo::MV2c10);
        selector->setFTagEff(FTagEff::WP77);
        selector->setFTagOREff(FTagEff::WP85);
        break;
    case AnalysisType::Ana_HLFV : 
        selector = new JetSelector_HLFV(); break;
        selector->setFTagAlgo(FTagAlgo::MV2c10);
        selector->setFTagEff(FTagEff::WP85);
        break;
    default:
        cout<<"JetSelector::build(): unknown analysis type '"<<AnalysisType2str(a)<<"'"
            <<" returning vanilla JetSelector"<<endl;
        selector = new JetSelector();
    }
    selector->setVerbose(verbose);
    return selector;
}
//----------------------------------------------------------
JetSelector::JetSelector() :
    m_ftag_wp(FTagEff::WP77),
    m_ftagOR_wp(FTagEff::WP77),
    m_ftag_algo(FTagAlgo::MV2c10),
    m_systematic(NtSys::NOM),
    m_verbose(false) 
    {}
//----------------------------------------------------------
JetSelector& JetSelector::setSystematic(const NtSys::SusyNtSys &s) {
    m_systematic = s;
    return *this;
}
bool JetSelector::passFlavTagger(const Jet* jet, FTagAlgo tagger, FTagEff eff_wp) {
    float tagger_threshold = get_ftag_threshold(tagger, eff_wp);
    if(tagger_threshold<-99.) {
        cout << "JetSelector::isBJet    WARNING Invalid tagger thresold (tagger="<<FTagAlgo2str(tagger)<<", wp=" << FTagEff2str(eff_wp) << "), unable to tag jet!" << endl;
        return false;
    }

    bool pass_tagger = false;
    if(tagger==FTagAlgo::MV2c10)
        pass_tagger = (jet->mv2c10 > tagger_threshold);
    else if(tagger==FTagAlgo::DL1)
        pass_tagger = (jet->dl1 > tagger_threshold);
    return pass_tagger;
}

bool JetSelector::isBJet(const Jet* jet) {
    bool pass_pt = (jet->Pt()>20.0);
    bool pass_eta = (fabs(jet->Eta())<2.5);
    bool pass_jvt = passJvt(jet, /*bjet=*/true);
    bool pass_tagger = passFlavTagger(jet, ftagAlgo(), ftagEff());
    return ( pass_pt && pass_eta && pass_jvt && pass_tagger );
}

bool JetSelector::isBJetOR(const Jet* jet, bool useUpperPt) {
    bool pass_pt = (jet->Pt()>20.0 && (!useUpperPt || jet->Pt() < 100.0));
    bool pass_eta = (fabs(jet->Eta())<2.5);
    bool pass_jvt = passJvt(jet, /*bjet=*/true);
    bool pass_tagger = passFlavTagger(jet, ftagAlgo(), ftagOREff());
    return ( pass_pt && pass_eta && pass_jvt && pass_tagger );
}

float JetSelector::bjet_sf(const Jet* jet, bool ineff) {
    FTagEff eff_wp = ftagEff();
    FTagAlgo tagger = ftagAlgo();

    float sf = 1.0;

    if(tagger==FTagAlgo::MV2c10) {
        if(eff_wp==FTagEff::WP70) {
            cout << "JetSelector::bjet_sf    WARNING We do not store flavor tagging SF for 70% WP! (returning 1.0)" << endl;
            sf = 1.0;
        }
        else if(eff_wp==FTagEff::WP77) {
            sf = (ineff ? jet->btagNotSF_mv2c10_77 : jet->btagSF_mv2c10_77);
        }
        else if(eff_wp==FTagEff::WP85) {
            sf = (ineff ? jet->btagNotSF_mv2c10_85 : jet->btagSF_mv2c10_85);
        }
    } // MV2c10
    else if(tagger==FTagAlgo::DL1) {
        if(eff_wp==FTagEff::WP70) {
            cout << "JetSelector::bjet_sf    WARNING We do not store flavor tagging SF for 70% WP! (returning 1.0)" << endl;
            sf = 1.0;
        }
        else if(eff_wp==FTagEff::WP77) {
            sf = (ineff ? jet->btagNotSF_dl1_77 : jet->btagSF_dl1_77);
        }
        else if(eff_wp==FTagEff::WP85) {
            sf = (ineff ? jet->btagNotSF_dl1_85 : jet->btagSF_dl1_85);
        }
    } // DL1

    return sf;
}

float JetSelector::get_ftag_threshold(const FTagAlgo& tagger, const FTagEff& wp) {
    if(tagger==FTagAlgo::MV2c10) {
        if(wp==FTagEff::WP70) return mv2c10_70efficiency();
        else if(wp==FTagEff::WP77) return mv2c10_77efficiency();
        else if(wp==FTagEff::WP85) return mv2c10_85efficiency();
    }
    else if(tagger==FTagAlgo::DL1) {
        if(wp==FTagEff::WP70) return dl1_70efficiency();
        else if(wp==FTagEff::WP77) return dl1_77efficiency();
        else if(wp==FTagEff::WP85) return dl1_85efficiency();
    }
    return -101.;
}

bool JetSelector::isForward(const Jet* jet) {
    return jet != nullptr
        && jet->Pt() > 30.0
        && 2.4 < fabs(jet->detEta) && fabs(jet->detEta) < 4.5
        ;
}
//----------------------------------------------------------
// Threshold as recommended at 
// - https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PileupJetRecommendations
// - (OLD) https://twiki.cern.ch/twiki/bin/view/AtlasProtected/JVTCalibrationRel21
bool JetSelector::passJvt(const Jet* jet, bool bjet) {
    if (jet == nullptr) return false;
    
    float maxPt = bjet ? 60.0 : 120.0; //Default
    float maxEta = bjet ? 2.4 : 2.5;
    float minJVT = fabs(jet->Eta()) > 2.4 ? 0.11 : 0.59;
    
    return false
        || jet->Pt() > maxPt
        || fabs(jet->Eta()) > maxEta
        || jet->jvt > minJVT
        ;
}
size_t JetSelector::count_B_jets(const JetVector &jets) {
    return std::count_if(jets.begin(), jets.end(),
                         std::bind1st(std::mem_fun(&JetSelector::isBJet), this));
}
size_t JetSelector::count_F_jets(const JetVector &jets) {
    return std::count_if(jets.begin(), jets.end(),
                            std::bind1st(std::mem_fun(&JetSelector::isForward), this));
}

//----------------------------------------------------------
bool JetSelector::isBaseline(const Jet* jet) {
    return (jet != nullptr
            && jet->Pt() > 20.0
            );
}
bool JetSelector::isSignal(const Jet* jet) {
    return (isBaseline(jet)
           && passJvt(jet)
           );
}
bool JetSelector::isForJVTSF(const Jet* jet) {
    // Should be the same as the function applying the JVT cut
    // but without the JVT cut applied
    return (isBaseline(jet)
           //&& passJvt(jet)
           );

}
//----------------------------------------------------------

//----------------------------------------------------------
// begin JetSelector_Stop2L Ana_Stop2L
//----------------------------------------------------------
bool JetSelector_Stop2L::isBaseline(const Jet* jet) {
    return jet != nullptr
        && jet->Pt() > 20.0
        ;
}
bool JetSelector_Stop2L::isSignal(const Jet* jet) {
    return isBaseline(jet)
        && fabs(jet->Eta()) < 2.8
        && passJvt(jet)
        ;
}
bool JetSelector_Stop2L::isForJVTSF(const Jet* jet) {
    return isBaseline(jet)
        && fabs(jet->Eta()) < 2.8
        //&& passJvt(jet)
        ;
}

//----------------------------------------------------------
// begin JetSelector_HLFV Ana_HLFV
//----------------------------------------------------------
bool JetSelector_HLFV::isBaseline(const Jet* jet) {
    return jet != nullptr
        && jet->Pt() > 20.0
        && fabs(jet->Eta()) < 4.5
        && passJvt(jet)
        ;
}
bool JetSelector_HLFV::isSignal(const Jet* jet) {
    return isBaseline(jet);
}
bool JetSelector_HLFV::isForJVTSF(const Jet* jet) {
    return jet != nullptr
        && jet->Pt() > 20.0
        && fabs(jet->Eta()) < 4.5
        //&& passJvt(jet)
        ;
}

} // Susy namespace
