#include "SusyNtuple/MuonSelector.h"
#include "SusyNtuple/Muon.h"

#include <algorithm>
#include <cassert>
#include <iostream>

using Susy::NtSys::SusyNtSys;
using std::cout;
using std::endl;
using std::string;

namespace Susy {
//----------------------------------------------------------
MuonSelector* MuonSelector::build(const AnalysisType &a, bool verbose) {
    MuonSelector *s;
    switch(a) {
    case AnalysisType::Ana_2Lep   :
        s = new MuonSelector_2Lep();
        s->setSignalId(MuonId::Medium);
        s->setSignalIsolation(Isolation::GradientLoose);
        break;
    case AnalysisType::Ana_Stop2L :
        s = new MuonSelector_Stop2L();
        s->setSignalId(MuonId::Medium);
        s->setSignalIsolation(Isolation::FixedCutLoose);
        break;
    case AnalysisType::Ana_HLFV :
        s = new MuonSelector_HLFV();
        s->setSignalId(MuonId::Medium);
        s->setSignalIsolation(Isolation::FixedCutTightTrackOnly);
        break;
    default:
        cout<<"MuonSelector::build(): unknown analysis type '"<<AnalysisType2str(a)<<"'"
            <<" returning vanilla MuonSelector"<<endl;
        s = new MuonSelector();
    }
    s->setVerbose(verbose);
    return s;
}
//----------------------------------------------------------
MuonSelector::MuonSelector():
    m_signalId(Susy::MuonId::MuonIdInvalid),
    m_signalIsolation(Isolation::IsolationInvalid),
    m_verbose(false)
    {}
//----------------------------------------------------------
bool MuonSelector::isBaseline(const Muon* mu) {
     return mu != nullptr
         && mu->medium
         && fabs(mu->z0SinTheta()) < 0.5
         && mu->Pt() > 10.0
         && fabs(mu->Eta()) <  2.4
         ;
}
//----------------------------------------------------------
bool MuonSelector::isSignal(const Muon* mu) {
    return isBaseline(mu)
        && mu->Pt() > 10.0
        && mu->medium
        && mu->isoGradientLoose
        && passIpCut(mu)
        ;
}
bool MuonSelector::isAntiID(const Muon *mu) {
    // Very loose definition
    // Most analyses will not work with this
    return isBaseline(mu) && !isSignal(mu);
}
//----------------------------------------------------------
bool MuonSelector::passIpCut(const Muon* mu) {
    return mu != nullptr
        && fabs(mu->d0sigBSCorr) < 3.0
        && fabs(mu->z0SinTheta()) < 0.5
        ;
}
//----------------------------------------------------------
float MuonSelector::effSF(const Muon& mu, const NtSys::SusyNtSys sys) {
    float out_sf = mu.muoEffSF[m_signalId]; // nominal
    if(sys!=NtSys::NOM) {
        // retrieve the error w.r.t. nominal for the given sys
        // (we store the delta w.r.t. to the nominal, i.e. delta = SF_sys - SF_nom
        // and errEffSF returns the delta)
        float delta = errEffSF(mu, sys);
        out_sf += delta;
    }
    return out_sf;
}
//----------------------------------------------------------
float MuonSelector::errEffSF(const Muon& mu, const SusyNtSys sys) {
    // return the error on the muon SF associated with systematc sys
    float err = 0.0;
    if(sys == NtSys::MUON_EFF_BADMUON_STAT_DN) {
        err = mu.errEffSF_badmu_stat_dn[m_signalId];
    }
    else if(sys == NtSys::MUON_EFF_BADMUON_STAT_UP) {
        err = mu.errEffSF_badmu_stat_up[m_signalId];
    }
    else if(sys == NtSys::MUON_EFF_BADMUON_SYS_DN) {
        err = mu.errEffSF_badmu_syst_dn[m_signalId];
    }
    else if(sys == NtSys::MUON_EFF_BADMUON_SYS_UP) {
        err = mu.errEffSF_badmu_syst_up[m_signalId];
    }
    else if(sys == NtSys::MUON_EFF_ISO_STAT_DN) {
        err = mu.errEffSF_iso_stat_dn[m_signalId];
    }
    else if(sys == NtSys::MUON_EFF_ISO_STAT_UP) {
        err = mu.errEffSF_iso_stat_up[m_signalId];
    }
    else if(sys == NtSys::MUON_EFF_ISO_SYS_DN) {
        err = mu.errEffSF_iso_syst_dn[m_signalId];
    }
    else if(sys == NtSys::MUON_EFF_ISO_SYS_UP) {
        err = mu.errEffSF_iso_syst_up[m_signalId];
    }
    else if(sys == NtSys::MUON_EFF_RECO_STAT_DN) {
        err = mu.errEffSF_reco_stat_dn[m_signalId];
    }
    else if(sys == NtSys::MUON_EFF_RECO_STAT_UP) {
        err = mu.errEffSF_reco_stat_up[m_signalId];
    }
    else if(sys == NtSys::MUON_EFF_RECO_SYS_DN) {
        err = mu.errEffSF_reco_syst_dn[m_signalId];
    }
    else if(sys == NtSys::MUON_EFF_RECO_SYS_UP) {
        err = mu.errEffSF_reco_syst_up[m_signalId];
    }
    else if(sys == NtSys::MUON_EFF_RECO_STAT_LOWPT_DN) {
        err = mu.errEffSF_reco_lowpt_stat_dn[m_signalId];
    }
    else if(sys == NtSys::MUON_EFF_RECO_STAT_LOWPT_UP) {
        err = mu.errEffSF_reco_lowpt_stat_up[m_signalId];
    }
    else if(sys == NtSys::MUON_EFF_RECO_SYS_LOWPT_DN) {
        err = mu.errEffSF_reco_lowpt_syst_dn[m_signalId];
    }
    else if(sys == NtSys::MUON_EFF_RECO_SYS_LOWPT_UP) {
        err = mu.errEffSF_reco_lowpt_syst_up[m_signalId];
    }
    else if(sys == NtSys::MUON_EFF_TTVA_STAT_DN) {
        err = mu.errEffSF_ttva_stat_dn[m_signalId];
    }
    else if(sys == NtSys::MUON_EFF_TTVA_STAT_UP) {
        err = mu.errEffSF_ttva_stat_up[m_signalId];
    }
    else if(sys == NtSys::MUON_EFF_TTVA_SYS_DN) {
        err = mu.errEffSF_ttva_syst_dn[m_signalId];
    }
    else if(sys == NtSys::MUON_EFF_TTVA_SYS_UP) {
        err = mu.errEffSF_ttva_syst_up[m_signalId];
    }
    else if(sys == NtSys::MUON_EFF_TrigStat_DN) {
        err = mu.errEffSF_trig_stat_dn[m_signalId];
    }
    else if(sys == NtSys::MUON_EFF_TrigStat_UP) {
        err = mu.errEffSF_trig_stat_up[m_signalId];
    }
    else {
        cout<<"MuonSelector::errEffSF(): you are calling this function with"
            <<" sys '"<<NtSys::SusyNtSysNames.at(sys)<<"'."
            <<" This is not a muon sys. Returning "<<err
            <<endl;
    }
    return err;
}
//----------------------------------------------------------
// begin MuonSelector_2Lep Ana_2Lep
//----------------------------------------------------------
bool MuonSelector_2Lep::isBaseline(const Muon* mu) {
    return mu != nullptr
        && mu->medium
        && fabs(mu->z0SinTheta()) < 0.5
        && mu->Pt()        > 10.0
        && fabs(mu->Eta()) <  2.4
        ;
}
bool MuonSelector_2Lep::isSignal(const Muon* mu) {
    return isBaseline(mu)
        && mu->isoGradientLoose
        && passIpCut(mu)
        ;
}

//----------------------------------------------------------
// begin MuonSelector_Stop2L Ana_Stop2L
//----------------------------------------------------------
bool MuonSelector_Stop2L::isBaseline(const Muon* mu) {
    return mu != nullptr
        && mu->Pt() > 4.0
        && fabs(mu->Eta()) < 2.7
        && fabs(mu->z0SinTheta()) < 0.5
        && mu->medium
        ;
}
bool MuonSelector_Stop2L::isSignal(const Muon* mu) {
    return isBaseline(mu)
        && mu->isoFCLoose
        && fabs(mu->d0sigBSCorr) < 3.0
        ;
}
bool MuonSelector_Stop2L::isAntiID(const Muon* mu) {
    return isBaseline(mu)
        //&& mu->isoFCLoose
        && fabs(mu->d0sigBSCorr) < 3.0

        // Reversed/Loosened cuts
        && !mu->isoFCLoose
        ;
}

//----------------------------------------------------------
// begin MuonSelector_HLFV Ana_HLFV
//----------------------------------------------------------
bool MuonSelector_HLFV::isBaseline(const Muon* mu) {
    return mu != nullptr
        && mu->Pt() > 10.0
        && fabs(mu->Eta()) <= 2.5
        //&& fabs(mu->d0sigBSCorr) < 15.0
        //&& fabs(mu->z0SinTheta()) < 15.0
        && mu->loose
        ;
}
bool MuonSelector_HLFV::isSignal(const Muon* mu) {
    return isBaseline(mu)
        && mu->medium
        && mu->isoFCTightTrackOnly
        && fabs(mu->Eta()) <= 2.47
        ;
}
bool MuonSelector_HLFV::isAntiID(const Muon* mu) {
    return isBaseline(mu)
        && mu->medium
        //&& mu->isoFCTightTrackOnly
        && fabs(mu->Eta()) <= 2.47

        // Reversed/Loosened cuts
        && !mu->isoFCTightTrackOnly
        ;
}
} // namespace Susy
