#include "SusyNtuple/OverlapTools.h"
#include "SusyNtuple/SusyNt.h"
#include "SusyNtuple/JetSelector.h"


#include <cassert>
#include <set>
#include <iomanip>
#include <iostream>
#include <sstream>

using Susy::OverlapTools;
using Susy::Electron;
using Susy::Muon;
using Susy::Jet;
using Susy::Lepton;
using Susy::Tau;
using Susy::Photon;
using std::cout;
using std::cerr;
using std::endl;
using std::string;


#include <stdio.h>
//----------------------------------------------------------

namespace Susy {
//----------------------------------------------------------
OverlapTools* OverlapTools::build(const AnalysisType &a, bool verbose) {
    OverlapTools *o;
    switch(a) {
    case AnalysisType::Ana_2Lep   :
        o = new OverlapTools_2Lep();
        break;
    case AnalysisType::Ana_Stop2L :
        o = new OverlapTools_Stop2L();
        break;
    case AnalysisType::Ana_HLFV :
        o = new OverlapTools_HLFV();
        break;
    default:
        cout<<"OverlapTools::build(): unknown analysis type '"<<AnalysisType2str(a)<<"'"
            <<" returning vanilla OverlapTools"<<endl;
        o = new OverlapTools();
    }
    o->setVerbose(verbose);
    return o;
}
//----------------------------------------------------------
OverlapTools::OverlapTools() :
    m_electronIsolation(Isolation::IsolationInvalid),
    m_muonIsolation(Isolation::IsolationInvalid),
    m_verbose(false),
    m_useOldOverlap(false)
    {}
//----------------------------------------------------------
void OverlapTools::performOverlap(ElectronVector& electrons, MuonVector& muons,
                                    JetVector& jets, TauVector& taus, PhotonVector& photons) {
    //----------------------------------------------------------
    // Implement overlap removal (ORDER IS IMPORTANT!)
    //----------------------------------------------------------
    
    // (1) PFlow-Muon-jet OR (not implemented)
    // m_PFj_overlap(, );

    // (2) Electron-electron OR
    e_e_overlap(electrons);
    
    // (3) Tau-electron OR
    t_e_overlap(taus, electrons);
    
    // (4) Tau-muon OR
    t_m_overlap(taus, muons);
    
    // (5) Electron-muon OR
    m_e_overlap(muons, electrons);
    e_m_overlap(electrons, muons);
    
    // (6) Pho-electron OR
    p_e_overlap(photons, electrons);
    
    // (7) Pho-muon OR
    p_m_overlap(photons, muons);
    
    // (8) Electron-jet OR
    j_e_overlap(jets, electrons);
    e_j_overlap(electrons, jets);
    
    // (9) Muon-jet OR
    j_m_overlap(jets, muons);
    m_j_overlap(muons, jets);
    
    // (10) Tau-jet OR
    j_t_overlap(jets, taus);
    t_j_overlap(taus, jets);
    
    // (11) Pho-jet OR (not implemented)
    j_p_overlap(jets, photons);
    
    // (12) Electron-fatjet OR (not implemented)
    // e_Fj_overlap(, );
    
    // (13) jet-fatjet OR (not implemented)
    // j_Fj_overlap(, );
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
// Helper tools
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------
void OverlapTools::e_e_overlap(ElectronVector& electrons,
                               bool useTrackMatch, bool useClusterMatch,
                               double dEta_max, double dPhi_max) {
    int nEl=electrons.size();
    if(nEl<2) return;

    static std::set<const Electron*> electronsToRemove;
    electronsToRemove.clear();

    // Find all possible e-e pairings
    for(int iEl=0; iEl<nEl; iEl++){
        const Electron* ei = electrons.at(iEl);
        for(int jEl=iEl+1; jEl<nEl; jEl++){
            const Electron* ej = electrons.at(jEl);

            // Check for match
            bool match = electronsMatch(ei, ej,
                            useTrackMatch, useClusterMatch,
                            dEta_max, dPhi_max);
            if (match) {
                const Electron* e_remove = handleOverlap(ei, ej);
                electronsToRemove.insert(e_remove);
                if(verbose() && e_remove==ei) {
                    print_rm_msg(":e_e_overlap: ", ei, ej);
                } else if (verbose() && e_remove==ej){
                    print_rm_msg(":e_e_overlap: ", ej, ei);
                }
            }

        } // jEl
    } // iEl

    // remove the flagged electrons
    for(int iEl=nEl-1; iEl>=0; iEl--){
        if(electronsToRemove.find(electrons.at(iEl)) != electronsToRemove.end()){
            electrons.erase(electrons.begin()+iEl);
        } // found one
    }
}

//----------------------------------------------------------
void OverlapTools::t_e_overlap(TauVector& taus, ElectronVector& electrons,
                               double dR,
                               const ElectronId& el_ID) {
    int nTau = taus.size();
    int nEle = electrons.size();
    if(nTau==0 || nEle==0) return;

    // Find all possible tau-e pairings
    for(int iTau=nTau-1; iTau>=0; iTau--){
        const Tau* tau = taus.at(iTau);
        for(int iEl=nEle-1; iEl>=0; iEl--){
            const Electron* e = electrons.at(iEl);

            // Check the electron ID
            if (!electronPassesID(e, el_ID)) continue;

            // Test for overlap
            if(tau->DeltaRy(*e) < dR){
                if(verbose()) print_rm_msg("t_e_overlap: ", tau, e);
                taus.erase(taus.begin()+iTau);
                break; // loop tau doesn't exist anymore!
            }

        } // for(iEl)
    } // for(iTau)
}

//----------------------------------------------------------
void OverlapTools::t_m_overlap(TauVector& taus, MuonVector& muons,
                               double dR,
                               double minMuonPt,
                               double minTauPtMuComb) {
    int nTau = taus.size();
    int nMuo = muons.size();
    if(nTau==0 || nMuo==0) return;
    for(int iTau=nTau-1; iTau>=0; iTau--){
        const Tau* tau = taus.at(iTau);
        for(int iMu=nMuo-1; iMu>=0; iMu--){
            const Muon* mu = muons.at(iMu);

            // muons must have a min. pT for comparison
            if(mu->Pt() < minMuonPt /*GeV*/) continue;

            // high pT taus are only compared to combined muons
            if( (tau->Pt() > minTauPtMuComb /*GeV*/) && !mu->isCombined) continue;

            if(tau->DeltaRy(*mu) < dR){
                if(verbose()) print_rm_msg("t_m_overlap: ", tau, mu);
                taus.erase(taus.begin()+iTau);
                break; // loop tau doesn't exist anymore!
            } // if(dR< )
        } // for(iMu)
    } // for(iTau)
}

//----------------------------------------------------------
void OverlapTools::m_e_overlap(MuonVector& muons, ElectronVector& electrons) {
    // Remove calo-tagged muons that have shared ID track with electrons.
    // See OverlapTools::eleMuSharedTrack(...)

    if(electrons.size()==0 || muons.size()==0) return;

    for(int iMu=muons.size()-1; iMu>=0; iMu--){
        const Muon* mu = muons.at(iMu);
        if( !mu->isCaloTagged ) continue;
        for(int iEl=0; iEl<(int)electrons.size(); iEl++){
            const Electron* el = electrons.at(iEl);
            if(eleMuSharedTrack(el, mu)) {
                if(verbose()) print_rm_msg("m_e_overlap: ", mu, el);
                muons.erase(muons.begin()+iMu);
                break;
            } // shared tracks
        } // iEl
    } // iMu
}
//----------------------------------------------------------
void OverlapTools::e_m_overlap(ElectronVector& electrons, MuonVector& muons) {
    // Remove electrons that have shared ID track with remaining muons.
    // See OverlapTools::eleMuSharedTrack(...)

    if(electrons.size()==0 || muons.size()==0) return;

    for(int iEl=electrons.size()-1; iEl>=0; iEl--){
        const Electron* el = electrons.at(iEl);
        for(int iMu=0; iMu<(int)muons.size(); iMu++){
            const Muon* mu = muons.at(iMu);
            if(eleMuSharedTrack(el, mu)) {
                if(verbose()) print_rm_msg("e_m_overlap: ", el, mu);
                electrons.erase(electrons.begin()+iEl);
                break;
            }
        } // iMu
    } // iEl
}

////----------------------------------------------------------
//----------------------------------------------------------
void OverlapTools::p_e_overlap(PhotonVector& photons, ElectronVector& electrons, double dR) {
    if(photons.size()==0 || electrons.size()==0) return;
    for(int iPh = photons.size()-1; iPh>=0; iPh--){
        const Photon* pho = photons.at(iPh);
        for(int iEl = 0; iEl < (int)electrons.size(); iEl++){
            const Electron* ele = electrons.at(iEl);
            if(pho->DeltaRy(*ele) < dR) {
                if(verbose()) print_rm_msg("p_e_overlap: ", pho, ele);
                photons.erase(photons.begin()+iPh);
                break;
            }
        } // iEl
    } // iPh
}
//----------------------------------------------------------
void OverlapTools::p_m_overlap(PhotonVector& photons, MuonVector& muons, double dR) {
    if(photons.size()==0 || muons.size()==0) return;
    for(int iPh = photons.size()-1; iPh>=0; iPh--){
        const Photon* pho = photons.at(iPh);
        for(int iMu = 0; iMu < (int)muons.size(); iMu++){
            const Muon* mu = muons.at(iMu);
            if(pho->DeltaRy(*mu) < dR) {
                if(verbose()) print_rm_msg("p_m_overlap: ", pho, mu);
                photons.erase(photons.begin()+iPh);
                break;
            }
        } // iMu
    } // iPh
}
//----------------------------------------------------------
void OverlapTools::j_e_overlap(JetVector& jets, ElectronVector& electrons,
                               double dR,
                               bool doBJetOR,
                               bool applyPtRatio,
                               double eleJetPtRatio,
                               double bJetORLepThresh) {
    if(electrons.size()==0 || jets.size()==0) return;
    bool checkLepThresh = bJetORLepThresh > 0;
    for(int iJ=jets.size()-1; iJ>=0; iJ--){
        const Jet* j = jets.at(iJ);

        // Don't reject b-tagged jets below pT threshold
        bool isB = jetSelector()->isBJetOR(j, /*useUpperPt*/ true);
        if(doBJetOR && !checkLepThresh && isB) {
            continue;
        }
        // flag overlapping jets
        for(int iEl=0; iEl < (int)electrons.size(); iEl++){
            const Electron* el = electrons.at(iEl);

            // Don't reject jets with high relative PT
            if (applyPtRatio) {
                double ratio = el->Pt()/j->Pt();
                if (ratio < eleJetPtRatio) continue;
            }

            // Don't reject b-tagged jet with electron below pT threshold
            if (checkLepThresh) {
                isB = jetSelector()->isBJetOR(j, /*useUpperPt*/ false);
                if (el->Pt() < bJetORLepThresh && isB) continue;
            }
            
            // Check for overlap
            if(el->DeltaRy(*j) < dR) {
                if(verbose()) print_rm_msg("j_e_overlap: ", j, el);
                jets.erase(jets.begin()+iJ);
                break;
            }
        } // iEl
    } // iJ
}

//----------------------------------------------------------
void OverlapTools::e_j_overlap(ElectronVector& electrons, JetVector& jets, 
                               double dR,
                               bool doSlidingCone,
                               bool applyJVT) {
    // Remove electrons overlapping with remaining jets
    //     > slidingCone : if true, use a cone with dR size determined by
    //                     sliding cone algorithm. See
    //                     OverlapTools::getSlidingDRCone(...)
    if(electrons.size()==0 || jets.size()==0) return;
    for(int iEl = electrons.size()-1; iEl>=0; iEl--){
        const Electron* el = electrons.at(iEl);
        for(int iJ = 0; iJ < (int)jets.size(); iJ++){
            const Jet* j = jets.at(iJ);

            // don't reject electron due to pileup
            // in AssociationUtils, applied via getObjectPriority in handleOverlap method
            // getObjectPriority checks the InputLabel, which is set by SUSYTools
            if(applyJVT && !JetSelector::passJvt(j)) continue;

            // use sliding cone dR if needed
            if(doSlidingCone) dR = getSlidingDRCone(el->Pt());
            if(el->DeltaRy(*j) < dR) {
                if(verbose()) print_rm_msg("e_j_overlap: ", el, j);
                electrons.erase(electrons.begin()+iEl);
                break;
            }
        } // iJ
    } // iEl
}
//----------------------------------------------------------
void OverlapTools::j_m_overlap(JetVector& jets, MuonVector& muons,
                               double dR,
                               bool doBJetOR,
                               bool doGhost,
                               bool applyRelPt) {
    // Remove jets overlapping with muons
    //     > doBJetOR : if true, do not compare jets and muons when jet is
    //                  tagged as a b-jet
    //                  See JetSelector::isBJetOR(...)
    //     > doGhost  : if true, check if muon is ghost associated with the jet
    //                  and remove the jet if it is (in addition to the dR match).
    //                  See OverlapTools::muonIsGhostMatched(...)
    //     > applyRelPt : use muon/jet pT ratios
    if(jets.size()==0 || muons.size()==0) return;
    for(int iJ = jets.size()-1; iJ>=0; iJ--){
        const Jet* j = jets.at(iJ);

        // Don't reject user-defined b-tagged jets
        if(doBJetOR && jetSelector()->isBJetOR(j)) continue;

        for(int iMu = 0; iMu < (int)muons.size(); iMu++){
            const Muon* mu = muons.at(iMu);

            // Don't reject jets with high track multiplicity and
            // high relative PT ratio
            float ptRatio = mu->Pt() / j->Pt() * 1.0;
            float trkRatio = mu->Pt() / j->sumTrkPt * 1.0;
            bool highRelPt = (ptRatio<0.5 || trkRatio<0.7);
            bool highNumTrk = j->nTracks >= 3 ;
            if(highNumTrk && (!applyRelPt || highRelPt)) {
                continue;
            }

            // check dR match regardless
            if(j->DeltaRy(*mu) < dR || (doGhost && muonIsGhostMatched(mu, j))) {
                if(verbose()) print_rm_msg("j_m_overlap: ", j, mu);
                jets.erase(jets.begin()+iJ);
                break;
            }
        } // iMu
    } // iJ
}
//----------------------------------------------------------
void OverlapTools::m_j_overlap(MuonVector& muons, JetVector& jets,
                               double dR,
                               bool doSlidingCone,
                               bool applyJVT) {
    // Remove muons overlapping with remaining jets
    //     > slidingCone : if true, use a cone with dR size determined by
    //                     sliding cone algorithm. See
    //                     OverlapTools::getSlidingDRCone(...)
    if(muons.size()==0 || jets.size()==0) return;
    for(int iMu = muons.size()-1; iMu>=0; iMu--){
        const Muon* mu = muons.at(iMu);
        for(int iJ = 0; iJ < (int)jets.size(); iJ++){
            const Jet* j = jets.at(iJ);

            // don't reject muon due to pileup
            // in AssociationUtils, applied via getObjectPriority in handleOverlap method
            // getObjectPriority checks the InputLabel, which is set by SUSYTools
            if(applyJVT && !JetSelector::passJvt(j)) continue;

            
            // use sliding cone dR if needed
            if(doSlidingCone) dR = getSlidingDRCone(mu->Pt());
            if(mu->DeltaRy(*j) < dR) {
                if(verbose()) print_rm_msg("m_j_overlap: ", mu, j);
                muons.erase(muons.begin()+iMu);
                break;
            }
        } // iJ
    } // iMu
}

//----------------------------------------------------------
void OverlapTools::j_t_overlap(JetVector& jets, TauVector& taus,
                               double dR,
                               bool doBJetOR) {
    int nTau=taus.size();
    int nJet=jets.size();
    if(nTau==0 || nJet==0) return;
    for(int iJet=nJet-1; iJet>=0; iJet--){
        const Jet* jet = jets.at(iJet);
        for(int iTau=nTau-1; iTau>=0; iTau--){
            const Tau* tau = taus.at(iTau);

            // Don't reject user-defined b-tagged jets
            if(doBJetOR && jetSelector()->isBJetOR(jet)) {
                continue;
            }
            // Remove remaining jets that overlap with taus
            if(jet->DeltaRy(*tau) < dR){
                if(verbose()) print_rm_msg("j_t_overlap: ", jet, tau);
                jets.erase(jets.begin()+iJet);
                break; // loop jet doesn't exist anymore!
            }

        } // for(iTau)
    } // for(iJet)
}
void OverlapTools::t_j_overlap(TauVector& taus, JetVector& jets, double dR) {
    int nTau=taus.size();
    int nJet=jets.size();
    if(nTau==0 || nJet==0) return;
    for(int iTau=nTau-1; iTau>=0; iTau--){
        const Tau* tau = taus.at(iTau);
        for(int iJet=nJet-1; iJet>=0; iJet--){
            const Jet* jet = jets.at(iJet);

            // Remove taus that overlap with jets
            if(tau->DeltaRy(*jet) < dR){
                if(verbose()) print_rm_msg("t_j_overlap: ", tau, jet);
                taus.erase(taus.begin()+iTau);
                break; // loop tau doesn't exist anymore!
            }

        } // for(iTau)
    } // for(iJet)
}


//----------------------------------------------------------
void OverlapTools::j_p_overlap(JetVector& jets, PhotonVector& photons, double dR /*=0.4*/) {
    // Remove jets matched within dRy cone with remaining photons
    // >> default cone size is dRy = 0.4
    if(jets.size()==0 || photons.size()==0) return;
    for(int iJ = jets.size()-1; iJ>=0; iJ--){
        const Jet* j = jets.at(iJ);
        for(int iP = 0; iP < (int)photons.size(); iP++) {
            const Photon* pho = photons.at(iP);
            if(pho->DeltaRy(*j) < dR) {
                if(verbose()) print_rm_msg("j_p_overlap: ", j, pho);
                jets.erase(jets.begin()+iJ);
                break;
            }
        } // iP
    } // iJ
}


//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
// Helper tools
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//----------------------------------------------------------
bool OverlapTools::electronsMatch(const Electron* el1, const Electron* el2,
                                  bool useTrackMatch, bool useClusterMatch,
                                  double dEta_max, double dPhi_max){
    // Sanity check
    if(!useTrackMatch && !useClusterMatch){
        cout << "OverlapTools::leptonPassesIsolation: "
             << "You must enable at least one of "
             << "UseTrackMatch or UseClusterMatch" 
             << endl;
    }
    // Look for a shared track
    if (useTrackMatch) {
        bool trackMatched = el2->sharedEleEleTrk.at(el1->idx);
        //auto begin = el2->sharedEleEleTrk.begin();
        //auto end = el2->sharedEleEleTrk.end();
        //bool trackMatched = std::find(begin, end, el1->idx) != end;
        return trackMatched;
    }
    // Look for overlapping clusters
    if (useClusterMatch){
        TLorentzVector Vi, Vj;
        Vi.SetPtEtaPhiM(el1->Pt(), el1->clusEtaBE, el1->clusPhiBE, el1->M());
        Vj.SetPtEtaPhiM(el2->Pt(), el2->clusEtaBE, el2->clusPhiBE, el2->M());
        double dPhi = Vi.DeltaPhi(Vj);
        double dEta = std::fabs(el1->clusEtaBE - el2->clusEtaBE);
        if( dEta < dEta_max && dPhi < dPhi_max){
            return true;
        }
    }

    // No match
    return false;
}
const Electron* OverlapTools::handleOverlap(const Electron* el1, const Electron* el2){
    // Always reject author "Ambiguous" when compared to author "Electron"
    // Otherwise, reject the softer electron
    if (el1->authorAmbiguous && el2->authorElectron){
        return el1;
    } else if (el1->authorElectron && el2->authorAmbiguous){
        return el2;
    } else {
        return (el1->Pt() < el2->Pt()) ? el1 : el2;
    }
}

bool OverlapTools::electronPassesID(const Electron* el, const ElectronId &el_ID){
    if      (el_ID == ElectronId::TightLLH)        return el->tightLLH;
    else if (el_ID == ElectronId::MediumLLH)       return el->mediumLLH;
    else if (el_ID == ElectronId::LooseLLHBLayer)  return el->looseLLHBLayer;
    else if (el_ID == ElectronId::LooseLLH)        return el->looseLLH;
    else if (el_ID == ElectronId::VeryLooseLLH)    return el->veryLooseLLH;
    else {
        cout<<"OverlapTools::electronPassesID:"
            <<" unknown electron ID '"<<ElectronId2str(el_ID)<<"'."<<endl;
        exit(1);
    }
    return false;
}

bool OverlapTools::leptonPassesIsolation(const Lepton* lep, const Isolation &iso) {

    if      (iso == Isolation::GradientLoose)           return lep->isoGradientLoose;
    else if (iso == Isolation::Gradient)                return lep->isoGradient;
    else if (iso == Isolation::LooseTrackOnly)          return lep->isoLooseTrackOnly;
    else if (iso == Isolation::Loose)                   return lep->isoLoose;
    else if (iso == Isolation::FixedCutTightTrackOnly)  return lep->isoFixedCutTightTrackOnly;
    else if (iso == Isolation::FixedCutLoose)           return lep->isoFCLoose;
    else if (iso == Isolation::FixedCutTight)           return lep->isoFCTight;
    else {
        cout<<"OverlapTools::leptonPassesIsolation:"
            <<" unknown isolation '"<<Isolation2str(iso)<<"'."<<endl;
        exit(1);
    }
}

void OverlapTools::removeNonisolatedLeptons(ElectronVector& electrons, MuonVector& muons) {
    if(m_electronIsolation==Isolation::IsolationInvalid ||
       m_muonIsolation==Isolation::IsolationInvalid) {
        cout<<"OverlapTools::removeNonisolatedLeptons:"
            <<" you need to call setElectronIsolation() and setMuonIsolation() first."
            <<endl;
        exit(1);
    }
    for(int iEl = (int)electrons.size()-1; iEl >= 0; iEl--) {
        const Electron* e = electrons.at(iEl);
        if(!leptonPassesIsolation(e, m_electronIsolation)) { electrons.erase(electrons.begin()+iEl); continue; }
    }
    for(int iMu = (int)muons.size()-1; iMu >= 0; iMu--) {
        const Muon* m = muons.at(iMu);
        if(!leptonPassesIsolation(m, m_muonIsolation)) { muons.erase(muons.begin()+iMu); continue; }
    }
}
//----------------------------------------------------------
bool OverlapTools::muonIsGhostMatched(const Muon* mu, const Jet* jet) {
    if(!(mu->ghostTrack.size()>0))
        return false;
    if(jet->idx > ((int)mu->ghostTrack.size()-1))
        return false;
    return ((mu->ghostTrack[jet->idx]==1) ? true : false);
}
//----------------------------------------------------------
bool OverlapTools::eleMuSharedTrack(const Electron* el, const Muon* mu) {
    if(!(el->sharedMuTrk.size()>0))
        return false;
    if(mu->idx > ((int)el->sharedMuTrk.size()-1))
        return false;
    return ((el->sharedMuTrk[mu->idx]==1) ? true : false);
}
//----------------------------------------------------------
float OverlapTools::getSlidingDRCone(float pT, float c1, float c2, float maxConeSize) {
    float dRcone = c1 + ( c2 / pT * 1.0);
    // return upper-limit on cone size
    return std::min(dRcone, maxConeSize);
}
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
// Debugging tools
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
string OverlapTools::str(const Jet* j) {
    std::ostringstream oss;
    oss<<"jet "
       <<"("
       <<std::setprecision(3)<<j->Pt()<<", "
       <<std::setprecision(3)<<j->Eta()<<", "
       <<std::setprecision(3)<<j->Phi()<<", "
       <<"nTk="<<j->nTracks<<", "
       <<"mv2c10="<<j->mv2c10
       <<")";
    return oss.str();
}
string OverlapTools::str(const Lepton* l) {
    std::ostringstream oss;
    oss<<(l->isEle() ? "el":"mu")<<" "
       <<"("
       <<std::setprecision(3)<<l->Pt()<<", "
       <<std::setprecision(3)<<l->Eta()<<", "
       <<std::setprecision(3)<<l->Phi()
       <<")";
    return oss.str();
}
string OverlapTools::str(const Photon* pho) {
    std::ostringstream oss;
    oss <<"("
        <<std::setprecision(3)<<pho->Pt()<<", "
        <<std::setprecision(3)<<pho->Eta()<<", "
        <<std::setprecision(3)<<pho->Phi()
        <<")";
    return oss.str();
}
string OverlapTools::str(const Tau* tau) {
    std::ostringstream oss;
    oss << "("
        << tau->Pt() << ","
        << tau->Eta() << ","
        << tau->Phi() << ","
        << "q=" << tau->q << ","
        << "nTrack=" << tau->nTrack << ","
        << "quality=";
    if (tau->loose) oss << "loose,";
    if (tau->medium) oss << "medium,";
    if (tau->tight) oss << "tight,";
    oss << ")";
    return oss.str();
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
// begin Analysis specific classes
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------
// begin OverlapTools_Stop2L
//----------------------------------------------------------
void OverlapTools_Stop2L::performOverlap(ElectronVector& electrons, 
                                         MuonVector& muons, 
                                         JetVector& jets, 
                                         TauVector& /*taus*/, 
                                         PhotonVector& /*photons*/
                                         ) {  
    e_e_overlap(electrons);
    m_e_overlap(muons, electrons);
    e_m_overlap(electrons, muons);
    j_e_overlap(jets, electrons, /*dRcone=*/0.2, /*doBJetOR=*/true);
    e_j_overlap(electrons, jets, /*dRcone=*/0.4, /*slidingCone=*/true, /*applyJVT=*/true);
    j_m_overlap(jets, muons);
    m_j_overlap(muons, jets, /*dRcone=*/0.4, /*slidingCone=*/true, /*applyJVT=*/true);
}

//----------------------------------------------------------
/////////////////////////////////////////////////////////////////////////////
void OverlapTools_HLFV::performOverlap(ElectronVector& electrons, 
                                       MuonVector& muons,
                                       JetVector& jets, 
                                       TauVector& taus, 
                                       PhotonVector& /*photons*/
                                       ) {
    // Standard Association Utils OR
    e_e_overlap(electrons);
    t_e_overlap(taus, electrons);
    t_m_overlap(taus, muons);
    m_e_overlap(muons, electrons);
    e_m_overlap(electrons, muons);
    j_e_overlap(jets, electrons);
    e_j_overlap(electrons, jets);
    j_m_overlap(jets, muons);
    m_j_overlap(muons, jets);
    j_t_overlap(jets, taus);
    t_j_overlap(taus, jets);
}

} // namespace Susy
