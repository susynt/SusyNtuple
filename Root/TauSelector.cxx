#include "SusyNtuple/TauSelector.h"
#include "SusyNtuple/Tau.h"

#include <iostream>

using Susy::NtSys::SusyNtSys;
using std::cout;
using std::endl;
using std::string;

namespace Susy {

//----------------------------------------------------------
TauSelector* TauSelector::build(const AnalysisType &a, bool verbose) {
    TauSelector *selector;
    switch(a) {
    case AnalysisType::Ana_2Lep   : selector = new TauSelector_2Lep();      break;
    case AnalysisType::Ana_HLFV   : selector = new TauSelector_HLFV();      break;
    default:
        cout<<"TauSelector::build(): unknown analysis type '"<<AnalysisType2str(a)<<"'"
            <<" returning vanilla TauSelector"<<endl;
        selector = new TauSelector();
    }
    selector->setVerbose(verbose);
    return selector;
}
//----------------------------------------------------------
TauSelector::TauSelector():
    m_verbose(false)
    {}
//----------------------------------------------------------
bool TauSelector::isBaseline(const Tau* tau) {
    return tau != nullptr
        && tau->medium
        ;
}
bool TauSelector::isSignal(const Tau* tau) {
    return isBaseline(tau) 
        && fabs(tau->Eta()) < 2.5 
        && tau->Pt() > 20
        ;
}
//----------------------------------------------------------
// begin TauSelector_HLFV Ana_HLFV
//----------------------------------------------------------
bool TauSelector_HLFV::isBaseline(const Tau* tau) {
    return tau != nullptr
        && tau->Pt() > 20.0
        && (fabs(tau->Eta()) < 1.37 || 1.52 < fabs(tau->Eta())) 
        && fabs(tau->Eta()) < 2.5
        && fabs(tau->q) == 1
        && (tau->nTrack == 1 || tau->nTrack == 3)
        && tau->medium
        ;
}
bool TauSelector_HLFV::isSignal(const Tau* tau) {
    return isBaseline(tau)
           ;
}
//----------------------------------------------------------
} // namespace Susy

