#include "SusyNtuple/ElectronSelector.h"
#include "SusyNtuple/Electron.h"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <cmath> // abs

using Susy::NtSys::SusyNtSys;
using std::cout;
using std::endl;
using std::string;

namespace Susy {

//----------------------------------------------------------
ElectronSelector* ElectronSelector::build(const AnalysisType &a, bool verbose) {
    ElectronSelector* s = nullptr;
    switch(a) {
    case AnalysisType::Ana_2Lep:
        s = new ElectronSelector_2Lep();
        s->setSignalId(ElectronId::MediumLLH);
        s->setSignalIsolation(Isolation::GradientLoose);
        break;
    case AnalysisType::Ana_Stop2L:
        s = new ElectronSelector_Stop2L();
        s->setSignalId(ElectronId::MediumLLH);
        s->setSignalIsolation(Isolation::Gradient);
        break;
    case AnalysisType::Ana_HLFV:
        s = new ElectronSelector_HLFV();
        s->setSignalId(ElectronId::MediumLLH);
        s->setSignalIsolation(Isolation::Gradient);
        break;
    default:
        cout<<"ElectronSelector::build(): unknown analysis type '"<<AnalysisType2str(a)<<"'"
            <<" returning vanilla ElectronSelector"<<endl;
        s = new ElectronSelector();
    }
    s->setVerbose(verbose);
    return s;
}
//---------------------------------------------------------
ElectronSelector::ElectronSelector() :
    m_signalId(ElectronId::ElectronIdInvalid),
    m_signalIsolation(Isolation::IsolationInvalid),
    m_verbose(false)
    {}
//---------------------------------------------------------
bool ElectronSelector::isBaseline(const Electron* el) {
    return el != nullptr
        && el->looseLLHBLayer
        && fabs(el->z0SinTheta()) < 0.5
        && el->Pt()  > 10.0
        && std::abs(el->clusEta) < 2.47 // SUSYTools uses CaloCluster::eta() for this but CaloCluster::etaBE(2) for crack region...
        ;
}
bool ElectronSelector::isSignal(const Electron* el) {
    return isBaseline(el)
        && el->Pt() > 10.0
        && std::abs(el->Eta()) < 2.47
        && el->tightLLH
        && passIpCut(el)
        && el->isoGradientLoose
        ;
}
bool ElectronSelector::isAntiID(const Electron* el) {
    // Very loose definition
    // Most analyses will not work with this
    return isBaseline(el) && !isSignal(el);
}

bool ElectronSelector::passIpCut(const Electron* el) {
    return (std::abs(el->d0sigBSCorr)  < 5.0 &&
            std::abs(el->z0SinTheta()) < 0.5 );
}

bool ElectronSelector::outsideCrackRegion(const Electron* el) {
    return (std::abs(el->clusEtaBE) < 1.37 ||
            std::abs(el->clusEtaBE) > 1.52 );
}

//---------------------------------------------------------
float ElectronSelector::effSF(const Electron& ele, const NtSys::SusyNtSys sys) {
    float out_sf = ele.eleEffSF[m_signalId];
    if(sys != NtSys::NOM) {
        float delta = errEffSF(ele, sys);
        out_sf += delta;
    }
    return out_sf;
}

float ElectronSelector::errEffSF(const Electron& ele, const SusyNtSys sys) {
    // return the error on the electron SF associated with systematic sys
    float err = 0.0;
    if     (sys == NtSys::EL_EFF_ID_TOTAL_Uncorr_UP) {
        err = ele.errEffSF_id_up[m_signalId];
    }
    else if(sys == NtSys::EL_EFF_ID_TOTAL_Uncorr_DN) {
        err = ele.errEffSF_id_dn[m_signalId];
    }
    else if(sys == NtSys::EL_EFF_Reco_TOTAL_Uncorr_UP) {
        err = ele.errEffSF_reco_up[m_signalId];
    }
    else if(sys == NtSys::EL_EFF_Reco_TOTAL_Uncorr_DN) {
        err = ele.errEffSF_reco_dn[m_signalId];
    }
    else if(sys == NtSys::EL_EFF_Iso_TOTAL_Uncorr_UP) {
        err = ele.errEffSF_iso_up[m_signalId];
    }
    else if(sys == NtSys::EL_EFF_Iso_TOTAL_Uncorr_DN) {
        err = ele.errEffSF_iso_dn[m_signalId];
    }
    else if(sys == NtSys::EL_EFF_Trigger_TOTAL_DN) {
        err = ele.errEffSF_trig_dn_single[m_signalId];
    }
    else if(sys == NtSys::EL_EFF_Trigger_TOTAL_UP) {
        err = ele.errEffSF_trig_up_single[m_signalId];
    }
    else {
        cout << "ElectronSelector::errEffSF(): you are calling this function with"
             <<" sys '" << NtSys::SusyNtSysNames.at(sys)<<"'."
             <<" This is not an electron sys. Returning " << err
             << endl;
    }
    return err;
}

//----------------------------------------------------------
// begin ElectronSelector_2Lep Ana_2Lep
//----------------------------------------------------------
bool ElectronSelector_2Lep::isBaseline(const Electron* el) {
    return el != nullptr
        && el->looseLLHBLayer
        && fabs(el->z0SinTheta())
        && el->Pt() > 10.0
        && std::abs(el->clusEta) < 2.47
        ;
}
bool ElectronSelector_2Lep::isSignal(const Electron* el) {
    return isBaseline(el)
        && el->mediumLLH
        && el->isoGradientLoose
        && passIpCut(el)
        ;
}

//----------------------------------------------------------
// begin ElectronSelector_Stop2L Ana_Stop2L
//----------------------------------------------------------
bool ElectronSelector_Stop2L::isBaseline(const Electron* el) {
    return el != nullptr
        && el->looseLLHBLayer
        && el->Pt()  > 4.5
        && std::abs(el->clusEta) < 2.47 // SUSYTools uses CaloCluster::eta() for this but CaloCluster::etaBE(2) for crack region...
        && std::abs(el->z0SinTheta()) < 0.5
        ;
}
bool ElectronSelector_Stop2L::isSignal(const Electron* el) {
    return isBaseline(el)
        && fabs(el->Eta()) < 2.47
        && el->mediumLLH
        && el->isoGradient
        //&& (el->Pt() > 200 ? el->isoFCHighPtCaloOnly : el->isoGradient)
        && std::abs(el->d0sigBSCorr)  < 5.0
        ;
}
bool ElectronSelector_Stop2L::isAntiID(const Electron* el) {
    // Reverse iso and loosen ID cut. Everything else is the same
    return isBaseline(el)
        && fabs(el->Eta()) < 2.47
        //&& el->mediumLLH
        //&& el->isoGradient
        && std::abs(el->d0sigBSCorr)  < 5.0

        // Reversed/Loosened cuts
        && (!el->isoGradient || !el->mediumLLH)
        && el->looseLLHBLayer
        ;
}

//----------------------------------------------------------
// begin ElectronSelector_HLFV Ana_HLFV
//----------------------------------------------------------
bool ElectronSelector_HLFV::isBaseline(const Electron* el) {
    return el != nullptr
        && el->looseLLHBLayer
        //&& std::abs(el->d0sigBSCorr)  < 15.0
        //&& std::abs(el->z0SinTheta()) < 15.0
        && el->Pt()  > 15.0
        && outsideCrackRegion(el)
        && std::abs(el->clusEtaBE) < 2.47
        ;
}
bool ElectronSelector_HLFV::isSignal(const Electron* el) {
    return isBaseline(el)
        && el->mediumLLH
        && el->isoGradient
        ;
}
bool ElectronSelector_HLFV::isAntiID(const Electron* el) {
    return isBaseline(el)
        //&& el->mediumLLH
        //&& el->isoGradient
        
        // Reversed/Loosened cuts
        && (!el->isoGradient || !el->mediumLLH)
        && el->looseLLHBLayer
        ;
}
//----------------------------------------------------------
} // namespace Susy
