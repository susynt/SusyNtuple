#include <iostream>
#include <iomanip>
#include "SusyNtuple/Muon.h"

using namespace std;

using Susy::Muon;
using Susy::Lepton;

/*--------------------------------------------------------------------------------*/
// Copy constructor
/*--------------------------------------------------------------------------------*/
Muon::Muon(const Muon &rhs):
    Lepton(rhs),
    isCaloTagged(rhs.isCaloTagged),
    isStandalone(rhs.isStandalone),
    isSiForward(rhs.isSiForward),
    isCombined(rhs.isCombined),
    ghostTrack(rhs.ghostTrack),
    idTrackPt(rhs.idTrackPt),
    idTrackEta(rhs.idTrackEta),
    idTrackPhi(rhs.idTrackPhi),
    idTrackQ(rhs.idTrackQ),
    idTrackTheta(rhs.idTrackTheta),
    idTrackQoverP(rhs.idTrackQoverP),
    msTrackPt(rhs.msTrackPt),
    msTrackEta(rhs.msTrackEta),
    msTrackPhi(rhs.msTrackPhi),
    msTrackQ(rhs.msTrackQ),
    msTrackTheta(rhs.msTrackTheta),
    msTrackQoverP(rhs.msTrackQoverP),
    energyLoss(rhs.energyLoss),
    veryLoose(rhs.veryLoose),
    loose(rhs.loose),
    medium(rhs.medium),
    tight(rhs.tight),
    muoEffSF(rhs.muoEffSF),
    muoTrigSF(rhs.muoTrigSF),
    muoTrigSF_map(rhs.muoTrigSF_map),
    muoTrigEffData_medium(rhs.muoTrigEffData_medium),
    muoTrigEffMC_medium(rhs.muoTrigEffMC_medium),
    muoTrigEffData_loose(rhs.muoTrigEffData_loose),
    muoTrigEffMC_loose(rhs.muoTrigEffMC_loose),
    muoTrigEffErrData_stat_up_medium(rhs.muoTrigEffErrData_stat_up_medium),
    muoTrigEffErrData_stat_dn_medium(rhs.muoTrigEffErrData_stat_dn_medium),
    muoTrigEffErrMC_stat_up_medium(rhs.muoTrigEffErrMC_stat_up_medium),
    muoTrigEffErrMC_stat_dn_medium(rhs.muoTrigEffErrMC_stat_dn_medium),
    muoTrigEffErrData_syst_up_medium(rhs.muoTrigEffErrData_syst_up_medium),
    muoTrigEffErrData_syst_dn_medium(rhs.muoTrigEffErrData_syst_dn_medium),
    muoTrigEffErrMC_syst_up_medium(rhs.muoTrigEffErrMC_syst_up_medium),
    muoTrigEffErrMC_syst_dn_medium(rhs.muoTrigEffErrMC_syst_dn_medium),
    diMuTrigMap(rhs.diMuTrigMap),
    isBadMuon(rhs.isBadMuon),
    isCosmic(rhs.isCosmic),
    ms_up(rhs.ms_up),
    ms_dn(rhs.ms_dn),
    id_up(rhs.id_up),
    id_dn(rhs.id_dn),
    scale_up(rhs.scale_up),
    scale_dn(rhs.scale_dn),
    sagitta_bias_dn(rhs.sagitta_bias_dn),
    sagitta_bias_up(rhs.sagitta_bias_up),
    sagitta_rho_dn(rhs.sagitta_rho_dn),
    sagitta_rho_up(rhs.sagitta_rho_up),
    errEffSF_badmu_stat_up(rhs.errEffSF_badmu_stat_up),
    errEffSF_badmu_stat_dn(rhs.errEffSF_badmu_stat_dn),
    errEffSF_badmu_syst_up(rhs.errEffSF_badmu_syst_up),
    errEffSF_badmu_syst_dn(rhs.errEffSF_badmu_syst_dn),
    errEffSF_iso_stat_up(rhs.errEffSF_iso_stat_up),
    errEffSF_iso_stat_dn(rhs.errEffSF_iso_stat_dn),
    errEffSF_iso_syst_up(rhs.errEffSF_iso_syst_up),
    errEffSF_iso_syst_dn(rhs.errEffSF_iso_syst_dn),
    errEffSF_reco_stat_up(rhs.errEffSF_reco_stat_up),
    errEffSF_reco_stat_dn(rhs.errEffSF_reco_stat_dn),
    errEffSF_reco_syst_up(rhs.errEffSF_reco_syst_up),
    errEffSF_reco_syst_dn(rhs.errEffSF_reco_syst_dn),
    errEffSF_reco_lowpt_stat_up(rhs.errEffSF_reco_lowpt_stat_up),
    errEffSF_reco_lowpt_stat_dn(rhs.errEffSF_reco_lowpt_stat_dn),
    errEffSF_reco_lowpt_syst_up(rhs.errEffSF_reco_lowpt_syst_up),
    errEffSF_reco_lowpt_syst_dn(rhs.errEffSF_reco_lowpt_syst_dn),
    errEffSF_ttva_stat_up(rhs.errEffSF_ttva_stat_up),
    errEffSF_ttva_stat_dn(rhs.errEffSF_ttva_stat_dn),
    errEffSF_ttva_syst_up(rhs.errEffSF_ttva_syst_up),
    errEffSF_ttva_syst_dn(rhs.errEffSF_ttva_syst_dn),
    errEffSF_trig_stat_up(rhs.errEffSF_trig_stat_up),
    errEffSF_trig_stat_dn(rhs.errEffSF_trig_stat_dn),
    errEffSF_trig_syst_up(rhs.errEffSF_trig_syst_up),
    errEffSF_trig_syst_dn(rhs.errEffSF_trig_syst_dn)
{
}
/*--------------------------------------------------------------------------------*/
// Assignment operator
/*--------------------------------------------------------------------------------*/
Muon& Muon::operator=(const Muon &rhs)
{
    if (this != &rhs) {
        Lepton::operator=(rhs);
        isCaloTagged = rhs.isCaloTagged;
        isStandalone = rhs.isStandalone;
        isSiForward = rhs.isSiForward;
        isCombined = rhs.isCombined;
        ghostTrack = rhs.ghostTrack;
        idTrackPt = rhs.idTrackPt;
        idTrackEta = rhs.idTrackEta;
        idTrackPhi = rhs.idTrackPhi;
        idTrackQ = rhs.idTrackQ;
        idTrackTheta = rhs.idTrackTheta;
        idTrackQoverP = rhs.idTrackQoverP;
        msTrackPt = rhs.msTrackPt;
        msTrackEta = rhs.msTrackEta;
        msTrackPhi = rhs.msTrackPhi;
        msTrackQ = rhs.msTrackQ;
        msTrackTheta = rhs.msTrackTheta;
        msTrackQoverP = rhs.msTrackQoverP;
        energyLoss = rhs.energyLoss;
        veryLoose = rhs.veryLoose;
        loose = rhs.loose;
        medium = rhs.medium;
        tight = rhs.tight;
        diMuTrigMap = rhs.diMuTrigMap;
        isBadMuon = rhs.isBadMuon;
        isCosmic = rhs.isCosmic;
        ms_up = rhs.ms_up;
        ms_dn = rhs.ms_dn;
        id_up = rhs.id_up;
        id_dn = rhs.id_dn;
        scale_up = rhs.scale_up;
        scale_dn = rhs.scale_dn;
        sagitta_bias_dn = rhs.sagitta_bias_dn;
        sagitta_bias_up = rhs.sagitta_bias_up;
        sagitta_rho_dn = rhs.sagitta_rho_dn;
        sagitta_rho_up = rhs.sagitta_rho_up;
        muoEffSF = rhs.muoEffSF;
        muoTrigSF = rhs.muoTrigSF;
        muoTrigSF_map = rhs.muoTrigSF_map;
        muoTrigEffData_medium = rhs.muoTrigEffData_medium;
        muoTrigEffMC_medium = rhs.muoTrigEffMC_medium;
        muoTrigEffData_loose = rhs.muoTrigEffData_loose;
        muoTrigEffMC_loose = rhs.muoTrigEffMC_loose;
        muoTrigEffErrData_stat_up_medium = rhs.muoTrigEffErrData_stat_up_medium;
        muoTrigEffErrData_stat_dn_medium = rhs.muoTrigEffErrData_stat_dn_medium;
        muoTrigEffErrMC_stat_up_medium = rhs.muoTrigEffErrMC_stat_up_medium;
        muoTrigEffErrMC_stat_dn_medium = rhs.muoTrigEffErrMC_stat_dn_medium;
        muoTrigEffErrData_syst_up_medium = rhs.muoTrigEffErrData_syst_up_medium;
        muoTrigEffErrData_syst_dn_medium = rhs.muoTrigEffErrData_syst_dn_medium;
        muoTrigEffErrMC_syst_up_medium = rhs.muoTrigEffErrMC_syst_up_medium;
        muoTrigEffErrMC_syst_dn_medium = rhs.muoTrigEffErrMC_syst_dn_medium;

        errEffSF_badmu_stat_up = rhs.errEffSF_badmu_stat_up;
        errEffSF_badmu_stat_dn = rhs.errEffSF_badmu_stat_dn;
        errEffSF_badmu_syst_up = rhs.errEffSF_badmu_syst_up;
        errEffSF_badmu_syst_dn = rhs.errEffSF_badmu_syst_dn;
        errEffSF_iso_stat_up = rhs.errEffSF_iso_stat_up;
        errEffSF_iso_stat_dn = rhs.errEffSF_iso_stat_dn;
        errEffSF_iso_syst_up = rhs.errEffSF_iso_syst_up;
        errEffSF_iso_syst_dn = rhs.errEffSF_iso_syst_dn;
        errEffSF_reco_stat_up = rhs.errEffSF_reco_stat_up;
        errEffSF_reco_stat_dn = rhs.errEffSF_reco_stat_dn;
        errEffSF_reco_syst_up = rhs.errEffSF_reco_syst_up;
        errEffSF_reco_syst_dn = rhs.errEffSF_reco_syst_dn;
        errEffSF_reco_lowpt_stat_up = rhs.errEffSF_reco_lowpt_stat_up;
        errEffSF_reco_lowpt_stat_dn = rhs.errEffSF_reco_lowpt_stat_dn;
        errEffSF_reco_lowpt_syst_up = rhs.errEffSF_reco_lowpt_syst_up;
        errEffSF_reco_lowpt_syst_dn = rhs.errEffSF_reco_lowpt_syst_dn;
        errEffSF_ttva_stat_up = rhs.errEffSF_ttva_stat_up;
        errEffSF_ttva_stat_dn = rhs.errEffSF_ttva_stat_dn;
        errEffSF_ttva_syst_up = rhs.errEffSF_ttva_syst_up;
        errEffSF_ttva_syst_dn = rhs.errEffSF_ttva_syst_dn;
        errEffSF_trig_stat_up = rhs.errEffSF_trig_stat_up;
        errEffSF_trig_stat_dn = rhs.errEffSF_trig_stat_dn;
        errEffSF_trig_syst_up = rhs.errEffSF_trig_syst_up;
        errEffSF_trig_syst_dn = rhs.errEffSF_trig_syst_dn;

    }
    return *this;
}
/*--------------------------------------------------------------------------------*/
// Muon Set State
/*--------------------------------------------------------------------------------*/
void Muon::setState(int sys)
{
    resetTLV();
    if(sys == NtSys::NOM) return;
    
    float sf = 0;
    if     ( sys == NtSys::MUON_MS_UP ) sf = ms_up;
    else if( sys == NtSys::MUON_MS_DN ) sf = ms_dn;
    else if( sys == NtSys::MUON_ID_UP ) sf = id_up;
    else if( sys == NtSys::MUON_ID_DN ) sf = id_dn;
    else if( sys == NtSys::MUON_SCALE_UP ) sf = scale_up;
    else if( sys == NtSys::MUON_SCALE_DN ) sf = scale_dn;
    else if( sys == NtSys::MUON_SAGITTA_RESBIAS_UP ) sf = sagitta_bias_up;
    else if( sys == NtSys::MUON_SAGITTA_RESBIAS_DN ) sf = sagitta_bias_dn;
    else if( sys == NtSys::MUON_SAGITTA_RHO_UP ) sf = sagitta_rho_up;
    else if( sys == NtSys::MUON_SAGITTA_RHO_DN ) sf = sagitta_rho_dn;
    else return;
    
    this->SetPtEtaPhiE(sf * this->Pt(), this->Eta(), this->Phi(), sf * this->E());
}
/*--------------------------------------------------------------------------------*/
// Muon print
/*--------------------------------------------------------------------------------*/
void Muon::print() const
{
  cout.precision(2);
  cout << fixed << "Mu : q " << setw(2) << q << " pt " << setw(6) << Pt() << " eta " << setw(5) << Eta()
       << " phi " << setw(5) << Phi()
       << " loose " << loose
       << " medium " << medium
       << " isoGradientLoose " << isoGradientLoose
       << " isCosmic " << isCosmic
       << " isBadMuon " << isBadMuon
       //<< " type " << mcType << " origin " << mcOrigin 
       << endl;
  cout.precision(6);
  cout.unsetf(ios_base::fixed);
}

