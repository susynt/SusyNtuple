#include "SusyNtuple/SumwGrabber.h"

// SusyNtuple
#include "SusyNtuple/KinematicTools.h"
#include "SusyNtuple/SusyDefs.h"
using namespace Susy; // everything in SusyNtuple is in this namespace

//ROOT

// std/stl
#include <iomanip> // setw
#include <iostream>
#include <string>
#include <sstream> // stringstream, ostringstream
using namespace std;

//////////////////////////////////////////////////////////////////////////////
SumwGrabber::SumwGrabber() :
    m_dbg(0),
    m_file_suffix(""),
    m_input_chain(nullptr),
    m_outputs_initialized(false),
    m_out_file(nullptr),
    m_sumw_histo(nullptr),
    m_dsid(-1),
    m_mctype(MCType::MCInvalid),
    m_total_events_processed(0),
    m_total_sumw(0),
    m_total_sumw2(0)
{
}
//////////////////////////////////////////////////////////////////////////////
void SumwGrabber::Begin(TTree* /*tree*/)
{
    // call base class' Begin method
    SusyNtAna::Begin(0);
    if(dbg()) cout << "SumwGrabber::Begin" << endl;
    mcWeighter().setVerbose( ( (dbg()>0) ? true : false ) );

    return;
}
//////////////////////////////////////////////////////////////////////////////
void SumwGrabber::initialize_sumw_histograms()
{
    m_outputs_initialized = true;

    m_dsid = nt.evt()->mcChannel;
    m_mctype = static_cast<Susy::MCType>(nt.evt()->mcType);

    stringstream histoname;
    histoname << "h_sumw_" << m_dsid << "_" << MCType2str(m_mctype);
    m_sumw_histo = new TH1D(histoname.str().c_str(), histoname.str().c_str(), 1, 0, 1);


}
//////////////////////////////////////////////////////////////////////////////
Bool_t SumwGrabber::Process(Long64_t entry)
{

    // calling "GetEntry" loads into memory the susyNt class objects for this event
    GetEntry(entry);
    SusyNtAna::clearObjects(); // clear the previous event's objects

    if(!m_outputs_initialized)
        initialize_sumw_histograms();

    // increment the chain entry (c.f. SusyNtuple/SusyNtAna.h)
    m_chainEntry++;

    // evt() provides pointer to the SusyNt::Event class object for this event
    //int run_number = nt.evt()->run;
    //int event_number = nt.evt()->eventNumber;

    m_total_sumw = SusyNtAna::mcWeighter().getSumw(nt.evt(), /*multi-period*/ false);
    m_sumw_histo->Fill(0.0, m_total_sumw);


    return kTRUE;
}
//////////////////////////////////////////////////////////////////////////////
void SumwGrabber::Terminate()
{
    // close SusyNtAna and print timers
    SusyNtAna::Terminate();


    cout << "=================================================================================" << endl;
    cout << "SumwGrabber::Terminate    ";
    cout << "Sumw job done" << endl;
    cout << "SumwGrabber::Terminate    ";
    cout << "DSID: " << m_dsid << "  MCTYPE: " << MCType2str(m_mctype) << "  ";
    cout << "SUMW: " << m_total_sumw << endl;
    cout << "=================================================================================" << endl;

    save_output_file();

    return;
}
//////////////////////////////////////////////////////////////////////////////
void SumwGrabber::save_output_file()
{
    stringstream filename; 
    filename << "sumw_file_" << m_dsid << "_" << MCType2str(m_mctype);
    if(suffix()!="") filename << "_" << suffix();
    filename << ".root";
    m_out_file = new TFile(filename.str().c_str(), "RECREATE");
    if(m_out_file->IsZombie()) {
        cout << "SumwGrabber::initialize_sumw_histograms    ";
        cout << "ERROR Output file is a zombie!" << endl;
        exit(1);
    }

    m_out_file->cd();
    m_sumw_histo->Write();
    m_out_file->Write();
    m_out_file->Close();

    cout << "SumwGrabber::save_output_file    Sumw histogram saved to file: ";
    cout << m_out_file->GetName() << endl;
}
//////////////////////////////////////////////////////////////////////////////
