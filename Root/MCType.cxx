#include "SusyNtuple/MCType.h"

using namespace std;

namespace Susy {

    MCType MCTypeFromStr(const string& type)
    {
        MCType out = MCType::MCInvalid;
        if(type == "mc15b") { out = MCType::MC15b; }
        else if(type == "mc15c") { out = MCType::MC15c; }
        else if(type == "mc16a") { out = MCType::MC16a; }
        else if(type == "mc16c") { out = MCType::MC16c; }
        else if(type == "mc16d") { out = MCType::MC16d; }
        else if(type == "mc16e") { out = MCType::MC16e; }
        return out;
    }

    string MCType2str(const MCType &type)
    {
        string s = "Invalid";
        if(type == MCType::MC15b) { s = "mc15b"; }
        else if(type == MCType::MC15c) { s = "mc15c"; }
        else if(type == MCType::MC16a) { s = "mc16a"; }
        else if(type == MCType::MC16c) { s = "mc16c"; }
        else if(type == MCType::MC16d) { s = "mc16d"; }
        else if(type == MCType::MC16e) { s = "mc16e"; }
        return s;
    }

    vector<string> MCTypes2vec()
    {
        vector<string> types;
        for(int type = 0; type < (int)MCType::MCInvalid; type++) {
            types.push_back(MCType2str((MCType)type));
        }
        return types;
    }

} // namespace Susy
