#include "SusyNtuple/FTaggingEnums.h"

namespace Susy
{
    using std::string;

    std::string FTagEff2str(const FTagEff& eff)
    {
        std::string s = "Unknown";
        switch(eff) {
            case FTagEff::WP70          : s = "WP70"; break;
            case FTagEff::WP77          : s = "WP77"; break;
            case FTagEff::WP85          : s = "WP85"; break;
            case FTagEff::WPInvalid     : s = "WPInvalid"; break;
        } // swtich
        return s;
    }

    std::string FTagAlgo2str(const FTagAlgo& algo)
    {
        std::string s = "Unknown";
        switch(algo) {
            case FTagAlgo::MV2c10       : s = "MV2c10"; break;
            case FTagAlgo::DL1          : s = "DL1"; break;
            case FTagAlgo::AlgoInvalid    : s = "AlgoInvalid"; break;
        } // switch
        return s;
    }

} // namespace Susy
