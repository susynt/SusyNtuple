#include "SusyNtuple/Lepton.h"

using Susy::Lepton;
using Susy::Particle;
 
/*--------------------------------------------------------------------------------*/
// Copy constructor
/*--------------------------------------------------------------------------------*/
Lepton::Lepton(const Lepton &rhs) :
    Particle(rhs),
    idx(rhs.idx),
    q(rhs.q),
    isSUSYToolsBaseline(rhs.isSUSYToolsBaseline),
    isSUSYToolsSignal(rhs.isSUSYToolsSignal),
    ptvarcone20(rhs.ptvarcone20),
    ptcone20(rhs.ptcone20),
    ptvarcone30(rhs.ptvarcone30),
    ptcone30(rhs.ptcone30),
    ptvarcone40(rhs.ptvarcone40),
    ptcone40(rhs.ptcone40),
    etcone20(rhs.etcone20),
    topoetcone20(rhs.topoetcone20),
    topoetcone30(rhs.topoetcone30),
    topoetcone40(rhs.topoetcone40),
    ptvarcone20_TightTTVA_pt1000(rhs.ptvarcone20_TightTTVA_pt1000),
    ptvarcone30_TightTTVA_pt1000(rhs.ptvarcone30_TightTTVA_pt1000),
    ptvarcone30_TightTTVA_pt500(rhs.ptvarcone30_TightTTVA_pt500),
    ptcone20_TightTTVA_pt1000(rhs.ptcone20_TightTTVA_pt1000),
    ptcone20_TightTTVA_pt500(rhs.ptcone20_TightTTVA_pt500),
    isoGradientLoose(rhs.isoGradientLoose),
    isoGradient(rhs.isoGradient),
    isoLooseTrackOnly(rhs.isoLooseTrackOnly),
    isoLoose(rhs.isoLoose),
    isoFixedCutTightTrackOnly(rhs.isoFixedCutTightTrackOnly),
    isoFCLoose(rhs.isoFCLoose),
    isoFCTight(rhs.isoFCTight),
    isoFCTightTrackOnly(rhs.isoFCTightTrackOnly),
    isoFCHighPtCaloOnly(rhs.isoFCHighPtCaloOnly),
    isoGradientLooseCloseBy(rhs.isoGradientLooseCloseBy),
    isoFixedCutTightTrackOnlyCloseBy(rhs.isoFixedCutTightTrackOnlyCloseBy),
    d0(rhs.d0),
    errD0(rhs.errD0),
    d0sigBSCorr(rhs.d0sigBSCorr),
    z0(rhs.z0),
    errZ0(rhs.errZ0),
    mcType(rhs.mcType),
    mcOrigin(rhs.mcOrigin),
    mcFirstEgMotherTruthType(rhs.mcFirstEgMotherTruthType),
    mcFirstEgMotherTruthOrigin(rhs.mcFirstEgMotherTruthOrigin),
    mcFirstEgMotherPdgId(rhs.mcFirstEgMotherPdgId),
    effSF(rhs.effSF),
    trigBits(rhs.trigBits)
{
}
/*--------------------------------------------------------------------------------*/
// Assignment operator
/*--------------------------------------------------------------------------------*/
Lepton& Lepton::operator=(const Lepton &rhs)
{
  if (this != &rhs) {
    Particle::operator=(rhs);
    idx = rhs.idx;
    q = rhs.q; 
    isSUSYToolsBaseline = rhs.isSUSYToolsBaseline;
    isSUSYToolsSignal = rhs.isSUSYToolsSignal;
    ptvarcone20 = rhs.ptvarcone20;
    ptcone20 = rhs.ptcone20;
    ptvarcone30 = rhs.ptvarcone30;
    ptcone30 = rhs.ptcone30;
    ptvarcone40 = rhs.ptvarcone40;
    ptcone40 = rhs.ptcone40;
    etcone20 = rhs.etcone20;
    topoetcone20 = rhs.topoetcone20;
    topoetcone30 = rhs.topoetcone30;
    topoetcone40 = rhs.topoetcone40;
    ptvarcone20_TightTTVA_pt1000 = rhs.ptvarcone20_TightTTVA_pt1000;
    ptvarcone30_TightTTVA_pt1000 = rhs.ptvarcone30_TightTTVA_pt1000;
    ptvarcone30_TightTTVA_pt500 = rhs.ptvarcone30_TightTTVA_pt500;
    ptcone20_TightTTVA_pt1000 = rhs.ptcone20_TightTTVA_pt1000;
    ptcone20_TightTTVA_pt500 = rhs.ptcone20_TightTTVA_pt500;
    isoGradientLoose = rhs.isoGradientLoose;
    isoGradient = rhs.isoGradient;
    isoLooseTrackOnly = rhs.isoLooseTrackOnly;
    isoLoose = rhs.isoLoose;
    isoFixedCutTightTrackOnly = rhs.isoFixedCutTightTrackOnly;
    isoFCLoose = rhs.isoFCLoose;
    isoFCTight = rhs.isoFCTight;
    isoFCTightTrackOnly = rhs.isoFCTightTrackOnly;
    isoFCHighPtCaloOnly = rhs.isoFCHighPtCaloOnly;
    isoGradientLooseCloseBy = rhs.isoGradientLooseCloseBy;
    isoFixedCutTightTrackOnlyCloseBy = rhs.isoFixedCutTightTrackOnlyCloseBy;
    d0 = rhs.d0;
    errD0 = rhs.errD0;
    d0sigBSCorr = rhs.d0sigBSCorr;
    z0 = rhs.z0;
    errZ0 = rhs.errZ0;
    mcType = rhs.mcType;
    mcOrigin = rhs.mcOrigin;
    mcFirstEgMotherTruthType = rhs.mcFirstEgMotherTruthType,
    mcFirstEgMotherTruthOrigin = rhs.mcFirstEgMotherTruthOrigin,
    mcFirstEgMotherPdgId = rhs.mcFirstEgMotherPdgId,
    effSF = rhs.effSF;
    trigBits = rhs.trigBits;
  }
  return *this;
}
