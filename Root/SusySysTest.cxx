#include "SusyNtuple/SusySysTest.h"
#include "SusyNtuple/SusyNtSys.h"

//std/stl
#include <iostream>
#include <string>
#include <iomanip>
using namespace std;


SusySysTest::SusySysTest() :
    m_input_chain(nullptr),
    m_dbg(false)
{
    m_skip_weighter = true;
}

SusySysTest::~SusySysTest()
{
}

void SusySysTest::Begin(TTree* /*tree*/)
{
    SusyNtAna::Begin(0);
}

Bool_t SusySysTest::Process(Long64_t entry)
{
    GetEntry(entry);
    SusyNtAna::clearObjects();

    m_chainEntry++;
    if(m_chainEntry % 500 == 0 || dbg()) {
        cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
        cout << "SusySysTest::Process    **** Processing entry " << setw(6) << m_chainEntry
            << " event " << setw(7) << nt.evt()->eventNumber << " ***** " << endl;
    }

    for(int i = 0; i < NtSys::SYS_UNKNOWN; i++) {
        cout << "=====================================================" << endl;
        cout << "SusySysTest    At systematic : " << i << "  (" << SusyNtSysNames.at((SusyNtSys)i) << ")" << endl;
        SusyNtAna::selectObjects((SusyNtSys)i);
    }

    return true;
}

void SusySysTest::Terminate()
{

    return;
}



