#include "SusyNtuple/PhotonSelector.h"
#include "SusyNtuple/Photon.h"

#include <iostream>

using std::cout;
using std::endl;
using std::string;

namespace Susy {
// -------------------------------------------------------------------------------------------- //
PhotonSelector* PhotonSelector::build(const AnalysisType &a, bool verbose) {
    PhotonSelector *p;
    switch(a) {
    case AnalysisType::Ana_2Lep :
        p = new PhotonSelector_2Lep();
        p->setSignalIsolation(Isolation::FixedCutTight);
        break;
    default :
        cout << "PhotonSelector::build(): unknown analysis type '" << AnalysisType2str(a) << "'"
             << " returning vanilla PhotonSelector" << endl;
        p = new PhotonSelector();
    } // switch
    p->setVerbose(verbose);
    return p;
}

// -------------------------------------------------------------------------------------------- //
// Constructor
// -------------------------------------------------------------------------------------------- //
PhotonSelector::PhotonSelector():
    m_signalIsolation(Isolation::IsolationInvalid),
    m_verbose(false)
    {}

// -------------------------------------------------------------------------------------------- //
bool PhotonSelector::isBaseline(const Photon* ph) {
    return ph != nullptr
        && ph->tight
        && ph->Pt() > 25
        && fabs(ph->clusEtaBE) < 2.37
        && (ph->authorPhoton || ph->authorAmbiguous)
        ;
}
// -------------------------------------------------------------------------------------------- //
bool PhotonSelector::isSignal(const Photon* ph) {
    return isBaseline(ph)
        && ph->isoFixedCutTight
        ;
}
// -------------------------------------------------------------------------------------------- //
// begin PhotonSelector_2Lep Ana_2Lep
// -------------------------------------------------------------------------------------------- //
// Use default
} // namespace Susy
