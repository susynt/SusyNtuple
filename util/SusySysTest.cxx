//std/stl
#include <string>
#include <cstdlib>
#include <iostream>
using namespace std;

//ROOT
#include "TChain.h"

//SusyNtuple
#include "SusyNtuple/SusySysTest.h"
#include "SusyNtuple/ChainHelper.h"
#include "SusyNtuple/string_utils.h"

void help()
{
    cout << "-------------------------------------------------------" << endl;
    cout << "  SusySysTest" << endl;
    cout << endl;
    cout << " Options:" << endl;
    cout << "  -i               input file (ROOT file, *.txt file, or dir)" << endl;
    cout << "  -n               number of events to process (default: all)" << endl;
    cout << "  -d               set debug on (default: false)" << endl;
    cout << "  -h|--help        print this help message" << endl;
    cout << endl;
    cout << "-------------------------------------------------------" << endl;
}

int main(int argc, char** argv)
{
    int n_events = -1;
    bool dbg = false;
    string input = "";

    for(int i = 1; i < argc; i++) {
        if(strcmp(argv[i], "-n") == 0) n_events = atoi(argv[++i]);
        else if(strcmp(argv[i], "-i") == 0) input = argv[++i];
        else if(strcmp(argv[i], "-d") == 0) dbg = true;
        else if(strcmp(argv[i], "-h") == 0) { help(); return 0; }
        else if(strcmp(argv[i], "--help") == 0) { help(); return 0; }
        else {
            cout << "unknown input argument '" << argv[i] << "' provided" << endl;
            return 1;
        }
    }

    if(input.empty()) {
        cout << "you did not provide an input file" << endl;
        return 1;
    }

    TChain* chain = new TChain("susyNt");
    ChainHelper::addInput(chain, input, 1);
    Long64_t n_entries = chain->GetEntries();
    if(n_events < 0) n_events = n_entries;

    SusySysTest* ana = new SusySysTest();
    ana->set_chain(chain);
    ana->set_debug(dbg);
    ana->setAnaType(AnalysisType::Ana_2Lep);

    cout << "-------------------------------------------------------" << endl;
    cout << " SusySysTest" << endl;
    cout << "  > input          : " << input << endl;
    cout << "  > n events       : " << n_events << endl;
    cout << "  > verbose        : " << dbg << endl;
    cout << "-------------------------------------------------------" << endl;

    if(n_events > 0) chain->Process(ana, input.c_str(), n_events);
    delete chain;
    cout << "SusySysTest    Job done!" << endl;


    return 0;
}
