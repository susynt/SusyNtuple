//SusyNtuple
#include "SusyNtuple/SumwGrabber.h"
#include "SusyNtuple/ChainHelper.h"
#include "SusyNtuple/string_utils.h"

//std/stl
#include <iostream>
#include <cstdlib>
#include <string>
using namespace std;

//ROOT
#include "TChain.h"

//////////////////////////////////////////////////////
//
// grabSumw 
// Executable auto-generated with SusyNtuple/make_susy_skeleton on 2018-04-13 07:52
//
//
//////////////////////////////////////////////////////


void help()
{
    cout << "----------------------------------------------------------" << endl;
    cout << " grabSumw" << endl;
    cout << endl;
    cout << "  Options:" << endl;
    cout << "   -d          debug level (integer) (default: 0)" << endl;
    cout << "   -i          input file (ROOT file, *.txt file, or directory)" << endl;
    cout << "   -s          add a suffix to the output ROOT file (default: "")" << endl;
    cout << "   -h          print this help message" << endl;
    cout << endl;
    cout << "  Example Usage:" << endl;
    cout << "   grabSumw -i susyNt.root" << endl;
    cout << "----------------------------------------------------------" << endl;
}

int main(int argc, char** argv)
{

    /////////////////////////
    // cmd line options
    /////////////////////////

    int n_events = -1;
    int dbg = 0;
    string input = "";
    string suffix = "";

    for(int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-d") == 0) dbg = atoi(argv[++i]);
        else if (strcmp(argv[i], "-i") == 0) input = argv[++i];
        else if (strcmp(argv[i], "-h") == 0) { help(); return 0; }
        else if (strcmp(argv[i], "-s") == 0) { suffix = argv[++i]; }
        else {
            cout << "grabSumw    Unknown command line argument '" << argv[i] << "', exiting" << endl;
            help();
            return 1;
        }
    } // i

    if(input.empty()) {
        cout << "grabSumw    You must specify an input" << endl;
        return 1;
    }


    /////////////////////////////////////////////////////////
    // Build the TChain object
    // For SusyNtuple analysis, the chain name is susyNt
    /////////////////////////////////////////////////////////
    TChain* chain = new TChain("susyNt");

    // use ChainHelper to infer the input type (ROOT file, *.txt, or dir/)
    // and build the full chain of the input files
    // (c.f. SusyNtuple/ChainHelper.h)
    ChainHelper::addInput(chain, input, dbg>0);
    //Long64_t n_entries_in_chain = chain->GetEntries();
    // let's see what it looks like
    chain->ls();

    /////////////////////////////////////////////////////////
    // Build the TSelector object
    // SusyNt analyses inheriting from SusyNtAna must
    // build their own TSelector looper
    /////////////////////////////////////////////////////////
    SumwGrabber* analysis = new SumwGrabber();
    analysis->setAnaType(AnalysisType::Ana_2Lep);

    analysis->set_debug(dbg);
    analysis->setSampleName(ChainHelper::sampleName(input, dbg>0)); // SusyNtAna setSampleName (c.f. SusyNtuple/SusyNtAna.h)
    analysis->set_chain(chain); // propagate the TChain to the analysis
    analysis->set_suffix(suffix);
    n_events = 1;
//    if(n_events < 0) n_events = n_entries_in_chain;

//    cout << "---------------------------------------------------------" << endl;
//    cout << " Total entries in input chain          : " << n_entries_in_chain << endl;
//    cout << " Total entries to process for analysis : " << n_events << endl;
//    cout << "---------------------------------------------------------" << endl;

    
    // call TChain Process to star the TSelector looper over the input TChain
    if(n_events > 0) chain->Process(analysis, input.c_str(), 1);

    cout << endl;
    cout << "grabSumw    Analysis loop done" << endl;

    delete chain;
    delete analysis;
    return 0;
}
