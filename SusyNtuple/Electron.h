// Dear emacs, this is -*- c++ -*-
#ifndef SUSYNTUPLE_ELECTRON_H
#define SUSYNTUPLE_ELECTRON_H

// std
#include <vector>

// SusyNtuple
#include "SusyNtuple/Lepton.h"
#include "SusyNtuple/SusyNtSys.h"
#include "SusyNtuple/ElectronId.h"

namespace Susy
{
/// Electron class
class Electron : public Lepton
{
public:
    Electron(){ clear(); }
    virtual ~Electron(){};
    Electron(const Electron &);
    /** Assignment operator */
    Electron& operator=(const Electron &);

    /// Author information
    // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/EGammaIdentificationRun2
    int author;
    bool authorElectron;            ///< electron reconstructed exclusively as an electron (author == 1)
    bool authorAmbiguous;           ///< electron reconstructed both as electron and photon (author == 16)

    // Cluster/track variables
    float clusE;              ///< CaloCluster energy
    float clusEta;            ///< CaloCluster eta
    float clusPhi;            ///< CaloCluster phi
    float clusEtaBE;          ///< CaloCluster eta (2nd sampling) EMB/EMEC combined barycenter eta
    float clusPhiBE;          ///< CaloCluster phi (2nd sampling) EMB/EMEC combined barycenter phi
    float trackPt;            ///< ID track pt
    float trackEta;           ///< ID track eta

    //LH quality flags
    bool veryLooseLLH;        ///< veryLooseLLH
    bool looseLLH;            ///< looseLLH
    bool looseLLHBLayer;      ///< LooseAndBLayerLLH
    bool mediumLLH;           ///< mediumLLH
    bool tightLLH;            ///< tightLLH
    
    // Charge flip
    bool isChargeFlip; ///< result from ElectronChargeIDSelectorTool
    double chargeFlipTaggerScore;       ///< BDT value from ElectronChargeIDSelectorTool

    // efficiency SF per electron LH WP
    std::vector<float> eleEffSF;
    std::vector<float> eleEffChargeFlipSF;
    // trigger efficiency SF per electron LH WP
    std::vector<float> eleTrigSF_single;
    std::vector<float> eleTrigSF_double;
    std::vector<float> eleTrigSF_mixed;

    std::vector<int> sharedMuTrk; ///< Indices of SusyNt preMuons with which this electron's track is shared
    std::vector<int> sharedEleEleTrk; ///< Indices of SusyNt preElectrons which this electron's track is shared

    //AT: This is insane >50!
    float res_all_dn;
    float res_all_up;
    float scale_af2_dn;
    float scale_af2_up;
    float scale_all_dn;
    float scale_all_up;

    std::vector<float> errEffSF_id_up;     // EL_EFF_ID_TOTAL_Uncorr_UP 
    std::vector<float> errEffSF_id_dn;     // EL_EFF_ID_TOTAL_Uncorr_DN 
    std::vector<float> errEffSF_reco_up;   // EL_EFF_Reco_TOTAL_Uncorr_UP
    std::vector<float> errEffSF_reco_dn;   // EL_EFF_Reco_TOTAL_Uncorr_DN
    std::vector<float> errEffSF_iso_up;    // EL_EFF_Iso_TOTAL_Uncorr_UP
    std::vector<float> errEffSF_iso_dn;    // EL_EFF_Iso_TOTAL_Uncorr_DN

    std::vector<float> errEffSF_trig_up_single;   // EL_EFF_Trigger_TOTAL_Uncorr_UP
    std::vector<float> errEffSF_trig_dn_single;   // EL_EFF_Trigger_TOTAL_Uncorr_DN
    std::vector<float> errEffSF_trig_up_double;   // EL_EFF_Trigger_TOTAL_Uncorr_UP
    std::vector<float> errEffSF_trig_dn_double;   // EL_EFF_Trigger_TOTAL_Uncorr_DN
    std::vector<float> errEffSF_trig_up_mixed;   // EL_EFF_Trigger_TOTAL_Uncorr_UP
    std::vector<float> errEffSF_trig_dn_mixed;   // EL_EFF_Trigger_TOTAL_Uncorr_DN

    // Polymorphism, baby!!
    bool isEle() const { return true;  }
    bool isMu()  const { return false; }

    /// Shift energy up/down for systematic
    void setState(int sys);

    /// Print method
    void print() const;

    /// Clear vars
    void clear(){
      author = 0;
      authorElectron = authorAmbiguous = false;
      clusE = clusEta = clusPhi = clusEtaBE = clusPhiBE = trackPt = trackEta = 0;
      veryLooseLLH = looseLLH = looseLLHBLayer = mediumLLH = tightLLH = false;
      isChargeFlip = false;
      chargeFlipTaggerScore = -99;
      sharedMuTrk.assign(50,0);
      sharedEleEleTrk.assign(50,0);
      
      res_all_dn= res_all_up= 0;
      scale_all_dn=scale_all_up=0;
      scale_af2_dn=scale_af2_up=0;

     eleEffSF.assign(ElectronId::ElectronIdInvalid, 1);
     eleEffChargeFlipSF.assign(ElectronId::ElectronIdInvalid, 1);
     eleTrigSF_single.assign(ElectronId::ElectronIdInvalid, 1);
     eleTrigSF_double.assign(ElectronId::ElectronIdInvalid, 1);
     eleTrigSF_mixed.assign(ElectronId::ElectronIdInvalid, 1);
     errEffSF_id_up.assign(ElectronId::ElectronIdInvalid, 0);
     errEffSF_id_dn.assign(ElectronId::ElectronIdInvalid, 0);
     errEffSF_reco_up.assign(ElectronId::ElectronIdInvalid, 0);
     errEffSF_reco_dn.assign(ElectronId::ElectronIdInvalid, 0);
     errEffSF_iso_up.assign(ElectronId::ElectronIdInvalid, 0);
     errEffSF_iso_dn.assign(ElectronId::ElectronIdInvalid, 0);
     errEffSF_trig_up_single.assign(ElectronId::ElectronIdInvalid, 0);
     errEffSF_trig_dn_single.assign(ElectronId::ElectronIdInvalid, 0);
     errEffSF_trig_up_double.assign(ElectronId::ElectronIdInvalid, 0);
     errEffSF_trig_dn_double.assign(ElectronId::ElectronIdInvalid, 0);
     errEffSF_trig_up_mixed.assign(ElectronId::ElectronIdInvalid, 0);
     errEffSF_trig_dn_mixed.assign(ElectronId::ElectronIdInvalid, 0);

      Lepton::clear();
    }

    ClassDef(Electron, 30);
};
} //Susy
#endif
