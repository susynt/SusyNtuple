//  -*- c++ -*-
#ifndef SUSY_JETSELECTOR_H
#define SUSY_JETSELECTOR_H

#include "SusyNtuple/SusyNtSys.h"
#include "SusyNtuple/AnalysisType.h"
#include "SusyNtuple/SusyDefs.h" // JetVector
#include "SusyNtuple/FTaggingEnums.h"

namespace Susy {

class Jet;

///  A class to select jets
/**
   The generic JetSelector implements a generic definition

   Analysis-dependent criteria should be implemented in your
   analysis-specific class inheriting from JetSelector.

   The analysis-specific selector should be instantiated with JetSelector::build().

   Please avoid having thresholds as input parameters of the selection
   functions (including default arguments). Prefer explicit values in
   the function definition, for example:
   \code{.cpp}
   JetSelector_myAnalysis::isSignalJet(const Jet* jet)
   {
       return (jet->Pt>20.0 && fabs(jet->Eta())<2.4);
   }
   \endcode
   This design has the following advantages:

   1. the default code does not have many 'if else' implementing the
   analysis-specific selection

   2. the common code can still be shared across groups

   3. the analysis-specific thresholds are explicitly written in the
   analysis-specific implementation of the overloaded function. This
   allows to almost copy-and-paste the recommendation from the
   analysis wiki.

   See test_JetSelector.cxx for an example of how this class can be used

   Note to self:
   design choices tested in https://gist.github.com/gerbaudo/7854402

   Note to self:
   since we usually store Jet* in vectors, all the is___() functions
   take a Jet* as input.

   \todo make the is__() functions const
   \todo convert the const Jet* argument to const Jet& (and skip all the if(jet) checks)

   davide.gerbaudo@gmail.com, Aug 2014
*/
class JetSelector
{
public:
    /// provide analysis-specific selector (or vanilla one if analysis is unknown)
    /**
       The user owns the selector (i.e. should use std::shared_ptr
       or delete it when done with it).
    */
    static JetSelector* build(const AnalysisType &a, bool verbose);

    JetSelector(); ///< Default ctor
    virtual ~JetSelector() {}; ///< dtor (for now we don't have anything to delete)
    JetSelector& setSystematic(const NtSys::SusyNtSys&); ///< set syst (needed for example for jvt)
    /// whether the jet is b-tagged
    virtual bool passFlavTagger(const Jet* jet, FTagAlgo tagger, FTagEff eff_wp);
    virtual bool isBJet(const Jet* jet);
    virtual bool isBJetOR(const Jet* jet, bool useUpperPt = true);

    virtual bool isForward(const Jet* jet);
    /// The jet-vertex-fraction requirement: usually applied to low-pt central jets
    static bool passJvt(const Jet* jet, bool bjet=false);

    virtual bool isBaseline(const Jet* jet); ///< often analysis-dependent
    virtual bool isSignal(const Jet* jet); ///< often analysis-dependent
    // The total JVT SF should be determined for all jets before the JVT cut is applied.
    // If the JVT selection is applied to baseline (signal) jets, 
    // then use all baseline (signal) jets minus the JVT cut.
    // This is accomplished by defining isForJVTSF as the same as baseline (signal) without the JVT cut
    virtual bool isForJVTSF(const Jet* jet);

    bool verbose() const { return m_verbose; }
    JetSelector& setVerbose(bool v) { m_verbose = v; return *this; }
    virtual size_t count_B_jets(const JetVector &jets) /*const*/;
    /// count forward jets \todo const
    virtual size_t count_F_jets(const JetVector &jets) /*const*/;

    /// set flavor tagger
    FTagAlgo ftagAlgo() const { return m_ftag_algo; }
    JetSelector& setFTagAlgo(const FTagAlgo& algo) { m_ftag_algo = algo; return *this; }

    /// set flavor tagging working point
    FTagEff ftagEff() const { return m_ftag_wp; }
    JetSelector& setFTagEff(const FTagEff& eff) { m_ftag_wp = eff; m_ftagOR_wp = eff; return *this; }
    FTagEff ftagOREff() const { return m_ftagOR_wp; }
    JetSelector& setFTagOREff(const FTagEff& eff) { m_ftagOR_wp = eff; return *this; }

    float bjet_sf(const Jet* jet, bool inefficiency_sf = false);

    float get_ftag_threshold(const FTagAlgo& algo, const FTagEff& eff);
    

    //@{
    /**
       b-tag operating points for AntiKt4EMTopoJets with MV2c20 and DL1
       last checked on 2019-07-09
       see:
    // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTaggingBenchmarksRelease21
    */
    static float dl1_70efficiency() { return 2.01504683495; }
    static float dl1_77efficiency() { return 1.44686043262; }
    static float dl1_85efficiency() { return 0.463453054428; }
    static float mv2c10_70efficiency() { return 0.830283045769; }
    static float mv2c10_77efficiency() { return 0.643362402916; }
    static float mv2c10_85efficiency() { return 0.108029261231; }

protected :

    FTagEff m_ftag_wp;
    FTagEff m_ftagOR_wp;
    FTagAlgo m_ftag_algo;

private:
    NtSys::SusyNtSys m_systematic; ///< current syst variation
    bool m_verbose; ///< toggle verbose messages
    }; // class JetSelector


//----------------------------------------------------------
//
// End generic selector, begin analysis-specific ones
//
//----------------------------------------------------------

/// implements jet selection from https://twiki.cern.ch/twiki/bin/view/AtlasProtected/DirectStop2Lepton
class JetSelector_2Lep : public JetSelector {};

class JetSelector_Stop2L : public JetSelector
{
    bool isBaseline(const Jet* jet) override final;
    bool isSignal(const Jet* jet) override final;
    bool isForJVTSF(const Jet* jet) override final;
};

class JetSelector_HLFV : public JetSelector
{
    bool isBaseline(const Jet* jet) override final;
    bool isSignal(const Jet* jet) override final;
    bool isForJVTSF(const Jet* jet) override final;
};
} // Susy

#endif
