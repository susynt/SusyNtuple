#ifndef SUSYNTUPLE_MCTYPE_H
#define SUSYNTUPLE_MCTYPE_H

#include <string>
#include <vector>

namespace Susy {

    enum MCType {
        MC15b=0,
        MC15c,
        MC16a,
        MC16c,
        MC16d,
        MC16e,
        MCInvalid
    };

    MCType MCTypeFromStr(const std::string& type = "");
    std::string MCType2str(const MCType& type);
    std::vector<std::string> MCTypes2vec();

} // namespace Susy

#endif
