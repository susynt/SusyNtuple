// Dear emacs, this is -*- c++ -*-
#ifndef SUSYNTUPLE_JET_H
#define SUSYNTUPLE_JET_H

#include "SusyNtuple/Particle.h"
#include "SusyNtuple/SusyNtSys.h"

namespace Susy
{
/// Jet class
class Jet : public Particle
{
public:
    Jet(){ clear(); }
    virtual ~Jet(){};
    Jet(const Jet &);
    /** Assignment operator */
    Jet& operator=(const Jet &);

    int idx;                  ///< index of jet in jet collection stored in SusyNt

    float jvt;                ///< Jet vertex tagger
    float detEta;             ///< Detector eta
    float emfrac;             ///< EM fraction
    int truthLabel;           ///< Flavor truth label
    bool matchTruth;          ///< Matches truth jet
    int nTracks;              ///< Number of tracks associated with jet
    float sumTrkPt;           ///< Sum pT of all tracks associated with this jet

    // btagging
    bool bjet;                ///< Is b-jet a la SUSYTools (70% w.p.)
    float effscalefact;       ///< B-tag SF a la SUSYTools (70% w.p.)  
    float btagSF_mv2c10_77;   ///< B-tag SF for MV2c10 tagger at 77% WP
    float btagSF_mv2c10_85;   ///< B-tag SF for MV2c10 tagger at 85% WP
    float btagSF_dl1_77;      ///< B-tag SF for DL1 tagger at 77%
    float btagSF_dl1_85;      ///< B-tag SF for DL1 tagger at 85%
    float btagNotSF_mv2c10_77;///< B-tag inefficiency SF for MV2c10 tagger at 77% WP
    float btagNotSF_mv2c10_85;///< B-tag inefficiency SF for MV2c10 tagger at 85% WP
    float btagNotSF_dl1_77;   ///< B-tag inefficiency SF for DL1 tagger at 77%
    float btagNotSF_dl1_85;   ///< B-tag inefficiency SF for DL1 tagger at 85%
    float mv2c10;             ///< MV2c10 btag discriminant
    float dl1;                ///< DL1 btag discriminant

    // jvt efficiency SF
    float jvtEff;             ///< JvtEfficiency nominal SF
    float jvtEff_up;          ///< JvtEfficiencyUp
    float jvtEff_dn;          ///< JvtEfficiencyDn

    // Flags/variables for cleaning
    bool isBad;      ///< bad jet flag computed with SUSYTools

    // Systematics
    float jer;                       ///< jet energy resolution
    float jer_datamc_up;    // JET_JER_DataVsMC_UP
    float jer_datamc_dn;    // JET_JER_DataVsMC_DN
    float jer_effective_np1_up; // JET_JER_EffectiveNP_1_UP
    float jer_effective_np1_dn; // JET_JER_EffectiveNP_1_DN
    float jer_effective_np2_up; // JET_JER_EffectiveNP_2_UP
    float jer_effective_np2_dn; // JET_JER_EffectiveNP_2_DN
    float jer_effective_np3_up; // JET_JER_EffectiveNP_3_UP
    float jer_effective_np3_dn; // JET_JER_EffectiveNP_3_DN
    float jer_effective_np4_up; // JET_JER_EffectiveNP_4_UP
    float jer_effective_np4_dn; // JET_JER_EffectiveNP_4_DN
    float jer_effective_np5_up; // JET_JER_EffectiveNP_5_UP
    float jer_effective_np5_dn; // JET_JER_EffectiveNP_5_DN
    float jer_effective_np6_up; // JET_JER_EffectiveNP_6_UP
    float jer_effective_np6_dn; // JET_JER_EffectiveNP_6_DN
    float jer_effective_np7rest_up; // JET_JER_EffectiveNP_7rest_UP
    float jer_effective_np7rest_dn; // JET_JER_EffectiveNP_7rest_DN


    float eta_intercal_up; // JET_EtaIntercalibration_NonClosure_UP
    float eta_intercal_dn; // JET_EtaIntercalibration_NonClosure_DN
    std::vector<float> groupedNP;    ///< Reduced 3*2 JES systematics
    std::vector<float> jerNP;
    std::vector<float> jesNP;
    std::vector<float> FTSys; ///< Flavor Tagger syst: B(10*2), C(4*2), light(12*2) jet systematics

    //ADD SYS!!! 18x2 + JER
    /*
    std::vector<float> bjes;
    std::vector<float> effNp;
    std::vector<float> etaInter;
    std::vector<float> flavor;
    std::vector<float> pileup;
    std::vector<float> punchThrough;
    std::vector<float> singlePart;
    //std::vector<float> relativeNC;
    */

    // Shift energy for systematic
    void setState(int sys);

    // Return flavor tag systematics
    float getFTSys(Susy::NtSys::SusyNtSys sys);
    void  setFTSys(Susy::NtSys::SusyNtSys sys, double scale);

    // Print method
    void print() const;

    // Clear vars
    void clear(){
        idx = 0;
        jvt = truthLabel = nTracks = 0;
        sumTrkPt = 0;
        matchTruth = false;
        bjet = false;
        effscalefact = 0.;
        btagSF_mv2c10_77 = btagSF_mv2c10_85 = btagSF_dl1_77 = btagSF_dl1_85 = 1;
        btagNotSF_mv2c10_77 = btagNotSF_mv2c10_85 = btagNotSF_dl1_77 = btagNotSF_dl1_85 = 1;
        detEta = 0;
        emfrac = 0;
        mv2c10 = 0;
        dl1 = 0;
        jvtEff = 1.0;
        jvtEff_up = jvtEff_dn = 0;
        isBad = false;
        
        jer = 0;
        jer_datamc_up = jer_datamc_dn = 0;
        jer_effective_np1_up = jer_effective_np1_dn = 0;
        jer_effective_np2_up = jer_effective_np2_dn = 0;
        jer_effective_np3_up = jer_effective_np3_dn = 0;
        jer_effective_np4_up = jer_effective_np4_dn = 0;
        jer_effective_np5_up = jer_effective_np5_dn = 0;
        jer_effective_np6_up = jer_effective_np6_dn = 0;
        jer_effective_np7rest_up = jer_effective_np7rest_dn = 0;
    
        groupedNP.resize(3*2,0.0);
        jerNP.resize(14*2,0.0);
        jesNP.resize(22*2,0.0);
        FTSys.resize(5*2,0.0);
        eta_intercal_up = 0;
        eta_intercal_dn = 0;

        /*
        bjes.resize(2,0.0);
        effNp.resize(6*2,0.0);
        etaInter.resize(2*2,0.0);
        flavor.resize(2*2,0.0);
        pileup.resize(4*2,0.0);
        punchThrough.resize(2,0.0);
        //relativeNC.resize(2,0.0);
        singlePart.resize(2,0.0);
        */
     

        Particle::clear();
    }

    ClassDef(Jet, 27);
};
} // Susy
#endif
