// Dear emacs, this is -*- c++ -*-
#ifndef SUSYNTUPLE_EVENT_H
#define SUSYNTUPLE_EVENT_H

//SusyNtuple
#include "SusyNtuple/SusyNtSys.h"
#include "SusyNtuple/SusyDefs.h" // DataStream
#include "SusyNtuple/TriggerTools.h" // DileptonTrigTuple
#include "SusyNtuple/MCType.h"

#include "TBits.h"
#include "TObject.h"

namespace Susy
{

/// Event class
class Event: public TObject
{
public:
    Event() :
        trigBits(m_nTriggerBits)
        {
            clear();
        }
    virtual ~Event(){};

    unsigned int run;   ///< run number
    unsigned int eventNumber; ///< event number
    unsigned int lb;          ///< lumi block number
    DataStream stream;        ///< DataStream enum, defined in SusyDefs.h
    int treatAsYear;          ///< SUSYObjDef::treatAsYear (equals either 2015 or 2016)

    bool isMC;                ///< is MC flag
    int mcType;               ///< corresponds to Susy::MCType (c.f. SusyNtuple/MCType.h)
    unsigned int mcChannel;   ///< MC channel ID number (mc run number)
    float w;                  ///< MC generator weight
    uint64_t initialNumberOfEvents;  ///< initial number of events processed before any xAOD skimming (before derivation process)
    double sumOfEventWeights;        ///< sum of MC event weights before any xAOD skimming (before derivation process)
    double sumOfEventWeightsSquared; ///< sum of MC event weights squared before any xAOD skimming (before derivation process)

    unsigned int larError;    ///< LAr error flag

    unsigned int nVtx;        ///< number of good vertices
    float avgMu;              ///< average interactions per bunch crossing
    float avgMuDataSF;        ///< average interactions per bunch crossing with Data SF applied
    float actualMu;           ///< actual interactions per bunch crossing (not BC averaged)
    float actualMuDataSF;     ///< actual interactions per bunch crossin with Data SF applied

    float pvX;
    float pvY;
    float pvZ;

    float beamPosX;
    float beamPosY;
    float beamPosZ;
    float beamPosSigmaX;
    float beamPosSigmaY;
    float beamPosSigmaZ;

    int nTracksAtPV;          ///< number of tracks associated with the primary vertex
    int nNeutralsAtPV;        ///< number of neutrals associated with the primary vertex

    /// SUSY subprocess ID's: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/SUSYSignalUncertainties#Subprocess_IDs
    int susyFinalState;       ///< Susy process
    int hDecay;               ///< Higgs decay (see WhTruthExtractor)
    bool eventWithSusyProp;   ///< Event generated involving SUSY propagators
    ///< (See Serhan's msg http://goo.gl/ucwl9)
    int susySpartId1;         ///< SUSY sparticle 1 pdg ID
    int susySpartId2;         ///< SUSY sparticle 2 pdg ID
    float susy3BodyLeftPol;   ///< Left polarized stop
    float susy3BodyRightPol;  ///< Right polarized stop (70%)
    float susy3BodyOnlyMass;  ///< No new angle, test for just mass - ASM-2016-04-25


    //  sherpa 2.2 V+jets reweighting
    // see: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/CentralMC15ProductionList#NEW_Sherpa_v2_2_V_jets_NJet_rewe
    bool isSherpaVjetsSample;
    float sherpa22VjetsWeight;

    //unsigned int trigFlags; ///< Event level trigger bits
    long long trigFlags;      ///< Event level trigger bits

    TBits               trigBits;
    static const size_t m_nTriggerBits=64;

    // Dilepton trigger matching information
    std::map<DileptonTrigTuple, int> m_dilepton_trigger_matches; 
    std::map<DileptonTrigSFIdx, float> m_dilepton_trigger_sf_ee;
    std::map<DileptonTrigSFIdx, float> m_dilepton_trigger_sf_mm;
    std::map<DileptonTrigSFIdx, float> m_dilepton_trigger_sf_df;

    /// Check trigger firing
    /** provide the trigger chain via bit mask, e.g. TRIG_mu18 */
    bool passTrig(long long mask, bool requireAll=true) const {
      if(requireAll) return (trigFlags & mask) == mask;
      else return mask == 0 || (trigFlags & mask) != 0;
    }

    // Event Flag to check for LAr, bad jet, etc. List found in SusyDefs.h under EventCheck
    // Use cutFlags instead
    //int evtFlag[NtSys_N];

    /// Event cleaning cut flags. The bits are defined in SusyDefs as EventCleaningCuts
    unsigned int cutFlags[NtSys::SYS_UNKNOWN];

    // Reweighting and scaling
    float wPileup;            ///< pileup weight for full dataset
    float wPileup_up;         ///< pileup weight shifted for systematic
    float wPileup_dn;         ///< pileup weight shifted for systematic
    float wPileup_period;     ///< pileup period weight (wPileup = period weight x primary weight)
    float xsec;               ///< cross section * kfactor * efficiency, from SUSY db
    float errXsec;            ///< cross section uncertainty
    float sumw;               ///< Sum of generator weights

    // dihiggs specifc // dantrim Nov 24 2018 -- putting this here at the moment, can think of something better later
    float truth_mhh;          ///< truth level dihiggs mass
    float truth_pT_h0;        ///< truth level pT of leading higgs
    float truth_pT_h1;        ///< truth level pT of sub-leading higgs
    float truth_pThh;         ///< truth level pT of hh system
    float truth_dphi_hh;      ///< truth level delta phi between h0 and h1

    /// PDF Systematic information

    /// MC weights
    // would like this to be a `std::map<std::string, float>, but would need something like an updated version of TruthTools
    // e.g. see https://svnweb.cern.ch/trac/atlasoff/browser/Generators/GenAnalysisTools/TruthTools?order=name
    std::vector<float> mcWeights;

    /// print event
    void print() const;

    /// Clear vars
    void clear(){
      run = eventNumber = lb = 0;
      stream = Stream_Unknown;
      treatAsYear = -1;
      isMC = false;
      mcType = MCType::MCInvalid;
      mcChannel = w = 0;
      initialNumberOfEvents = sumOfEventWeights = sumOfEventWeightsSquared = 0;
      larError = 0;
      nVtx = avgMu = avgMuDataSF = trigFlags = 0;
      actualMu = actualMuDataSF = 0;
      pvX = pvY = pvZ = 0.;
      beamPosX = beamPosY = beamPosZ = beamPosSigmaX = beamPosSigmaY = beamPosSigmaZ = 0.;
      nTracksAtPV = nNeutralsAtPV = 0;
      susyFinalState = 0;
      hDecay = -1;
      eventWithSusyProp = false;
      susySpartId1 = susySpartId2 = 0;
      susy3BodyLeftPol = susy3BodyRightPol = susy3BodyOnlyMass = 0;
      isSherpaVjetsSample = false;
      sherpa22VjetsWeight = 1;
      //memset(evtFlag,0,sizeof(evtFlag));
      memset(cutFlags,0,sizeof(cutFlags));
      wPileup = wPileup_up = wPileup_dn = wPileup_period = 0;
      xsec = errXsec = sumw = 0;
      truth_mhh = truth_pT_h0 = truth_pT_h1 = truth_pThh = truth_dphi_hh = 0.;
      mcWeights.clear();
      m_dilepton_trigger_matches.clear();
      m_dilepton_trigger_sf_ee.clear();
      m_dilepton_trigger_sf_mm.clear();
      m_dilepton_trigger_sf_df.clear();
    }

    ClassDef(Event, 42);
  };
} // Susy
#endif
