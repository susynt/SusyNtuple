#ifndef SusyNtuple_SumwGrabber_h
#define SusyNtuple_SumwGrabber_h

//ROOT
#include "TTree.h"
#include "TChain.h"
#include "TFile.h"
#include "TH1.h"

//SusyNtuple
#include "SusyNtuple/SusyNtAna.h"
#include "SusyNtuple/SusyNtTools.h"

//std/stl
#include <fstream>

/////////////////////////////////////////////////////////////
//
// SumwGrabber
// Class auto-generated with SusyNtuple/make_susy_skeleton on 2018-04-13 07:52
//
/////////////////////////////////////////////////////////////

// for TSelector analysis loopers processing susyNt you MUST inherit from SusyNtAna
// in order to pick up the susyNt class objects
class SumwGrabber : public SusyNtAna
{

    public :
        SumwGrabber();
        virtual ~SumwGrabber() {};

        void set_debug(int dbg) { m_dbg = dbg; }
        int dbg() { return m_dbg; }

        void set_suffix(std::string suffix) { m_file_suffix = suffix; }
        std::string suffix() { return m_file_suffix; }

        void set_chain(TChain* chain) { m_input_chain = chain; }
        TChain* chain() { return m_input_chain; }

        ////////////////////////////////////////////
        // TSelector methods override
        ////////////////////////////////////////////
        virtual void Begin(TTree* tree); // Begin is called before looping on entries
        virtual Bool_t Process(Long64_t entry); // Main event loop function called on each event
        virtual void Terminate(); // Terminate is called after looping has finished

        void initialize_sumw_histograms();
        void save_output_file();


    private :
        int m_dbg;
        std::string m_file_suffix;
        TChain* m_input_chain; // the TChain object we are processing
        bool m_outputs_initialized;
        TFile* m_out_file;
        TH1D* m_sumw_histo;
        int m_dsid;
        MCType m_mctype;
        long int m_total_events_processed;
        double m_total_sumw;
        double m_total_sumw2;

}; //class


#endif
