#ifndef SUSY_FTAGGING_ENUMS_H
#define SUSY_FTAGGING_ENUMS_H

#include <string>

namespace Susy
{
    enum FTagEff {
        WP70=0,
        WP77,
        WP85,
        WPInvalid 
    };

    std::string FTagEff2str(const FTagEff& eff);

    enum FTagAlgo {
        MV2c10=0,
        DL1,
        AlgoInvalid
    };

    std::string FTagAlgo2str(const FTagAlgo& algo);

} // namespace Susy

#endif
