#ifndef SUSYNTUPLE_SUSYSYSTEST_H
#define SUSYNTUPLE_SUSYSYSTEST_H

//ROOT
class TChain;

//SusyNtuple
#include "SusyNtuple/SusyNtAna.h"
#include "SusyNtuple/SusyNtTools.h"

//std/stl
#include <fstream>
#include <string>
#include <vector>

class SusySysTest : public SusyNtAna
{
    public :
        SusySysTest();
        virtual ~SusySysTest();

        void set_chain(TChain* chain) { m_input_chain = chain; }
        TChain* chain() { return m_input_chain; }

        void set_debug(bool dbg) { m_dbg = dbg; }
        bool dbg() { return m_dbg; }


        //////////////////////////////////////
        //  TSelector Methods
        //////////////////////////////////////
        virtual void Begin(TTree* tree);
        virtual Bool_t Process(Long64_t entry);
        virtual void Terminate();

    private :
        TChain* m_input_chain;
        bool m_dbg;
}; // class SusySysTest

#endif
