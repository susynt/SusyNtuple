//  -*- c++ -*-
#ifndef SUSYNTUPLE_OVERLAPTOOLS_H
#define SUSYNTUPLE_OVERLAPTOOLS_H

#include <string>
using std::string;

#include "SusyNtuple/SusyDefs.h"
#include "SusyNtuple/SusyNt.h"
#include "SusyNtuple/AnalysisType.h"
#include "SusyNtuple/Isolation.h"

namespace Susy { class JetSelector; }
namespace Susy {

/// A class to  perform the overlap removal
/**
    The generic OverlapTools implements the generic definitions from
    AssociationUtils [1]. The SUSYTools configuration is default as
    specified in the SUSYTools object definition file [2].

    Analysis-dependent criteria should be implemented in your
    analysis-specific class inheriting from Overlap_Removals.

    The analysis-specific tool should be instantiated with Overlap_Removals::build().

    For details on the design and implementation of this class, see the
    documentation for JetSelector.

    daniel.joseph.antrim@cern.ch, February 24, 2016
    davide.gerbaudo@gmail.com

    [1] https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/AnalysisCommon/AssociationUtils/Root/OverlapRemovalTool.cxx 
    [2] https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/SUSYPhys/SUSYTools/Root/SUSYObjDef_xAOD.cxx
*/
class OverlapTools
{
public :
    /// provide analysis-specific OR tool (or vanilla one if analysis is unknown)
    /**
       The user owns the selector (i.e. should use std::shared_ptr
       or delete it when done with it).
    */
    static OverlapTools* build(const AnalysisType &a, bool verbose);
    OverlapTools(); ///< Default ctor
    virtual ~OverlapTools(){};  ///< dtor (for now we don't have anything to delete)
    // main overlap removal function, performs all removals
    /**
       includes several of the member functions below.
    */
    virtual void performOverlap(ElectronVector& electrons, MuonVector& muons,
                                JetVector& jets, TauVector& taus, PhotonVector& photons);

    
    //   the separate steps of OR removal implemented within "performOverlap(...)"
    // >> function name formatting (x_y_overlap): 
    //      - compare x and y with x being removed if there is overlap 
    //      - e = electron, m = muon, t = tau, p = photon
    //      - j = jet, PFj = Particle-flow jet, Fj = Fat-jet
    
    // (1) PFlow-Muon-jet OR (not implemented)
    // m_PFj_overlap(, );

    // (2) Electron-electron OR
    virtual void e_e_overlap(ElectronVector& electrons,
                             bool useTrackMatch = true,
                             bool useClusterMatch = false,
                             double dEta_max = 3*0.025,
                             double dPhi_max = 5*0.025);
    
    // (3) Tau-electron OR
    virtual void t_e_overlap(TauVector& taus, ElectronVector& electrons, 
                             double dR = 0.2,
                             const ElectronId& el_ID = ElectronId::LooseLLH); ///< remove tau from electron
    
    // (4) Tau-muon OR
    virtual void t_m_overlap(TauVector& taus, MuonVector& muons,
                             double dR = 0.2,
                             double minMuonPt = 2,
                             double minTauPtMuComb = 50); ///< remove tau from muon
    
    // (5) Electron-muon OR
    virtual void m_e_overlap(MuonVector& muons, ElectronVector& electrons); ///< remove calo muons overlapping with electrons
    virtual void e_m_overlap(ElectronVector& electrons, MuonVector& muons); ///< remove electron from muon (shared-track)
    
    // (6) Pho-electron OR
    virtual void p_e_overlap(PhotonVector& photons, ElectronVector& electrons,
                             double dR = 0.4); ///< remove photons overlapping with electrons
    
    // (7) Pho-muon OR
    virtual void p_m_overlap(PhotonVector& photons, MuonVector& muons,
                             double dR = 0.4); ///< remove photons overlapping with muons
    
    // (8) Electron-jet OR
    virtual void j_e_overlap(JetVector& jets, ElectronVector& electrons,
                             double dR = 0.2,
                             bool doBJetOR = false,
                             bool applyPtRatio = false,
                             double eleJetPtRatio = 0.8,
                             double bJetORLepThresh = -1);

    virtual void e_j_overlap(ElectronVector& electrons, JetVector& jets,
                             double dR = 0.4, 
                             bool doSlidingCone = false,
                             bool applyJVT = false); ///< remove electron from jet

    // (9) Muon-jet OR
    virtual void j_m_overlap(JetVector& jets, MuonVector& muons,
                             double dR = 0.2,
                             bool doBJetOR = false,
                             bool doGhost = true,
                             bool applyRelPt = false); ///< remove jet from muon

    virtual void m_j_overlap(MuonVector& muons, JetVector& jets,
                             double dR = 0.4, 
                             bool doSlidingCone = false,
                             bool applyJVT = false); ///< remove muon from jet
    
    // (10) Tau-jet OR
    virtual void j_t_overlap(JetVector& jets, TauVector& taus,
                             double dR = 0.2,
                             bool doBJetOR = false); ///< remove jet from tau
    virtual void t_j_overlap(TauVector& taus, JetVector& jets,
                             double dR = 0.2);
    
    // (11) Pho-jet OR (not implemented)
    virtual void j_p_overlap(JetVector& jets, PhotonVector& photons,
                             double dR = 0.4); ///< remove jet overlapping with photon

    
    // (12) Electron-fatjet OR (not implemented)
    // e_Fj_overlap(, );
    
    // (13) jet-fatjet OR (not implemented)
    // j_Fj_overlap(, );

    // helper methods

    /// remove non-isolated leptons from the input collections
    /**
       Some analyses might want to perform the overlap removal using
       isolated leptons only. In this case, they need to call this
       function within performOverlap() before calling all the
       a_b_overlap() functions. They also need to call
       setElectronIsolation() and setMuonIsolation() beforehand, with
       the values that can be retrieved from the ElectronSelector and
       MuonSelector.
     */
    virtual void removeNonisolatedLeptons(ElectronVector& electrons, MuonVector& muons);
    /// electron isolation used in leptonPassesIsolation()
    OverlapTools& setElectronIsolation( Isolation eleIso ) { m_electronIsolation = eleIso; return *this; }
    /// muon isolation used in leptonPassesIsolation()
    OverlapTools& setMuonIsolation( Isolation muIso ) { m_muonIsolation = muIso; return *this; }
    /// jet selector needed for j_X OR
    JetSelector* jetSelector() const { return m_jetSelector; }
    OverlapTools& jetSelector(JetSelector* js) { m_jetSelector = js; return *this; }
    /// used within removeNonisolatedLeptons()
    bool leptonPassesIsolation(const Lepton* lep, const Isolation &iso);

    bool electronPassesID(const Electron* el, const ElectronId &el_ID);
    bool electronsMatch(const Electron* el1, const Electron* el2,
                        bool useTrackMatch, bool useClusterMatch,
                        double dEta_max, double dPhi_max);
    const Electron* handleOverlap(const Electron* el1, const Electron* el2);


    /// check if muon ID tracks are ghost associated with the input jet
    bool muonIsGhostMatched(const Muon* mu, const Jet* jet);

    /// check if the electron and muon have shared ID tracks
    bool eleMuSharedTrack(const Electron* el, const Muon* mu);

    /// calculate the cone size for a sliding cone size based on object pT
    float getSlidingDRCone(float pT, float c1 = 0.04, float c2 = 10. /*GeV*/, float maxConeSize = 0.4);

    OverlapTools& setVerbose(bool v) { m_verbose = v; return *this; }
    OverlapTools& useOldOverlap(bool v) { m_useOldOverlap = v; return *this; }
    bool verbose() const { return m_verbose; }
    
    string str(const Jet* j);
    string str(const Lepton* l);
    string str(const Photon* pho);
    string str(const Tau* tau);
    
    // Default DeltaR printout
    template<class P1, class P2>
    void print_rm_msg(const std::string &msg, P1 drop, P2 keep) {
        std::cout << msg <<" removing "<< str(drop)
             <<" overlapping (dRy=" << keep->DeltaRy(*drop)
             <<") with "<<str(keep) << '\n';
    }

protected :
    Isolation m_electronIsolation;
    Isolation m_muonIsolation;
    JetSelector *m_jetSelector;
    bool m_verbose;
    bool m_useOldOverlap;

}; // class OverlapTools

//----------------------------------------------------------
//
// End generic selector, begin analysis-specific ones
//
//----------------------------------------------------------

/// implements OR procedure from ATL-COM-PHYS-2013-911
class OverlapTools_2Lep : public OverlapTools {
};
class OverlapTools_Stop2L : public OverlapTools {
    void performOverlap(ElectronVector& electrons, 
                        MuonVector& muons,
                        JetVector& jets, 
                        TauVector& /*taus*/, 
                        PhotonVector& /*photons*/
                        ) override final;
};


class OverlapTools_HLFV : public OverlapTools {
    void performOverlap(ElectronVector& electrons,
                        MuonVector& muons,
                        JetVector& jets,
                        TauVector& taus,
                        PhotonVector& photons
                        ) override final;
};
}







#endif
