//  -*- c++ -*-
#ifndef SusyNtuple_SusyNtSys_h
#define SusyNtuple_SusyNtSys_h

#include <string>
#include <map>

namespace Susy {

namespace NtSys {

enum SusyNtSys {

    ////////////////////////////////////////////
    // NOMINAL
    ////////////////////////////////////////////
    NOM

    ////////////////////////////////////////////
    // EGAMMA
    ////////////////////////////////////////////
    ,EG_RESOLUTION_ALL_DN
    ,EG_RESOLUTION_ALL_UP
    ,EG_SCALE_AF2_DN // new
    ,EG_SCALE_AF2_UP // new
    ,EG_SCALE_ALL_DN
    ,EG_SCALE_ALL_UP
    ,EL_EFF_ChargeIDSel_DN // new
    ,EL_EFF_ChargeIDSel_UP
    ,EL_EFF_ID_TOTAL_Uncorr_DN
    ,EL_EFF_ID_TOTAL_Uncorr_UP
    ,EL_EFF_Iso_TOTAL_Uncorr_DN
    ,EL_EFF_Iso_TOTAL_Uncorr_UP
    ,EL_EFF_Reco_TOTAL_Uncorr_DN
    ,EL_EFF_Reco_TOTAL_Uncorr_UP
    ,EL_EFF_TriggerEff_TOTAL_DN // new
    ,EL_EFF_TriggerEff_TOTAL_UP // new
    ,EL_EFF_Trigger_TOTAL_DN
    ,EL_EFF_Trigger_TOTAL_UP

    ////////////////////////////////////////////
    // FLAVOR TAGGING
    ////////////////////////////////////////////
    ,FT_EFF_B_systematics_UP
    ,FT_EFF_B_systematics_DN
    ,FT_EFF_C_systematics_UP
    ,FT_EFF_C_systematics_DN
    ,FT_EFF_Light_systematics_UP
    ,FT_EFF_Light_systematics_DN
    ,FT_EFF_extrapolation_UP
    ,FT_EFF_extrapolation_DN
    ,FT_EFF_extrapolation_charm_UP
    ,FT_EFF_extrapolation_charm_DN

    ////////////////////////////////////////////
    // JET ENERGY RESOLUTION
    ////////////////////////////////////////////
//    ,JER
    ,JET_JER_DataVsMC_UP
    ,JET_JER_DataVsMC_DN
    ,JET_JER_EffectiveNP_1_UP
    ,JET_JER_EffectiveNP_1_DN
    ,JET_JER_EffectiveNP_2_UP
    ,JET_JER_EffectiveNP_2_DN
    ,JET_JER_EffectiveNP_3_UP
    ,JET_JER_EffectiveNP_3_DN
    ,JET_JER_EffectiveNP_4_UP
    ,JET_JER_EffectiveNP_4_DN
    ,JET_JER_EffectiveNP_5_UP
    ,JET_JER_EffectiveNP_5_DN
    ,JET_JER_EffectiveNP_6_UP
    ,JET_JER_EffectiveNP_6_DN
    ,JET_JER_EffectiveNP_7rest_UP
    ,JET_JER_EffectiveNP_7rest_DN
    ,JET_JER_EffectiveNP_7_UP
    ,JET_JER_EffectiveNP_7_DN
    ,JET_JER_EffectiveNP_8_UP
    ,JET_JER_EffectiveNP_8_DN
    ,JET_JER_EffectiveNP_9_UP
    ,JET_JER_EffectiveNP_9_DN
    ,JET_JER_EffectiveNP_10_UP
    ,JET_JER_EffectiveNP_10_DN
    ,JET_JER_EffectiveNP_11_UP
    ,JET_JER_EffectiveNP_11_DN
    ,JET_JER_EffectiveNP_12rest_UP
    ,JET_JER_EffectiveNP_12rest_DN

    ////////////////////////////////////////////
    // JET ENERGY SCALE (GLOBALREDUCTION)
    ////////////////////////////////////////////
    ,JET_EffectiveNP_1_UP
    ,JET_EffectiveNP_1_DN
    ,JET_EffectiveNP_2_UP
    ,JET_EffectiveNP_2_DN
    ,JET_EffectiveNP_3_UP
    ,JET_EffectiveNP_3_DN
    ,JET_EffectiveNP_4_UP
    ,JET_EffectiveNP_4_DN
    ,JET_EffectiveNP_5_UP
    ,JET_EffectiveNP_5_DN
    ,JET_EffectiveNP_6_UP
    ,JET_EffectiveNP_6_DN
    ,JET_EffectiveNP_7_UP
    ,JET_EffectiveNP_7_DN
    ,JET_EffectiveNP_8rest_UP
    ,JET_EffectiveNP_8rest_DN
    ,JET_EtaIntercalibration_Modelling_UP
    ,JET_EtaIntercalibration_Modelling_DN
    ,JET_EtaIntercalibration_NonClosure_highE_UP
    ,JET_EtaIntercalibration_NonClosure_highE_DN
    ,JET_EtaIntercalibration_NonClosure_negEta_UP
    ,JET_EtaIntercalibration_NonClosure_negEta_DN
    ,JET_EtaIntercalibration_NonClosure_posEta_UP
    ,JET_EtaIntercalibration_NonClosure_posEta_DN
    ,JET_EtaIntercalibration_TotalStat_UP
    ,JET_EtaIntercalibration_TotalStat_DN
    ,JET_Flavor_Composition_UP
    ,JET_Flavor_Composition_DN
    ,JET_Flavor_Response_UP
    ,JET_Flavor_Response_DN
    ,JET_BJES_Response_UP
    ,JET_BJES_Response_DN
    ,JET_Pileup_OffsetMu_UP
    ,JET_Pileup_OffsetMu_DN
    ,JET_Pileup_OffsetNPV_UP
    ,JET_Pileup_OffsetNPV_DN
    ,JET_Pileup_PtTerm_UP
    ,JET_Pileup_PtTerm_DN
    ,JET_Pileup_RhoTopology_UP
    ,JET_Pileup_RhoTopology_DN
    ,JET_PunchThrough_MC16_UP
    ,JET_PunchThrough_MC16_DN
    ,JET_SingleParticle_HighPt_UP
    ,JET_SingleParticle_HighPt_DN

    ////////////////////////////////////////////
    // JET ENERGY SCALE (STRONGLY REDUCED)
    ////////////////////////////////////////////
    ,JET_GroupedNP_1_UP
    ,JET_GroupedNP_1_DN
    ,JET_GroupedNP_2_UP
    ,JET_GroupedNP_2_DN
    ,JET_GroupedNP_3_UP
    ,JET_GroupedNP_3_DN

    ////////////////////////////////////////////
    // JET ETA INTERCALIBRATION NON CLOSURE
    ////////////////////////////////////////////
    ,JET_EtaIntercalibration_UP
    ,JET_EtaIntercalibration_DN

    ////////////////////////////////////////////
    // JVT EFFICIENCY
    ////////////////////////////////////////////
    ,JET_JVTEff_UP
    ,JET_JVTEff_DN

    ////////////////////////////////////////////
    // MET SOFT TERM
    ////////////////////////////////////////////
    ,MET_SoftTrk_ResoPara
    ,MET_SoftTrk_ResoPerp
    ,MET_SoftTrk_ScaleDown
    ,MET_SoftTrk_ScaleUp

    ////////////////////////////////////////////
    // MUON
    ////////////////////////////////////////////
    ,MUON_EFF_BADMUON_STAT_DN
    ,MUON_EFF_BADMUON_STAT_UP
    ,MUON_EFF_BADMUON_SYS_DN
    ,MUON_EFF_BADMUON_SYS_UP
    ,MUON_EFF_ISO_STAT_DN
    ,MUON_EFF_ISO_STAT_UP
    ,MUON_EFF_ISO_SYS_DN
    ,MUON_EFF_ISO_SYS_UP
    ,MUON_EFF_RECO_STAT_DN
    ,MUON_EFF_RECO_STAT_UP
    ,MUON_EFF_RECO_SYS_DN
    ,MUON_EFF_RECO_SYS_UP
    ,MUON_EFF_RECO_STAT_LOWPT_DN
    ,MUON_EFF_RECO_STAT_LOWPT_UP
    ,MUON_EFF_RECO_SYS_LOWPT_DN
    ,MUON_EFF_RECO_SYS_LOWPT_UP
    ,MUON_EFF_TTVA_STAT_DN
    ,MUON_EFF_TTVA_STAT_UP
    ,MUON_EFF_TTVA_SYS_DN
    ,MUON_EFF_TTVA_SYS_UP
    ,MUON_EFF_TrigStat_DN
    ,MUON_EFF_TrigStat_UP
    ,MUON_EFF_TrigSys_DN
    ,MUON_EFF_TrigSys_UP
    ,MUON_MS_DN
    ,MUON_MS_UP
    ,MUON_ID_DN
    ,MUON_ID_UP
    ,MUON_SAGITTA_RESBIAS_DN
    ,MUON_SAGITTA_RESBIAS_UP
    ,MUON_SAGITTA_RHO_DN
    ,MUON_SAGITTA_RHO_UP
    ,MUON_SCALE_DN
    ,MUON_SCALE_UP

    ////////////////////////////////////////////
    // TAUS
    ////////////////////////////////////////////
    ,TAU_SME_TES_DET_DN
    ,TAU_SME_TES_DET_UP
    ,TAU_SME_TES_INSITU_DN
    ,TAU_SME_TES_INSITU_UP
    ,TAU_SME_TES_MODEL_DN
    ,TAU_SME_TES_MODEL_UP

    ////////////////////////////////////////////
    // PRW
    ////////////////////////////////////////////
    ,PILEUP_UP       ///< Positive shift for mu 
    ,PILEUP_DN       ///< Negative shift for mu

    ////////////////////////////////////////////
    // XSEC
    ////////////////////////////////////////////
    ,XS_UP
    ,XS_DN

    ////////////////////////////////////////////
    // NOPE NOPE NOPE TURN BACK
    ////////////////////////////////////////////
    ,SYS_UNKNOWN
};

const std::map<SusyNtSys, std::string> SusyNtSysNames {

    ////////////////////////////////////////////
    // NOMINAL
    ////////////////////////////////////////////
    {NOM,                               "NOM"},

    ////////////////////////////////////////////
    // EGAMMA
    ////////////////////////////////////////////
    {EG_RESOLUTION_ALL_DN,              "EG_RESOLUTION_ALL_DN"},
    {EG_RESOLUTION_ALL_UP,              "EG_RESOLUTION_ALL_UP"},
    {EG_SCALE_AF2_DN,                   "EG_SCALE_AF2_DN"},
    {EG_SCALE_AF2_UP,                   "EG_SCALE_AF2_UP"},
    {EG_SCALE_ALL_DN,                   "EG_SCALE_ALL_DN"},
    {EG_SCALE_ALL_UP,                   "EG_SCALE_ALL_UP"},
    {EL_EFF_ChargeIDSel_DN,             "EL_EFF_ChargeIDSel_DN"},
    {EL_EFF_ChargeIDSel_UP,             "EL_EFF_ChargeIDSel_UP"},
    {EL_EFF_ID_TOTAL_Uncorr_DN,         "EL_EFF_ID_TOTAL_Uncorr_DN"},
    {EL_EFF_ID_TOTAL_Uncorr_UP,         "EL_EFF_ID_TOTAL_Uncorr_UP"},
    {EL_EFF_Iso_TOTAL_Uncorr_DN,        "EL_EFF_Iso_TOTAL_Uncorr_DN"},
    {EL_EFF_Iso_TOTAL_Uncorr_UP,        "EL_EFF_Iso_TOTAL_Uncorr_UP"},
    {EL_EFF_Reco_TOTAL_Uncorr_DN,       "EL_EFF_Reco_TOTAL_Uncorr_DN"},
    {EL_EFF_Reco_TOTAL_Uncorr_UP,       "EL_EFF_Reco_TOTAL_Uncorr_UP"},
    {EL_EFF_TriggerEff_TOTAL_DN,        "EL_EFF_TriggerEff_TOTAL_DN"},
    {EL_EFF_TriggerEff_TOTAL_UP,        "EL_EFF_TriggerEff_TOTAL_UP"},
    {EL_EFF_Trigger_TOTAL_DN,           "EL_EFF_Trigger_TOTAL_DN"},
    {EL_EFF_Trigger_TOTAL_UP,           "EL_EFF_Trigger_TOTAL_UP"},

    ////////////////////////////////////////////
    // FLAVOR TAGGING
    ////////////////////////////////////////////
    {FT_EFF_B_systematics_UP,           "FT_EFF_B_systematics_UP"},
    {FT_EFF_B_systematics_DN,           "FT_EFF_B_systematics_DN"},
    {FT_EFF_C_systematics_UP,           "FT_EFF_C_systematics_UP"},
    {FT_EFF_C_systematics_DN,           "FT_EFF_C_systematics_DN"},
    {FT_EFF_Light_systematics_UP,       "FT_EFF_Light_systematics_UP"},
    {FT_EFF_Light_systematics_DN,       "FT_EFF_Light_systematics_DN"},
    {FT_EFF_extrapolation_UP,           "FT_EFF_extrapolation_UP"},
    {FT_EFF_extrapolation_DN,           "FT_EFF_extrapolation_DN"},
    {FT_EFF_extrapolation_charm_UP,     "FT_EFF_extrapolation_charm_UP"},
    {FT_EFF_extrapolation_charm_DN,     "FT_EFF_extrapolation_charm_DN"},

    ////////////////////////////////////////////
    // JET ENERGY RESOLUTION
    ////////////////////////////////////////////
//    {JER,                               "JER"},
    {JET_JER_DataVsMC_UP,               "JET_JER_DataVsMC_UP"},
    {JET_JER_DataVsMC_DN,               "JET_JER_DataVsMC_DN"},
    {JET_JER_EffectiveNP_1_UP,           "JET_JER_EffectiveNP_1_UP"},
    {JET_JER_EffectiveNP_1_DN,           "JET_JER_EffectiveNP_1_DN"},
    {JET_JER_EffectiveNP_2_UP,           "JET_JER_EffectiveNP_2_UP"},
    {JET_JER_EffectiveNP_2_DN,           "JET_JER_EffectiveNP_2_DN"},
    {JET_JER_EffectiveNP_3_UP,           "JET_JER_EffectiveNP_3_UP"},
    {JET_JER_EffectiveNP_3_DN,           "JET_JER_EffectiveNP_3_DN"},
    {JET_JER_EffectiveNP_4_UP,           "JET_JER_EffectiveNP_4_UP"},
    {JET_JER_EffectiveNP_4_DN,           "JET_JER_EffectiveNP_4_DN"},
    {JET_JER_EffectiveNP_5_UP,           "JET_JER_EffectiveNP_5_UP"},
    {JET_JER_EffectiveNP_5_DN,           "JET_JER_EffectiveNP_5_DN"},
    {JET_JER_EffectiveNP_6_UP,           "JET_JER_EffectiveNP_6_UP"},
    {JET_JER_EffectiveNP_6_DN,           "JET_JER_EffectiveNP_6_DN"},
    {JET_JER_EffectiveNP_7rest_UP,       "JET_JER_EffectiveNP_7rest_UP"},
    {JET_JER_EffectiveNP_7rest_DN,       "JET_JER_EffectiveNP_7rest_DN"},
    {JET_JER_EffectiveNP_7_UP,           "JET_JER_EffectiveNP_7_UP"},
    {JET_JER_EffectiveNP_7_DN,           "JET_JER_EffectiveNP_7_DN"},
    {JET_JER_EffectiveNP_8_UP,           "JET_JER_EffectiveNP_8_UP"},
    {JET_JER_EffectiveNP_8_DN,           "JET_JER_EffectiveNP_8_DN"},
    {JET_JER_EffectiveNP_9_UP,           "JET_JER_EffectiveNP_9_UP"},
    {JET_JER_EffectiveNP_9_DN,           "JET_JER_EffectiveNP_9_DN"},
    {JET_JER_EffectiveNP_10_UP,          "JET_JER_EffectiveNP_10_UP"},
    {JET_JER_EffectiveNP_10_DN,          "JET_JER_EffectiveNP_10_DN"},
    {JET_JER_EffectiveNP_11_UP,          "JET_JER_EffectiveNP_11_UP"},
    {JET_JER_EffectiveNP_11_DN,          "JET_JER_EffectiveNP_11_DN"},
    {JET_JER_EffectiveNP_12rest_UP,          "JET_JER_EffectiveNP_12rest_UP"},
    {JET_JER_EffectiveNP_12rest_DN,          "JET_JER_EffectiveNP_12rest_DN"},
    
    ////////////////////////////////////////////
    // JET ENERGY SCALE (GLOBAL REDUCTION)
    ////////////////////////////////////////////
    {JET_EffectiveNP_1_UP,              "JET_EffectiveNP_1_UP"},
    {JET_EffectiveNP_1_DN,              "JET_EffectiveNP_1_DN"},
    {JET_EffectiveNP_2_UP,              "JET_EffectiveNP_2_UP"},
    {JET_EffectiveNP_2_DN,              "JET_EffectiveNP_2_DN"},
    {JET_EffectiveNP_3_UP,              "JET_EffectiveNP_3_UP"},
    {JET_EffectiveNP_3_DN,              "JET_EffectiveNP_3_DN"},
    {JET_EffectiveNP_4_UP,              "JET_EffectiveNP_4_UP"},
    {JET_EffectiveNP_4_DN,              "JET_EffectiveNP_4_DN"},
    {JET_EffectiveNP_5_UP,              "JET_EffectiveNP_5_UP"},
    {JET_EffectiveNP_5_DN,              "JET_EffectiveNP_5_DN"},
    {JET_EffectiveNP_6_UP,              "JET_EffectiveNP_6_UP"},
    {JET_EffectiveNP_6_DN,              "JET_EffectiveNP_6_DN"},
    {JET_EffectiveNP_7_UP,              "JET_EffectiveNP_7_UP"},
    {JET_EffectiveNP_7_DN,              "JET_EffectiveNP_7_DN"},
    {JET_EffectiveNP_8rest_UP,          "JET_EffectiveNP_8rest_UP"},
    {JET_EffectiveNP_8rest_DN,          "JET_EffectiveNP_8rest_DN"},
    {JET_EtaIntercalibration_Modelling_UP,          "JET_EtaIntercalibration_Modelling_UP"},
    {JET_EtaIntercalibration_Modelling_DN,          "JET_EtaIntercalibration_Modelling_DN"},
    {JET_EtaIntercalibration_NonClosure_highE_UP,           "JET_EtaIntercalibration_NonClosure_highE_UP"},
    {JET_EtaIntercalibration_NonClosure_highE_DN,           "JET_EtaIntercalibration_NonClosure_highE_DN"},
    {JET_EtaIntercalibration_NonClosure_negEta_UP,          "JET_EtaIntercalibration_NonClosure_negEta_UP"},
    {JET_EtaIntercalibration_NonClosure_negEta_DN,          "JET_EtaIntercalibration_NonClosure_negEta_DN"},
    {JET_EtaIntercalibration_NonClosure_posEta_UP,          "JET_EtaIntercalibration_NonClosure_posEta_UP"},
    {JET_EtaIntercalibration_NonClosure_posEta_DN,          "JET_EtaIntercalibration_NonClosure_posEta_DN"},
    {JET_EtaIntercalibration_TotalStat_UP,          "JET_EtaIntercalibration_TotalStat_UP"},
    {JET_EtaIntercalibration_TotalStat_DN,          "JET_EtaIntercalibration_TotalStat_DN"},
    {JET_Flavor_Composition_UP,         "JET_Flavor_Composition_UP"},
    {JET_Flavor_Composition_DN,         "JET_Flavor_Composition_DN"},
    {JET_Flavor_Response_UP,            "JET_Flavor_Response_UP"},
    {JET_Flavor_Response_DN,            "JET_Flavor_Response_DN"},
    {JET_BJES_Response_UP,          "JET_BJES_Response_UP"},
    {JET_BJES_Response_DN,          "JET_BJES_Response_DN"},
    {JET_Pileup_OffsetMu_UP,        "JET_Pileup_OffsetMu_UP"},
    {JET_Pileup_OffsetMu_DN,        "JET_Pileup_OffsetMu_DN"},
    {JET_Pileup_OffsetNPV_UP,       "JET_Pileup_OffsetNPV_UP"},
    {JET_Pileup_OffsetNPV_DN,       "JET_Pileup_OffsetNPV_DN"},
    {JET_Pileup_PtTerm_UP,      "JET_Pileup_PtTerm_UP"},
    {JET_Pileup_PtTerm_DN,      "JET_Pileup_PtTerm_DN"},
    {JET_Pileup_RhoTopology_UP,     "JET_Pileup_RhoTopology_UP"},
    {JET_Pileup_RhoTopology_DN,     "JET_Pileup_RhoTopology_DN"},
    {JET_PunchThrough_MC16_UP,      "JET_PunchThrough_MC16_UP"},
    {JET_PunchThrough_MC16_DN,      "JET_PunchThrough_MC16_DN"},
    {JET_SingleParticle_HighPt_UP,  "JET_SingleParticle_HighPt_UP"},
    {JET_SingleParticle_HighPt_DN,  "JET_SingleParticle_HighPt_DN"},

    ////////////////////////////////////////////
    // JET ENERGY SCALE (STRONGLY REDUCED)
    ////////////////////////////////////////////
    {JET_GroupedNP_1_UP,                "JET_GroupedNP_1_UP"},
    {JET_GroupedNP_1_DN,                "JET_GroupedNP_1_DN"},
    {JET_GroupedNP_2_UP,                "JET_GroupedNP_2_UP"},
    {JET_GroupedNP_2_DN,                "JET_GroupedNP_2_DN"},
    {JET_GroupedNP_3_UP,                "JET_GroupedNP_3_UP"},
    {JET_GroupedNP_3_DN,                "JET_GroupedNP_3_DN"},

    ////////////////////////////////////////////
    // JET ETA INTERCALIBRATION NON CLOSURE
    ////////////////////////////////////////////
    {JET_EtaIntercalibration_UP,        "JET_EtaIntercalibration_UP"},
    {JET_EtaIntercalibration_DN,        "JET_EtaIntercalibration_DN"},

    ////////////////////////////////////////////
    // JVT EFFICIENCY
    ////////////////////////////////////////////
    {JET_JVTEff_UP,                     "JET_JVTEff_UP"},
    {JET_JVTEff_DN,                     "JET_JVTEff_DN"},

    ////////////////////////////////////////////
    // MET SOFT TERM
    ////////////////////////////////////////////
    {MET_SoftTrk_ResoPara,              "MET_SoftTrk_ResoPara"},
    {MET_SoftTrk_ResoPerp,              "MET_SoftTrk_ResoPerp"},
    {MET_SoftTrk_ScaleDown,             "MET_SoftTrk_ScaleDown"},
    {MET_SoftTrk_ScaleUp,               "MET_SoftTrk_ScaleUp"},

    ////////////////////////////////////////////
    // MUON
    ////////////////////////////////////////////
    {MUON_EFF_BADMUON_STAT_DN,"MUON_EFF_BADMUON_STAT_DN"},
    {MUON_EFF_BADMUON_STAT_UP,"MUON_EFF_BADMUON_STAT_UP"},
    {MUON_EFF_BADMUON_SYS_DN,"MUON_EFF_BADMUON_SYS_DN"},
    {MUON_EFF_BADMUON_SYS_UP,"MUON_EFF_BADMUON_SYS_UP"},
    {MUON_EFF_ISO_STAT_DN,"MUON_EFF_ISO_STAT_DN"},
    {MUON_EFF_ISO_STAT_UP,"MUON_EFF_ISO_STAT_UP"},
    {MUON_EFF_ISO_SYS_DN,"MUON_EFF_ISO_SYS_DN"},
    {MUON_EFF_ISO_SYS_UP,"MUON_EFF_ISO_SYS_UP"},
    {MUON_EFF_RECO_STAT_DN,"MUON_EFF_RECO_STAT_DN"},
    {MUON_EFF_RECO_STAT_UP,"MUON_EFF_RECO_STAT_UP"},
    {MUON_EFF_RECO_SYS_DN,"MUON_EFF_RECO_SYS_DN"},
    {MUON_EFF_RECO_SYS_UP,"MUON_EFF_RECO_SYS_UP"},
    {MUON_EFF_RECO_STAT_LOWPT_DN,"MUON_EFF_RECO_STAT_LOWPT_DN"},
    {MUON_EFF_RECO_STAT_LOWPT_UP,"MUON_EFF_RECO_STAT_LOWPT_UP"},
    {MUON_EFF_RECO_SYS_LOWPT_DN,"MUON_EFF_RECO_SYS_LOWPT_DN"},
    {MUON_EFF_RECO_SYS_LOWPT_UP,"MUON_EFF_RECO_SYS_LOWPT_UP"},
    {MUON_EFF_TTVA_STAT_DN,"MUON_EFF_TTVA_STAT_DN"},
    {MUON_EFF_TTVA_STAT_UP,"MUON_EFF_TTVA_STAT_UP"},
    {MUON_EFF_TTVA_SYS_DN,"MUON_EFF_TTVA_SYS_DN"},
    {MUON_EFF_TTVA_SYS_UP,"MUON_EFF_TTVA_SYS_UP"},
    {MUON_EFF_TrigStat_DN,"MUON_EFF_TrigStat_DN"},
    {MUON_EFF_TrigStat_UP,"MUON_EFF_TrigStat_UP"},
    {MUON_EFF_TrigSys_DN,"MUON_EFF_TrigSys_DN"},
    {MUON_EFF_TrigSys_UP,"MUON_EFF_TrigSys_UP"},
    {MUON_MS_DN,"MUON_MS_DN"},
    {MUON_MS_UP,"MUON_MS_UP"},
    {MUON_ID_DN,"MUON_ID_DN"},
    {MUON_ID_UP,"MUON_ID_UP"},
    {MUON_SAGITTA_RESBIAS_DN,"MUON_SAGITTA_RESBIAS_DN"},
    {MUON_SAGITTA_RESBIAS_UP,"MUON_SAGITTA_RESBIAS_UP"},
    {MUON_SAGITTA_RHO_DN,"MUON_SAGITTA_RHO_DN"},
    {MUON_SAGITTA_RHO_UP,"MUON_SAGITTA_RHO_UP"},
    {MUON_SCALE_DN,"MUON_SCALE_DN"},
    {MUON_SCALE_UP,"MUON_SCALE_UP"},

    ////////////////////////////////////////////
    // TAUS
    ////////////////////////////////////////////
    {TAU_SME_TES_DET_DN,                "TAU_SME_TES_DET_DN"},
    {TAU_SME_TES_DET_UP,                "TAU_SME_TES_DET_UP"},
    {TAU_SME_TES_INSITU_DN,             "TAU_SME_TES_INSITU_DN"},
    {TAU_SME_TES_INSITU_UP,             "TAU_SME_TES_INSITU_UP"},
    {TAU_SME_TES_MODEL_DN,              "TAU_SME_TES_MODEL_DN"},
    {TAU_SME_TES_MODEL_UP,              "TAU_SME_TES_MODEL_UP"},

    ////////////////////////////////////////////
    // PRW
    ////////////////////////////////////////////
    {PILEUP_UP,                         "PILEUP_UP"},
    {PILEUP_DN,                         "PILEUP_DN"},

    ////////////////////////////////////////////
    // XSEC
    ////////////////////////////////////////////
    {XS_UP,                             "XS_UP"},
    {XS_DN,                             "XS_DN"},

    ////////////////////////////////////////////
    // HOLD YOUR HORSES
    ////////////////////////////////////////////
    {SYS_UNKNOWN,                       "SYS_UNKNOWN"}
};

enum SysType {
    NOMINAL=0,
    WEIGHT,
    KINEMATIC,
    INVALID
};

const std::map<SusyNtSys, SysType> SusyNtSysTypes {

    ////////////////////////////////////////////
    // NOMINAL
    ////////////////////////////////////////////
    {NOM,                               SysType::NOMINAL},

    ////////////////////////////////////////////
    // EGAMMA
    ////////////////////////////////////////////
    {EG_RESOLUTION_ALL_DN,              SysType::KINEMATIC},  
    {EG_RESOLUTION_ALL_UP,              SysType::KINEMATIC},  
    {EG_SCALE_AF2_DN,                   SysType::KINEMATIC},  
    {EG_SCALE_AF2_UP,                   SysType::KINEMATIC},  
    {EG_SCALE_ALL_DN,                   SysType::KINEMATIC},  
    {EG_SCALE_ALL_UP,                   SysType::KINEMATIC},  
    {EL_EFF_ChargeIDSel_DN,             SysType::WEIGHT}, 
    {EL_EFF_ChargeIDSel_UP,             SysType::WEIGHT}, 
    {EL_EFF_ID_TOTAL_Uncorr_DN,         SysType::WEIGHT}, 
    {EL_EFF_ID_TOTAL_Uncorr_UP,         SysType::WEIGHT}, 
    {EL_EFF_Iso_TOTAL_Uncorr_DN,        SysType::WEIGHT}, 
    {EL_EFF_Iso_TOTAL_Uncorr_UP,        SysType::WEIGHT}, 
    {EL_EFF_Reco_TOTAL_Uncorr_DN,       SysType::WEIGHT}, 
    {EL_EFF_Reco_TOTAL_Uncorr_UP,       SysType::WEIGHT}, 
    {EL_EFF_TriggerEff_TOTAL_DN,        SysType::WEIGHT}, 
    {EL_EFF_TriggerEff_TOTAL_UP,        SysType::WEIGHT}, 
    {EL_EFF_Trigger_TOTAL_DN,           SysType::WEIGHT}, 
    {EL_EFF_Trigger_TOTAL_UP,           SysType::WEIGHT}, 

    ////////////////////////////////////////////
    // FLAVOR TAGGING
    ////////////////////////////////////////////
    {FT_EFF_B_systematics_UP,           SysType::WEIGHT}, 
    {FT_EFF_B_systematics_DN,           SysType::WEIGHT}, 
    {FT_EFF_C_systematics_UP,           SysType::WEIGHT}, 
    {FT_EFF_C_systematics_DN,           SysType::WEIGHT}, 
    {FT_EFF_Light_systematics_UP,       SysType::WEIGHT}, 
    {FT_EFF_Light_systematics_DN,       SysType::WEIGHT}, 
    {FT_EFF_extrapolation_UP,           SysType::WEIGHT}, 
    {FT_EFF_extrapolation_DN,           SysType::WEIGHT}, 
    {FT_EFF_extrapolation_charm_UP,     SysType::WEIGHT}, 
    {FT_EFF_extrapolation_charm_DN,     SysType::WEIGHT}, 

    ////////////////////////////////////////////
    // JET ENERGY RESOLUTION
    ////////////////////////////////////////////
    {JET_JER_DataVsMC_UP,               SysType::KINEMATIC},
    {JET_JER_DataVsMC_DN,               SysType::KINEMATIC},
    {JET_JER_EffectiveNP_1_UP,           SysType::KINEMATIC},
    {JET_JER_EffectiveNP_1_DN,           SysType::KINEMATIC},
    {JET_JER_EffectiveNP_2_UP,           SysType::KINEMATIC},
    {JET_JER_EffectiveNP_2_DN,           SysType::KINEMATIC},
    {JET_JER_EffectiveNP_3_UP,           SysType::KINEMATIC},
    {JET_JER_EffectiveNP_3_DN,           SysType::KINEMATIC},
    {JET_JER_EffectiveNP_4_UP,           SysType::KINEMATIC},
    {JET_JER_EffectiveNP_4_DN,           SysType::KINEMATIC},
    {JET_JER_EffectiveNP_5_UP,           SysType::KINEMATIC},
    {JET_JER_EffectiveNP_5_DN,           SysType::KINEMATIC},
    {JET_JER_EffectiveNP_6_UP,           SysType::KINEMATIC},
    {JET_JER_EffectiveNP_6_DN,           SysType::KINEMATIC},
    {JET_JER_EffectiveNP_7rest_UP,       SysType::KINEMATIC},
    {JET_JER_EffectiveNP_7rest_DN,       SysType::KINEMATIC},
    {JET_JER_EffectiveNP_7_UP,           SysType::KINEMATIC},
    {JET_JER_EffectiveNP_7_DN,           SysType::KINEMATIC},
    {JET_JER_EffectiveNP_8_UP,           SysType::KINEMATIC},
    {JET_JER_EffectiveNP_8_DN,           SysType::KINEMATIC},
    {JET_JER_EffectiveNP_9_UP,           SysType::KINEMATIC},
    {JET_JER_EffectiveNP_9_DN,           SysType::KINEMATIC},
    {JET_JER_EffectiveNP_10_UP,          SysType::KINEMATIC},
    {JET_JER_EffectiveNP_10_DN,          SysType::KINEMATIC},
    {JET_JER_EffectiveNP_11_UP,          SysType::KINEMATIC},
    {JET_JER_EffectiveNP_11_DN,          SysType::KINEMATIC},
    {JET_JER_EffectiveNP_12rest_UP,          SysType::KINEMATIC},
    {JET_JER_EffectiveNP_12rest_DN,          SysType::KINEMATIC},

    ////////////////////////////////////////////
    // JET ENERGY SCALE (GLOBAL REDUCTION)
    ////////////////////////////////////////////
    {JET_EffectiveNP_1_UP,              SysType::KINEMATIC},
    {JET_EffectiveNP_1_DN,              SysType::KINEMATIC},
    {JET_EffectiveNP_2_UP,              SysType::KINEMATIC},
    {JET_EffectiveNP_2_DN,              SysType::KINEMATIC},
    {JET_EffectiveNP_3_UP,              SysType::KINEMATIC},
    {JET_EffectiveNP_3_DN,              SysType::KINEMATIC},
    {JET_EffectiveNP_4_UP,              SysType::KINEMATIC},
    {JET_EffectiveNP_4_DN,              SysType::KINEMATIC},
    {JET_EffectiveNP_5_UP,              SysType::KINEMATIC},
    {JET_EffectiveNP_5_DN,              SysType::KINEMATIC},
    {JET_EffectiveNP_6_UP,              SysType::KINEMATIC},
    {JET_EffectiveNP_6_DN,              SysType::KINEMATIC},
    {JET_EffectiveNP_7_UP,              SysType::KINEMATIC},
    {JET_EffectiveNP_7_DN,              SysType::KINEMATIC},
    {JET_EffectiveNP_8rest_UP,          SysType::KINEMATIC},
    {JET_EffectiveNP_8rest_DN,          SysType::KINEMATIC},
    {JET_EtaIntercalibration_Modelling_UP,          SysType::KINEMATIC},
    {JET_EtaIntercalibration_Modelling_DN,          SysType::KINEMATIC},
    {JET_EtaIntercalibration_NonClosure_highE_UP,           SysType::KINEMATIC},
    {JET_EtaIntercalibration_NonClosure_highE_DN,           SysType::KINEMATIC},
    {JET_EtaIntercalibration_NonClosure_negEta_UP,          SysType::KINEMATIC},
    {JET_EtaIntercalibration_NonClosure_negEta_DN,          SysType::KINEMATIC},
    {JET_EtaIntercalibration_NonClosure_posEta_UP,          SysType::KINEMATIC},
    {JET_EtaIntercalibration_NonClosure_posEta_DN,          SysType::KINEMATIC},
    {JET_EtaIntercalibration_TotalStat_UP,          SysType::KINEMATIC},
    {JET_EtaIntercalibration_TotalStat_DN,          SysType::KINEMATIC},
    {JET_Flavor_Composition_UP,         SysType::KINEMATIC},
    {JET_Flavor_Composition_DN,         SysType::KINEMATIC},
    {JET_Flavor_Response_UP,            SysType::KINEMATIC},
    {JET_Flavor_Response_DN,            SysType::KINEMATIC},
    {JET_BJES_Response_UP,          SysType::KINEMATIC},
    {JET_BJES_Response_DN,          SysType::KINEMATIC},
    {JET_Pileup_OffsetMu_UP,        SysType::KINEMATIC},
    {JET_Pileup_OffsetMu_DN,        SysType::KINEMATIC},
    {JET_Pileup_OffsetNPV_UP,       SysType::KINEMATIC},
    {JET_Pileup_OffsetNPV_DN,       SysType::KINEMATIC},
    {JET_Pileup_PtTerm_UP,      SysType::KINEMATIC},
    {JET_Pileup_PtTerm_DN,      SysType::KINEMATIC},
    {JET_Pileup_RhoTopology_UP,     SysType::KINEMATIC},
    {JET_Pileup_RhoTopology_DN,     SysType::KINEMATIC},
    {JET_PunchThrough_MC16_UP,      SysType::KINEMATIC},
    {JET_PunchThrough_MC16_DN,      SysType::KINEMATIC},
    {JET_SingleParticle_HighPt_UP, SysType::KINEMATIC},
    {JET_SingleParticle_HighPt_DN, SysType::KINEMATIC},

    ////////////////////////////////////////////
    // JET ENERGY SCALE (STRONGLY REDUCED)
    ////////////////////////////////////////////
    {JET_GroupedNP_1_UP,                SysType::KINEMATIC}, 
    {JET_GroupedNP_1_DN,                SysType::KINEMATIC}, 
    {JET_GroupedNP_2_UP,                SysType::KINEMATIC}, 
    {JET_GroupedNP_2_DN,                SysType::KINEMATIC}, 
    {JET_GroupedNP_3_UP,                SysType::KINEMATIC}, 
    {JET_GroupedNP_3_DN,                SysType::KINEMATIC}, 

    ////////////////////////////////////////////
    // JET ETA INTERCALIBRATION NON CLOSURE
    ////////////////////////////////////////////
    {JET_EtaIntercalibration_UP,        SysType::KINEMATIC}, 
    {JET_EtaIntercalibration_DN,        SysType::KINEMATIC},  

    ////////////////////////////////////////////
    // JVT EFFICIENCY
    ////////////////////////////////////////////
    {JET_JVTEff_UP,                     SysType::WEIGHT}, 
    {JET_JVTEff_DN,                     SysType::WEIGHT},  

    ////////////////////////////////////////////
    // MET SOFT TERM
    ////////////////////////////////////////////
    {MET_SoftTrk_ResoPara,              SysType::KINEMATIC}, 
    {MET_SoftTrk_ResoPerp,              SysType::KINEMATIC}, 
    {MET_SoftTrk_ScaleDown,             SysType::KINEMATIC}, 
    {MET_SoftTrk_ScaleUp,               SysType::KINEMATIC}, 

    ////////////////////////////////////////////
    // MUON
    ////////////////////////////////////////////
    {MUON_EFF_BADMUON_STAT_DN,          SysType::WEIGHT}, 
    {MUON_EFF_BADMUON_STAT_UP,          SysType::WEIGHT}, 
    {MUON_EFF_BADMUON_SYS_DN,           SysType::WEIGHT}, 
    {MUON_EFF_BADMUON_SYS_UP,           SysType::WEIGHT}, 
    {MUON_EFF_ISO_STAT_DN,              SysType::WEIGHT}, 
    {MUON_EFF_ISO_STAT_UP,              SysType::WEIGHT}, 
    {MUON_EFF_ISO_SYS_DN,               SysType::WEIGHT}, 
    {MUON_EFF_ISO_SYS_UP,               SysType::WEIGHT}, 
    {MUON_EFF_RECO_STAT_DN,             SysType::WEIGHT}, 
    {MUON_EFF_RECO_STAT_UP,             SysType::WEIGHT}, 
    {MUON_EFF_RECO_SYS_DN,              SysType::WEIGHT}, 
    {MUON_EFF_RECO_SYS_UP,              SysType::WEIGHT}, 
    {MUON_EFF_RECO_STAT_LOWPT_DN,       SysType::WEIGHT}, 
    {MUON_EFF_RECO_STAT_LOWPT_UP,       SysType::WEIGHT}, 
    {MUON_EFF_RECO_SYS_LOWPT_DN,        SysType::WEIGHT}, 
    {MUON_EFF_RECO_SYS_LOWPT_UP,        SysType::WEIGHT}, 
    {MUON_EFF_TTVA_STAT_DN,             SysType::WEIGHT}, 
    {MUON_EFF_TTVA_STAT_UP,             SysType::WEIGHT}, 
    {MUON_EFF_TTVA_SYS_DN,              SysType::WEIGHT}, 
    {MUON_EFF_TTVA_SYS_UP,              SysType::WEIGHT}, 
    {MUON_EFF_TrigStat_DN,              SysType::WEIGHT}, 
    {MUON_EFF_TrigStat_UP,              SysType::WEIGHT}, 
    {MUON_EFF_TrigSys_DN,               SysType::WEIGHT}, 
    {MUON_EFF_TrigSys_UP,               SysType::WEIGHT}, 
    {MUON_MS_DN,                        SysType::KINEMATIC}, 
    {MUON_MS_UP,                        SysType::KINEMATIC}, 
    {MUON_ID_DN,                        SysType::KINEMATIC}, 
    {MUON_ID_UP,                        SysType::KINEMATIC}, 
    {MUON_SAGITTA_RESBIAS_DN,           SysType::KINEMATIC}, 
    {MUON_SAGITTA_RESBIAS_UP,           SysType::KINEMATIC}, 
    {MUON_SAGITTA_RHO_DN,               SysType::KINEMATIC}, 
    {MUON_SAGITTA_RHO_UP,               SysType::KINEMATIC}, 
    {MUON_SCALE_DN,                     SysType::KINEMATIC}, 
    {MUON_SCALE_UP,                     SysType::KINEMATIC}, 

    ////////////////////////////////////////////
    // TAUS
    ////////////////////////////////////////////
    {TAU_SME_TES_DET_DN,                SysType::KINEMATIC}, 
    {TAU_SME_TES_DET_UP,                SysType::KINEMATIC},  
    {TAU_SME_TES_INSITU_DN,             SysType::KINEMATIC},  
    {TAU_SME_TES_INSITU_UP,             SysType::KINEMATIC},  
    {TAU_SME_TES_MODEL_DN,              SysType::KINEMATIC},  
    {TAU_SME_TES_MODEL_UP,              SysType::KINEMATIC},  

    ////////////////////////////////////////////
    // PRW
    ////////////////////////////////////////////
    {PILEUP_UP,                         SysType::WEIGHT}, 
    {PILEUP_DN,                         SysType::WEIGHT},  

    ////////////////////////////////////////////
    // XSEC
    ////////////////////////////////////////////
    {XS_UP,                             SysType::WEIGHT}, 
    {XS_DN,                             SysType::WEIGHT}, 

    ////////////////////////////////////////////
    // HOLD YOUR HORSES
    ////////////////////////////////////////////
    {SYS_UNKNOWN,                       SysType::INVALID}
};

} //NtSys

} // Susy


#endif
