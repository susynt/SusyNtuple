// -*- c++ -*-
#ifndef SUSYNTUPLE_ELECTRONSELECTOR_H
#define SUSYNTUPLE_ELECTRONSELECTOR_H

#include "SusyNtuple/SusyNtSys.h"
#include "SusyNtuple/AnalysisType.h"
#include "SusyNtuple/ElectronId.h"
#include "SusyNtuple/Isolation.h"


namespace Susy {
class Electron;

/// A class to select electrons
/**
   The generic ElectronSelector implements a generic definitions

   Analysis-dependent criteria should be implemented in your
   analysis-specific class inheriting from ElectronSelector.

   The analysis-specific selector should be instantiated with ElectronsSelector::build().

   For details on the design and implementation of this class, see the
   documentation for JetSelector.

   davide.gerbaudo@gmail.com, Sep 2015
*/
class ElectronSelector
{
public:
    static ElectronSelector* build(const AnalysisType &a, bool verbose);
    ElectronSelector(); ///< Default ctor
    virtual ~ElectronSelector() {}; ///< dtor (for now we don't have anything to delete)
    ElectronSelector& setVerbose(bool v) { m_verbose = v; return *this; }

    ElectronSelector& setAnalysis(const AnalysisType& analysis);

    virtual bool isBaseline(const Electron* el); ///< whether el passes the baseline criteria
    virtual bool isSignal(const Electron* el); ///< whether el passes the signal criteria
    virtual bool isAntiID(const Electron* el); ///< whether el passes the Anti-ID criteria for fake factor method

    virtual bool passIpCut(const Electron* el);
    virtual bool outsideCrackRegion(const Electron *el);

    /// id of signal electron, used to determine err SF
    ElectronId signalId() const { return m_signalId; }
    /// set signal isolation
    /**
       Note: the value you set here should match whatever you have in
       _your_ (overriding) implementation of isSignalElectron()
     */
    ElectronSelector& setSignalId(const ElectronId &v) { m_signalId = v; return *this; }
    /// Retrieve the isolation requirement for signal electrons.
    /**
       This is helpful for other tools that need to know how the
       electrons are defined.
    */
    Isolation signalIsolation() const { return m_signalIsolation; }
    /// set signal isolation
    /**
       Note: the value you set here should match whatever you have in
       _your_ (overriding) implementation of isSignalElectron()
     */
    ElectronSelector& setSignalIsolation(const Isolation &v) { m_signalIsolation = v; return *this; }

    /**
       Get the input electron "ele"'s nominal efficicncy SF
    */
    float effSF(const Electron& ele, const NtSys::SusyNtSys sys = NtSys::NOM);
    /// wraps above
    float effSF(const Electron* ele, const NtSys::SusyNtSys sys = NtSys::NOM) { return effSF(*ele, sys); }

    ///  Error on the efficiency for the requested systematic
    float errEffSF(const Electron& ele, const NtSys::SusyNtSys sys);
    /// wraps above
    float errEffSF(const Electron* ele, const NtSys::SusyNtSys sys) { return errEffSF(*ele, sys); }

protected :
    ElectronId m_signalId;       ///< electron quality requirement (selected from eleID enum)
    Isolation m_signalIsolation; ///< electron isolation qualiy for signal electrons (c.f. SusyNtuple/Isolation.h)
    bool m_verbose;

}; // end ElectronSelector

//----------------------------------------------------------
//
// End generic selector, begin analysis-specific ones
//
//----------------------------------------------------------

class ElectronSelector_2Lep : public ElectronSelector {
    bool isBaseline(const Electron* el) override final;
    bool isSignal(const Electron* el) override final;
};

class ElectronSelector_Stop2L : public ElectronSelector {
    bool isBaseline(const Electron* el) override final;
    bool isSignal(const Electron* el) override final;
    bool isAntiID(const Electron* el) override final;
};

class ElectronSelector_HLFV : public ElectronSelector {
    bool isBaseline(const Electron* el) override final;
    bool isSignal(const Electron* el) override final;
    bool isAntiID(const Electron* el) override final;
};
} // Susy

#endif


