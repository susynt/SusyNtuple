// -*- c++ -*-
#ifndef SUSYNTUPLE_TAUSELECTOR_H
#define SUSYNTUPLE_TAUSELECTOR_H

#include "SusyNtuple/AnalysisType.h"

namespace Susy {

class Tau;

/// A class to select tau
/**
   The generic TauSelector implements a generic definition

   Analysis-dependent criteria should be implemented in your
   analysis-specific class inheriting from TauSelector.

   The analysis-specific selector should be instantiated with TauSelector::build().

   For details on the design and implementation of this class, see the
   documentation for JetSelector.

   davide.gerbaudo@gmail.com, Sep 2015
*/
class TauSelector
{
public:
    /// provide analysis-specific selector (or vanilla one if analysis is unknown)
    /**
       The user owns the selector (i.e. should use std::shared_ptr
       or delete it when done with it).
    */
    static TauSelector* build(const AnalysisType &a, bool verbose);
    TauSelector(); ///< Default ctor
    virtual ~TauSelector() {}; ///< dtor (for now we don't have anything to delete)

    TauSelector& setVerbose(bool v) { m_verbose = v; return *this; }


    /// whether tau passes the signal criteria
    /**
       Usually pt+bdt
    */
    virtual bool isBaseline(const Tau* tau);
    /// whether tau passes the signal criteria
    virtual bool isSignal(const Tau* tau);

protected :
    /// whether it should be verbose
    bool m_verbose;

}; // TauSelector

//----------------------------------------------------------
//
// End generic selector, begin analysis-specific ones
//
//----------------------------------------------------------

/// implements tau selection for ATL-COM-PHYS-2013-911
// Ana_2Lep
class TauSelector_2Lep : public TauSelector {
};

// Ana_HLFV
class TauSelector_HLFV : public TauSelector {
    bool isBaseline(const Tau* tau) override final;
    bool isSignal(const Tau* tau) override final;
};
} // namespace Susy
#endif
