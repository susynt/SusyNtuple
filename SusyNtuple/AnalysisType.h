// Dear emacs, this is -*- c++ -*-
#ifndef SUSYNTUPLE_ANALYSISTYPE_H
#define SUSYNTUPLE_ANALYSISTYPE_H

#include <string>

namespace Susy
{
/// 2-lep or 3-lep flag
enum class AnalysisType {
    Ana_2Lep,       ///< Dilepton electroweak
    Ana_Stop2L,     ///< Direct stop to two lepton analysis
    Ana_HLFV,       ///< Higgs to tau lep (LFV) 
    kUnknown
};
/// Human-readable names
std::string AnalysisType2str(const AnalysisType &a);

} // Susy
#endif
