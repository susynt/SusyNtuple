#ifndef TRIGGERLIST_H
#define TRIGGERLIST_H

#include <string>
#include <vector>

// All triggers are checked for matching (see XaodAnalysis::sample_event_triggers)
// Most but not all lepton triggers are matched (grep for functions with IsTrigMatched in XaodAnalysis.cxx)

//////////////////////////////////////////////////////////////////////////////
const std::vector<std::string> single_muo_triggers_list =
{
    "HLT_mu8noL1", // dantrim 2017 June 28 - this should never fire for event level, but need it here for trigger SF mapping
    "HLT_mu10noL1",
    "HLT_mu12noL1",
    "HLT_mu10", // for trigger SF mapping
    "HLT_mu14", // dantrim 2017 June 28 - this is for trigger SF mapping
    "HLT_mu18", // dantrim 2017 June 28 - this is for trigger SF mapping
    "HLT_mu20", // for trigger SF mapping
    "HLT_mu24", // for trigger SF mapping
    "HLT_mu26", // for trigger SF mapping
    "HLT_mu28", // for trigger SF mapping
    "HLT_mu20_iloose_L1MU15",
    "HLT_mu20_ivarloose_L1MU15",
    "HLT_mu22",
    "HLT_mu24_ivarmedium",
    "HLT_mu24_imedium",
    "HLT_mu24_ivarloose",
    "HLT_mu24_ivarloose_L1MU15",
    "HLT_mu26_ivarmedium",
    "HLT_mu26_imedium",
    "HLT_mu28_ivarmedium",
    "HLT_mu40",
    "HLT_mu50",
    "HLT_mu60",
    "HLT_mu60_0eta105_msonly"
};
//////////////////////////////////////////////////////////////////////////////
const std::vector<std::string> di_muo_triggers_list =
{
    "HLT_2mu10",
    "HLT_2mu14",
    "HLT_mu18_mu8noL1",
    "HLT_mu20_mu8noL1",
    "HLT_mu22_mu8noL1",
    "HLT_mu24_mu8noL1",
    "HLT_mu24_mu10noL1",
    "HLT_mu24_mu12noL1",
    "HLT_mu26_mu8noL1",
    "HLT_mu26_mu10noL1",
    "HLT_mu28_mu8noL1"
};
//////////////////////////////////////////////////////////////////////////////
const std::vector<std::string> single_ele_triggers_list =
{
    "HLT_e24_lhmedium_L1EM20VH",
    "HLT_e24_lhmedium_L1EM20VHI",
    "HLT_e24_lhtight_nod0_ivarloose",
    "HLT_e26_lhtight_nod0_ivarloose",
    "HLT_e28_lhtight_nod0_noringer_ivarloose",
    "HLT_e28_lhtight_nod0_ivarloose",
    "HLT_e32_lhtight_nod0_ivarloose",
    "HLT_e60_lhmedium",
    "HLT_e60_lhmedium_nod0",
    "HLT_e60_lhmedium_nod0_L1EM24VHI",
    "HLT_e80_lhmedium_nod0_L1EM24VHI",
    "HLT_e120_lhloose",
    "HLT_e140_lhloose_nod0",
    "HLT_e140_lhloose_nod0_L1EM24VHI",
    "HLT_e300_etcut",
    "HLT_e300_etcut_L1EM24VHI"
};
//////////////////////////////////////////////////////////////////////////////
const std::vector<std::string> di_ele_triggers_list =
{
    "HLT_2e12_lhloose_L12EM10VH",
    "HLT_2e15_lhvloose_nod0_L12EM13VH",
    "HLT_2e17_lhvloose_nod0",
    "HLT_2e17_lhvloose_nod0_L12EM15VHI",
    "HLT_2e19_lhvloose_nod0",
    "HLT_2e24_lhvloose_nod0"
};
//////////////////////////////////////////////////////////////////////////////
const std::vector<std::string> ele_muo_triggers_list =
{
    "HLT_e7_lhmedium_nod0_mu24",
    "HLT_e7_lhmedium_mu24",
    "HLT_e17_lhloose_mu14",
    "HLT_e17_lhloose_nod0_mu14",
    "HLT_e24_lhmedium_nod0_L1EM20VHI_mu8noL1",
    "HLT_e24_lhmedium_L1EM20VHI_mu8noL1",
    "HLT_e26_lhmedium_nod0_L1EM22VHI_mu8noL1",
    "HLT_e26_lhmedium_nod0_mu8noL1",
    "HLT_e28_lhmedium_nod0_mu8noL1"
};
//////////////////////////////////////////////////////////////////////////////
const std::vector<std::string> met_triggers_list = 
{
    // 2015
    "HLT_xe70",
    "HLT_xe70_mht",
    "HLT_xe70_tc_lcw",
    // 2016
    "HLT_xe80_tc_lcw_L1XE50", // Period A - C
    "HLT_xe90_mht_L1XE50", // Period A - D3
    "HLT_xe100_mht_L1XE50", // HLT_xe100_mht_L1XE50 ran unprescaled during D4-F1 other than at the start of some high-luminosity runs
    "HLT_xe110_mht_L1XE50",
    // 2017
    "HLT_xe110_pufit_L1XE50", // Lower thresholds (L1_XE50) but has been prescaled for part of the runs (~ 900 pb-1 loss of luminosity)
    "HLT_xe110_pufit_L1XE55",
    // 2018
    "HLT_xe110_pufit_xe65_L1XE50" // Unprescaled since run 350067
    "HLT_xe110_pufit_xe70_L1XE50",

};

#endif
