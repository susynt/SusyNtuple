// Dear emacs, this is -*- c++ -*-
#ifndef SUSYNTUPLE_LEPTON_H
#define SUSYNTUPLE_LEPTON_H

#include "SusyNtuple/Particle.h"

#include "TBits.h"

namespace Susy
{
/// Lepton class, common to electrons and muons
class Lepton : public Particle
{
public:
    Lepton() :
        trigBits(m_nTriggerBits)
        {
            clear();
        }
    virtual ~Lepton(){};
    /** Copy constructor */
    Lepton(const Lepton &);
    /** Assignment operator */
    Lepton& operator=(const Lepton &);

    int idx;                ///< index of lepton in lepton (muon or electron) collection stored in SusyNt

    // public member vars
    int q;                    ///< Charge
    bool isSUSYToolsBaseline; ///< flag computed by SUSYTools_xAOD
    bool isSUSYToolsSignal; ///< flag computed by SUSYTools_xAOD

    // Isolation variables common between electrons and muons
    // track-based isolation isolation variables
    float ptvarcone20;
    float ptcone20;
    float ptvarcone30;
    float ptcone30;
    float ptvarcone40;
    float ptcone40;
    // calo-based isolation isolation variables
    float etcone20;
    float topoetcone20;
    float topoetcone30;
    float topoetcone40;
    // "PU-robust" track-based isolation variables
    float ptvarcone20_TightTTVA_pt1000;
    float ptvarcone30_TightTTVA_pt1000;
    float ptvarcone30_TightTTVA_pt500;
    float ptcone20_TightTTVA_pt1000;
    float ptcone20_TightTTVA_pt500;

    // IsolationSelectionTool flags
    bool isoGradientLoose;          ///< GradientLoose WP
    bool isoGradient;               ///< Gradient WP
    bool isoLooseTrackOnly;         ///< LooseTrackOnly WP
    bool isoLoose;                  ///< Loose WP
    bool isoFixedCutTightTrackOnly; ///< FixedCutTightTrackOnly WP
    bool isoFCLoose;                ///< FCLoose WP
    bool isoFCTight;                ///< FCTight WP
    bool isoFCTightTrackOnly;       ///< FCTightTrackOnly WP (only applies to muons)
    bool isoFCHighPtCaloOnly;       ///< FCHighPtCaloOnly WP (only applies to electrons)

    // CloseBy correction
    bool isoGradientLooseCloseBy;   ///< GradientLoose WP with CloseBy lepton correction
    bool isoFixedCutTightTrackOnlyCloseBy; ///< FixedCutTightTrackOnly WP with CloseBy lepton correction

    float d0;                 ///< d0 extrapolated to PV
    float errD0;              ///< Uncertainty on d0
    float d0sigBSCorr;        ///< beam spot corrected d0Sig
    float z0;                 ///< z0 extrapolated to PV
    float errZ0;              ///< Uncertainty on z0

    unsigned int mcType;      ///< MCTruthClassifier particle type
    unsigned int mcOrigin;    ///< MCTruthClassifier particle origin

    /** Bkg ID information
            mcFirstEgMotherTruthType
            mcFirstEgMotherTruthOrigin
            mcFirstEgMotherPdgId

        For the moment this deals only with electrons
        but potentially in the future the same decorators
        may be needed for muons, so we add this to the
        Lepton class.

        For more information see the following:
        [1] https://twiki.cern.ch/twiki/bin/view/AtlasProtected/EGammaTruthRun2#Background_Electron_Classificati
        [2] https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MuonTruthRun2
        [3] http://atlas-computing.web.cern.ch/atlas-computing/links/buildDirectory/AtlasOffline/19.1.0/InstallArea/doc/MCTruthClassifier/html/classMCTruthClassifier.html
        [4] https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MCTruthClassifier#ID_track_classification
    */
    unsigned int mcFirstEgMotherTruthType;
    unsigned int mcFirstEgMotherTruthOrigin;
    int mcFirstEgMotherPdgId;

    float effSF;              ///< Efficiency scale factor  (for electron from LH)

    TBits   trigBits;         ///< TBits to store matched trigger chains
    static const size_t m_nTriggerBits=64;

    /// Methods to return impact parameter variables
    /** Note that these are not absolute valued! */
    float d0Sig() const {
      return d0/errD0;
    }
    float z0SinTheta() const {
      return z0 * sin(Theta());
    }

    // Polymorphism, baby!!
    virtual bool isEle() const { return false; }
    virtual bool isMu()  const { return false; }
    void setState(int /*sys*/){ resetTLV(); }

    /// Print method
    virtual void print() const {};

    /// Clear vars
    void clear(){
        idx = 0;
        q = 0;
        topoetcone20 = topoetcone30
                     = ptcone20    = ptcone30 
                     = ptvarcone20 = ptvarcone30 = 0;
        isoGradientLoose = isoGradient = isoLooseTrackOnly = isoLoose = isoFixedCutTightTrackOnly = false;
        isoGradientLooseCloseBy = isoFixedCutTightTrackOnly = false;
        isoFCLoose = isoFCTight = isoFCTightTrackOnly = isoFCHighPtCaloOnly = false;
        isoGradientLooseCloseBy = isoFixedCutTightTrackOnlyCloseBy = false;
        d0 = errD0 = d0sigBSCorr = z0 = errZ0 = 0;
        mcType = mcOrigin = 0;
        mcFirstEgMotherTruthType = mcFirstEgMotherTruthOrigin = mcFirstEgMotherPdgId = 0;
        effSF = 1;
        //errEffSF = 0;
        isSUSYToolsBaseline = isSUSYToolsSignal = false;
        Particle::clear();
    }

    ClassDef(Lepton, 29);
};
} // Susy
#endif
